<?php

require('init-mvc.php');

$dir = $tmvc->config['root_path'] . $tmvc->get_user_folder();
//exit($dir);
function listDir($dir)
{
	$it = new RecursiveDirectoryIterator($dir);
	$count = 0;
	foreach(new RecursiveIteratorIterator($it) as $file){
		// filter files to array & sort all alphabetically
		if(stristr($file, '.jpg') || stristr($file, '.gjpg') || stristr($file, '.jpeg')){
			$_sort[] = str_ireplace($dir, '', $file);
		}
	}
	
	if(!empty($_sort)){
		
		sort($_sort);
		foreach($_sort as $key => $value){
		
			$_out .= '<li class="doc" onclick="Sitis.loadImg(\''.$value.'\', ' . $count . ');"><span><a id="' . $value . '" class="file_name">' . $value . "</a></span></li>";
			if($count == 0){
				if(is_file($dir . $value)) $flag = 1; else $flag = 0;
				$f_file = $value;
			}
			$count++;
		}
	}
	
	if(!empty($flag)) return array('html' => $_out, 'f_file' => $f_file);
}

$out = listDir($dir);

?>

<script type="text/javascript">
	window.img_global = "<?php if(is_array($out)) echo $out['f_file']; else echo 0; ?>";
</script>

<?php

echo '<div id="vScroll"><ul id="tree1" class="tree" style="overflow: hidden;font-size: 12px"><li class="folder f-open first"><span>Project</span><ul>';

if(is_array($out)) echo $out['html']; else echo '<li class="doc"><span>empty</span></li>';

echo '</ul></li></ul></div><div id="vBar"><div id="vKnob"></div></div>';

?>

<script type="text/javascript">
window.addEvent('domready', function(){
	Sitis.refresh_files();
});
</script>


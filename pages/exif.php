<?php

	/**
	 * Get SGeo data from APP2 Exif section data
	 *
	 * @static
	 * @access public
	 * @param string $jpeg_path full path to jpeg file (linux style /)
	 * @param string $exec_path path to commmand line exec file, must be compiled in the same folder as ExifLibImpl.so
	 * @param string|array $exclude_element exclude SGeo tag element or elements from output
	 * @return array|int 0 - APP2 section contains no SGeo data, 1 - jpeg file not found, 2 - exec file not found , 3 - library permissions error access (status 127,126 etc.)
	 */
	
	function getExifAPP2Data($exec_path, $jpeg_path, $exclude_element = null)
	{
		if(!is_file($exec_path)) return 2;
		if(!is_file($jpeg_path)) return 1;
		$shell = exec($exec_path.' '.$jpeg_path, $result, $status);
		if($status > 0) return 3;
		if(!empty($result) || isset($result))
		{
			foreach($result as $key => $value)
			{
				//$parse = explode(':', str_replace(" ", '', $value));
				$parse = explode(':', $value);
				if(!is_array($exclude_element) && !is_null($exclude_element))
				{
					$tag[$parse[0]] = $parse[1];
				
				} elseif(is_array($exclude_element) && !is_null($exclude_element)){
					
					foreach($exclude_element as $key_e => $value_e)
					{
						$tag[$parse[0]] = $parse[1];
					}
				
				} else {$tag[$parse[0]] = $parse[1];}
			}
			return $tag;
			
		} else {return 0;}
	}
	
	function saveExifAPP2Data($root_path, $user_path, $file, $comment)
	{
		$_comment = urldecode($comment);
		//echo $_comment;
		$cmd = '/home/php5-3/geo-photo.net/www/exif/exifer ' . $root_path . $user_path . $file . ' --comments="' . ltrim($_comment) . '"';
		$exec = exec($cmd);
	}
	
?>

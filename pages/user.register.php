<?php
require('init-mvc.php');
$cook = $tmvc->config['session']['cookie_name'];
$_cook = $_COOKIE[$cook];
?>

<div style="font-size: 14px;" id="reg_form">
<div class="geo-warn"><table width="100%"><tr><td><img id="reg_src_img" src="images/info.png" alt="warning" style="padding: 0 10px" /></td><td id="reg_msg" style="text-align: left;font-size: 13px">If you already have Facebook, Twitter or Google Mail account - proceed to <b style="border-bottom: 1px dashed #aaa;cursor: pointer" onclick="Sitis.login_user();">Sign In</b></td></tr></table></div><br/>
<table width="100%"><tr>
<td>Login:</td><td style="text-align: right"><input id="reg_lgn" maxlength="40" type="text" class="reg_inp" autocomplete="off"><br/></td>
</tr><tr>
<td>E-mail:</td><td style="text-align: right"><input id="reg_email" maxlength="200" type="text" class="reg_inp" autocomplete="off"></td>
</tr><tr>
<td>Password:</td><td style="text-align: right"><input id="reg_pwd" maxlength="40" type="password" class="reg_inp" autocomplete="off"></td>
</tr><tr>
<td>Confirm password:</td><td style="text-align: right"><input id="reg_pwd_confirm" maxlength="40" type="password" class="reg_inp" autocomplete="off"><input type="text" id="reg_sess_id" style="display: none" value="<?php echo $_COOKIE[$cook]; ?>"></td>
</tr>
</table>
<div id="cptch"></div>
<br/>
<div style="text-align: right;">
	<input type="button" class="qq-upload-button reg_ok" style="padding: 10px 20px" value="Register" onclick="Sitis.check_sign_up();">
	<input type="button" class="qq-upload-button reg_cancel" style="padding: 10px 20px" value="Cancel" onclick="$('register_user').retrieve('instance').close();">
</div>

</div>

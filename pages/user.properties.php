<?php

require('init-mvc.php');
$cook = $tmvc->config['session']['cookie_name'];

if(urldecode($_POST['token']) == $_COOKIE[$cook]){
	if(isset($_POST['share'])){
		
		$array = $_POST;
		if(!$tmvc->apply_var($array)) exit('Error saving');
		
	} else {
	
	$data = $tmvc->get_user($_COOKIE[$cook]);
	if($data){
	if(!empty($data['is_public'])) $prefix = 'checked="checked"';
	
$content = <<<ZTML
<input type="checkbox" id="prop_share" {$prefix}> Share your account<br/><br/><input type="button" class="qq-upload-button reg_ok" style="padding: 10px 20px" value="Apply" onclick="Sitis.apply_properties();"><input type="button" class="qq-upload-button reg_cancel" style="margin-left: 10px;padding: 10px 20px" value="Cancel" onclick="$(\'user_prop\').retrieve(\'instance\').close();">
ZTML;

$out = <<<HTML
<script type="text/javascript">
new MUI.Window({
	id: 'user_prop',
	title: 'User properties',
	width: 200,
	height: 120,
	content: '{$content}',
	padding: {top: 12, right: 12, bottom: 10, left: 12},
	resizable: false,
	maximizable: false
});
</script>
HTML;
		
		} else {
		
$out = <<<HTML
<script type="text/javascript">
Sitis.login_user();
</script>
HTML;
		
		}
		
	echo $out;
	}
} else {exit('Wrong session');}

?>

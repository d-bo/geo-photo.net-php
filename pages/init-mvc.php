<?php

define('TMVC_BASEDIR','../mvc/');
define('TMVC_I18N_DIR', '../i18n/');
define('TMVC_ERROR_HANDLING', 1);
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
if(!defined('TMVC_BASEDIR')) define('TMVC_BASEDIR',dirname(__FILE__) . DS . '..' . DS . 'tinymvc' . DS);
require(TMVC_BASEDIR . 'sysfiles' . DS . 'TinyMVC.php');
$tmvc = new tmvc();

?>

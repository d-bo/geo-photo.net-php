<?php
	
	/* propertiesw tabs are in pages/test-tabs.php */
	
	require('init-mvc.php');
	require('exif.php');
	
	if(isset($_GET['uri']) || !empty($_GET['uri'])) $temp_folder = $tmvc->config['temp_folder']; else $temp_folder = $tmvc->get_user_folder();
	$lang_array = $tmvc->loadLang($tmvc->browser_lang);
	
	$dir = $tmvc->config['root_path'] . $temp_folder;
	$it = new DirectoryIterator($dir);
	foreach($it as $file){
		$_file = str_ireplace($dir, '', $file);
		break;
	}
	
	if(isset($_GET['uri']) || !empty($_GET['uri'])) $_file = $_GET['uri'].'.jpg';
	
	if(isset($_POST['img']) && !empty($_POST['img'])){
		$jpeg_path = $tmvc->config['root_path'] . $temp_folder . $_POST['img'];		
	} else {
		$jpeg_path = $tmvc->config['temp_folder'] . $_file;
	}
	
	if(!is_file($jpeg_path)) exit('<div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">Empty file folder</td></tr></table></div>');
	
	$exec_path = $tmvc->config['exec_path'];
	$data_app2 = getExifAPP2Data($exec_path, $jpeg_path);
	$data_app1 = @read_exif_data($jpeg_path);
	
	//if(round($data_app2['Latitude']) < 1  && round($data_app2['Longitude']) < 1) exit('<div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">No geo data</td></tr></table></div>');
	if(is_array($data_app2)){
		foreach($data_app2 as $key => $value){
			
			switch($key){
				case(stristr($key, 'user')):
					$user_array[$key] = $value;
					break;
				case(!stristr($key, 'user') && !stristr($key, 'result')):
					$_array[$key] = $value;
			}
		}
	}
	
	$lambda = function($array, $l_array)
	{
		$out = '<div id="scroll"><table width="100%" style="line-height: 10px;font-size: 10px">';
		if(is_array($array)){
			foreach($array as $zey => $value){
				
				$trim = str_replace('Result', '', $zey);
				$_trim = str_replace('User', '', $trim);
				$key = $_trim;
				
				if(!stristr('Comments', $key)){
					
					$_value = htmlspecialchars($value);
					foreach($l_array as $key1 => $value1){
						if($key == $key1) {$_key = $value1; break;} else {$_key = htmlspecialchars($key);}
					}
				}
				if($_key != 'Camera') $_prefix = '&#176;';
				if($_key == 'Version') $_prefix = '';
				$out .= '<tr><td style="width: 50%">' . $_key . '</td><td style="width: 50%"><input style="font-size: 10px;" type="text" value="' . $_value . $_prefix . '" readonly></td></tr>';
			}
		}
		$out .= '</table></div>';
		return $out;
	};
	
	if(isset($_POST['code']) && !empty($_POST['code'])){
		
		switch($_POST['code']){
			
			default:
				echo $lambda($_array, $tmvc->config['lang_array']);
				break;
			case(1):
				echo $lambda($user_array, $tmvc->config['lang_array']);
				break;
			case(2):
				echo $lambda($result_array, $tmvc->config['lang_array']);
				break;
		}
		
	} else {echo $lambda($_array, $tmvc->config['lang_array']);}
	
?>

<div id="scrollbar1" class="scrollbar-vert" style="position: absolute;right: 0"><div id="handle1" class="handle-vert"></div></div>

<script type="text/javascript">

	//Sitis.makeScrollbar( $('properties'), $('scroll'), $('scrollbar1'), $('handle1') );

</script>


<div id="container">

<?php

require('init-mvc.php');
$dir = $tmvc->config['root_path'] . $tmvc->get_user_folder();

function dirToArray($dir){
	$it = new RecursiveDirectoryIterator($dir);
	$count = 0;
	foreach(new RecursiveIteratorIterator($it) as $file){
		if(stristr($file, '.jpg') || stristr($file, '.jpeg') || stristr($file, '.gjpg')){
			$_file = str_ireplace($dir, '', $file);
			$files[] =  $_file;
			if($count == 0){
				if(is_file($file)) $flag = 1; else $flag = 0;
			}
			$count++;
		}
	}
	return array('files' => $files, 'flag' => $flag);
}

$files = dirToArray($dir);
if(!empty($files['files'])){
	sort($files['files']);
	$quality = 40;
	$w = $h = 55;
	foreach($files['files'] as $key => $value){
		echo '<img alt="pic" id="' . $value . '_pr" onclick="Sitis.loadImg(\'' . $value . '\');" data-src="files/?src=' . $value . '&w=' . $w . '&h=' . $h . '&zc=l&q=' . $quality . '&hash=' . sha1($value) . '" class="thumbs" width="' . $w . 'px" height="' . $h . 'px"/>';
	}
} else {
	echo '<div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">Empty file folder</td></tr></table></div>';
}

?>

</div>

<script type="text/javascript">

var lazyloader = new LazyLoad({
	range: 50,
    realSrcAttribute: "data-src",
    useFade: true,
    elements: 'img',
    container: $('mochaConsole')
});

</script>

<?php

require('init-mvc.php');
$dir = $tmvc->config['root_path'] . $tmvc->get_user_folder();

function dirToArray($dir){
	$it = new RecursiveDirectoryIterator($dir);
	$count = 0;
	foreach(new RecursiveIteratorIterator($it) as $file){
		if(stristr($file, '.jpg') || stristr($file, '.jpeg') || stristr($file, '.gjpg')){
			$_file = str_ireplace($dir, '', $file);
			$files[] =  $_file;
			if($count == 0){
				if(is_file($file)) $flag = 1; else $flag = 0;
			}
			$count++;
		}
	}
	return array('files' => $files, 'flag' => $flag);
}

function showPic($dir, $page, $num)
{
	$files = dirToArray($dir);
	$h = 65;
	$w = 65;
	$out = '<div style="padding-top: 10px"><table width="100%"></td>';
	if($page > 1) $start_item = ($page - 1) * $num; else $start_item = 0;
	
	// determine last page
	if(($start_item+$num) >= count($files['files'])){
		echo '<script type="text/javascript">$(\'gallery_next\').removeEvent(\'click\', pr_prev);</script>';
	};
	if($start_item > count($files['files'])){
		exit('not in page');
	} else {
		// needed files in array
		for($b=$start_item;$b<=($start_item + $num);$b++){
			if(!empty($files['files'][$b])) $_files[] = $files['files'][$b];
		}
		//exit("$start_item");
		$c = count($_files);
		//exit("$c");
			//print_r($_files);
			//exit("$c");
			if($files['flag'] > 0){
				foreach($_files as $key => $value){
					$out .= '<td class="td_1" width="' . 100 / $num . '%"><div width="100%" id="' . $value . 'x' . '" onclick="Sitis.loadImg(\'' . $value . '\');"><img class="td_img" width="' . $h . 'px" src="files/?src=' . $value . '&w=' . $w . '&h=' . $h . '&zc=l" /></div></td>';
				}
			} else {
				echo '<div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">Empty file folder</td></tr></table></div>';
			}
			
		$out .= '</tr></table></div>';
		return $out;
	}
}

if(isset($_POST['page']) && !empty($_POST['page'])){
	echo showPic($dir, $_POST['page'], 7);
} else {
	echo showpic($dir, 1, 7);
}

?>

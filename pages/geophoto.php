<?php

include('init-mvc.php');
$temp_folder = $tmvc->config['temp_folder'];

// init properties
if(isset($_GET['uri']) || !empty($_GET['uri'])) $temp_path = $temp_folder; else $temp_path = $tmvc->get_user_folder();
$thumb_path = 'files/';
$jpg_width = trim($_COOKIE['_imgx'], 'px');
$jpg_height = trim($_COOKIE['_imgy'], 'px');
$jpg_quality = 85;
// ajax
if(isset($_POST) && !empty($_POST)){
	
	if(!empty($_POST['img'])){
		
		$img_path = $thumb_path . '?src=' . $_POST['img'] . '&hp=' . $jpg_height . '&wl=' . $jpg_width . '&q=' . $jpg_quality;
		echo '<div style="text-align: center;overflow: hidden;padding: 0;margin: 0" id="img_inject">';
		echo '</div>';
		
	} else {
		
		echo '<div style="text-align: center">No images uploaded</div>';
		
	}
	
} else {
	
	// single file mode
	if(isset($_GET['uri']) || !empty($_GET['uri'])){
		$_file = $_GET['uri'] . '.jpg';
	} else {
		$dir = $tmvc->config['root_path'] . $tmvc->get_user_folder();
		$it = new DirectoryIterator($dir);
		foreach($it as $file){
			$_file = str_ireplace($dir, '', $file);
			break;
		}
	}
	if(is_file($file)){
		$img_path = $thumb_path . $_file;
		echo '<div style="text-align: center;" id="img_inject">';
		echo '</div>';
	} else {exit('123');}
}

?>

<script type="text/javascript">
	
	hImg = new Image();
	hImg.src = "http://<?php echo $tmvc->config['domain'] . DS . $img_path;?>";
	hImg.id = 'central_photo';
	hImg.onLoad = Sitis.loading();
	$('mainPanel').setStyle('text-align', 'center');
	$('mainPanel').setStyle('overflow', 'hidden');
	if($('central_photo')) $('central_photo').src = hImg.src; else $('mainPanel').adopt(hImg);
	
</script>


<?php
	
	require('init-mvc.php');
	require('exif.php');
	
	// take first file from dir
	//$dir = '/home/php5-3/geoview.sitis.mobi/www/_jpeg/';
	$dir = $tmvc->config['root_path'] . $tmvc->get_user_folder();
	$it = new DirectoryIterator($dir);
	//print_r($it);
	
	/* get first file in user dir, set flag=1 if file exists */
	foreach($it as $file){
		if(stristr($file, '.jpg') || stristr($file, '.jpeg') || stristr($file, '.gjpg')){
			if(is_file($file)) $flag = 1; else $flag = 0;
			$_file = str_ireplace($dir, '', $file);
			break;
		}
	}
	
	if(isset($_POST['img']) && !empty($_POST['img'])) $jpeg_path = $dir . $_POST['img']; else $jpeg_path = $dir . $_file;
	$exec_path = '/home/php5-3/geo-photo.net/www/exif/exifer';
	
	$i = 0;
	$columns = 3;
	$table_width = ceil(960 / $columns);
	
	if(!empty($_COOKIE['_gzoom']) && isset($_COOKIE['_gzoom'])) $gzoom = $_COOKIE['_gzoom']; else $gzoom = 16;
	if(!empty($_COOKIE['_yzoom']) && isset($_COOKIE['_yzoom'])) $yzoom = $_COOKIE['_yzoom']; else $yzoom = 16;
	if(!empty($_COOKIE['_bingzoom']) && isset($_COOKIE['_bingzoom'])) $bingzoom = $_COOKIE['_bingzoom']; else $bingzoom = 16;
	if(!empty($_COOKIE['_osmzoom']) && isset($_COOKIE['_osmzoom'])) $osmzoom = $_COOKIE['_osmzoom']; else $osmzoom = 16;
	
	$out = getExifAPP2Data($exec_path, $jpeg_path);
	$data_app2 = function($needle) use ($out)
	{
		if(!empty($out[$needle])) return $out[$needle]; else return '0';
	};
	
	$out1 = @read_exif_data($jpeg_path);
	$data_app1 = function($needle) use ($out1)
	{
		if(!empty($out1[$needle])) return $out1[$needle]; else return '0';
	};
	
?>

<script type="text/javascript">

var moffset = Cookie.read('_moffset');
var mnumarcs = Cookie.read('_mnumarcs');

if(moffset == null || moffset == '') {var moffset = MUI.Windows.windowOptions.markerOffset; Cookie.write('_moffset', moffset, {duration: 1});}
if(mnumarcs == null || mnumarcs == '') {var mnumarcs = MUI.Windows.windowOptions.markerNumArcs; Cookie.write('_mnumarcs', mnumarcs, {duration: 1});}

var img = "<?php if(isset($_POST['img']) && !empty($_POST['img'])) echo $_POST['img']; else echo $_file; ?>";

var lat = <?php echo $data_app2('Latitude') ?>;
var lng = <?php echo $data_app2('Longitude') ?>;

var azimuth = <?php echo $data_app2('Azimuth') ?>;
var pitch = <?php echo $data_app2('Pitch') ?>;
var roll = <?php echo $data_app2('Roll') ?>;

var h_angle = <?php echo $data_app2('HViewAngle') ?>;
var v_angle = <?php echo $data_app2('VViewAngle') ?>;
var orient = <?php echo $data_app1('Orientation') ?>;
var gzoom = <?php echo $gzoom; ?>;
var yzoom = <?php echo $yzoom; ?>;
var bingzoom = <?php echo $bingzoom; ?>;
var osmzoom = <?php echo $osmzoom; ?>;
p_t = 90 - Math.atan(0.5) * 180 / Math.PI;
image_rotation();
<?php

$comments = $data_app2('Comments');
$_comments = $comments;
if($_comments == '0' || ($_comments == '') || empty($_comments)) echo 'var comments = \'0\';'; else echo "var comments = \"".$_comments."\";";

?>

var start = new google.maps.LatLng(lat, lng);

if(window.rotation == 90 || window.rotation == 270) angle_src = v_angle; else angle_src = h_angle;

var end_angle = azimuth + (angle_src / 2);

var line_length = (moffset * mnumarcs) + 10;
var end_l = new google.maps.geometry.spherical.computeOffset(start, line_length, end_angle);

// begin
var begin_angle = azimuth - (angle_src / 2);
var end_l1 = new google.maps.geometry.spherical.computeOffset(start, line_length, begin_angle);

// azimuth
var end_l2 = new google.maps.geometry.spherical.computeOffset(start, line_length, azimuth);

// arc radius X coords
var arc_rad = new google.maps.geometry.spherical.computeOffset(start, 20, 0);

if(mnumarcs != '0'){
	
	var arc = [];
	
	for(i = 0; i <= mnumarcs; i++){
		k = 0;
		arc[i] = [];
		var offset = i * moffset;
		for(z = begin_angle; z <= end_angle; z++){
			arc[i][k] = new google.maps.geometry.spherical.computeOffset(start, offset, z);
			k++;
		}
	}
}

var coord = [start, end_l];
var coord1 = [start, end_l1];
var coord2 = [start, end_l2];

function image_rotation()
{
	window.rotation = 0;
	if(Math.abs(pitch) <= p_t)
	{
		_roll = Math.round(roll);
		if(_roll < 0) _roll += 360;
		//alert(_roll);
		_roll = Math.floor((_roll + 45) / 90) * 90;
		//alert(_roll);
		window.rotation = _roll % 360;
	}
}

function de_ra(de)
{
	var pi = Math.PI;
	var de_ra = (de)*(pi/180);
	return de_ra;
}

function arc(center, radius, startAngle, endAngle)
{
	angle = startAngle;
	coords = toCoords(center, radius, angle);
	path = "M " + coords[0] + " " + coords[1];
	while(angle <= endAngle){
		coords = toCoords(center, radius, angle);
		path += " L " + coords[0] + " " + coords[1];
		angle += 1;
	}
	
	return path;
}

function toCoords(center, radius, angle)
{
	var radians = (angle/180) * Math.PI;
	var x = center[0] + Math.cos(radians) * radius;
	var y = center[1] + Math.sin(radians) * radius;
	return [x, y];
}

</script>

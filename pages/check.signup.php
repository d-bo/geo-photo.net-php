<?php

	require('init-mvc.php');
	require('exif.php');

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	// lgn, email, pwd, check
	if(isset($_POST['lgn']) && isset($_POST['email']) && isset($_POST['pwd']) && isset($_POST['check'])){
		
		require('init-mvc.php');
		// CSRF check
		$cook = $tmvc->config['session']['cookie_name'];
		$_cook = $_COOKIE[$cook];
		if($_cook != $_COOKIE[$cook]) exit('Error');
		
		foreach($_POST as $key => $value){
			if(!is_numeric($key)){
				$data[$key] = array('str' => urldecode($_POST[$key]), 'count' => strlen($_POST[$key]));
			} else {$data[$key] = $value;}
		}
		$error = array();
		
		// further check
		if($data[0] < 4 || $data[0] > 20 || empty($_POST['lgn'])) $error['lgn'] = 1;
		if(!$tmvc->is_valid_email($data['email']['str']) || empty($_POST['email'])) $error['email'] = 1;
		if($data[2] < 4 || $data[2] > 20 || empty($_POST['pwd'])) $error['pwd'] = 1;
		if($_POST['pwd'] != $_POST['check']) $error['check'] = 1;
		$ch = $tmvc->check_lgn_email($data['lgn']['str'], $data['email']['str']);
		if($ch['lgn'] > 0 || $ch['email'] > 0) {$error['double_login'] = $ch['lgn']; $error['double_email'] = $ch['email'];}
		if(count($error) > 0) echo json_encode($error); else {
			
			$_activate = rand(3892, 15988);
			$activate  = sha1($_activate);
			$session_token = sha1(uniqid(microtime()) . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
			// create user with activation code
			$record = $tmvc->record_user($data['lgn']['str'], $data['email']['str'], $data['pwd']['str'], $session_token, $activate);
			if($record){
				
				// send email
				require_once('../plugins/phpmailer/class.phpmailer.php');

				$mail             = new PHPMailer();
				
				$body             = '<div style="font-size: 16px">Follow the link to activate your acoount: <br><br>http://geo-photo.net/activate/?token=' . urlencode($activate) . '</div>';
				
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = "mail.sitis.ru"; // SMTP server
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
														   // 1 = errors and messages
														   // 2 = messages only
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				//$mail->Host       = "mail.yourdomain.com"; // sets the SMTP server
				//$mail->Port       = 26;                    // set the SMTP port for the GMAIL server
				$mail->Username   = "license@sitis.ru"; // SMTP аккаунт
				$mail->Password   = "1a2s3d4f";
				
				$mail->SetFrom('license@sitis.ru', 'Sitis');
				
				//$mail->AddReplyTo("name@yourdomain.com","First Last");
				
				$mail->Subject    = "GeoPhoto. Account activation.";
				
				$mail->AltBody    = "Follow the link to activate your account: \r\rhttp://geo-photo.net/activate/?token=" . urlencode($activate); // optional, comment out and test
				
				$mail->MsgHTML($body);
				
				$address = $data['email']['str'];
				$mail->AddAddress($address, $address);
				
				//$mail->AddAttachment("images/phpmailer.gif");      // attachment
				//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
				
				$mail->send();
				echo '0';
				/*
				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} else {
				  echo "Message sent!";
				}
				*/
				
			} else {echo $record;}
			
		}
	}
}

?>

<?php

/**
 * application.php
 *
 * application configuration
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

/* URL routing, use preg_replace() compatible syntax */
$config['routing']['search'] =  array();
$config['routing']['replace'] = array();
$config['lang_cookie'] = '_lng';

/* set this to force controller and method instead of using URL params */
$config['root_controller'] = 'default';
$config['root_action'] = 'index';
//$config['root_path'] = '/home/php5-3/geoview.sitis.mobi/www/';
$config['domain'] = 'geo-photo.net';
$config['root_path'] = '/usr/share/geoview/';
$config['exec_path'] = '/home/php5-3/geo-photo.net/www/exif/exifer';
$config['user_folder'] = '_jpeg/';
$config['temp_folder'] = '_temp/';

/* name of default controller/method when none is given in the URL */
//$config['default_controller'] = 'default';
//$config['default_action'] = 'index';

/* name of PHP function that handles system errors */
$config['error_handler_class'] = 'TinyMVC_ErrorHandler';

/* enable timer. use {TMVC_TIMER} in your view to see it */
$config['timer'] = true;
$config['db_prefix'] = 'geophoto';
$config['cookie_prefix'] = '_mobile';
$config['base_href'] = 'http://geo-photo.net/';
$config['self_provider'] = 'geophoto';

// pgqsql connection
$config['pgsql']['dbname'] = 'geophoto';
$config['pgsql']['host'] = '127.0.0.1';
$config['pgsql']['username'] = 'postgres';
$config['pgsql']['password'] = 'qazwsx';

// cookie, sessions
$config['session']['cookie_name'] = '_trace';
$config['account_cookie'] = '_view';

/* OAUTH config */
/* twitter */
$config['consumer_key'] = 'GGGQV0hEvvP4NzaDQZhQ';
$config['consumer_secret'] = 'UZgAQVP1UZgd5hgINLyW8rRQ0QeO1QVyCxTzY5A38';
$config['consumer_secret'] = 'UZgAQVP1UZgd5hgINLyW8rRQ0QeO1QVyCxTzY5A38';
$config['access_token'] = '274868795-5fQWQ4Ru8kO6d5WaYrlwUU9AvvNOdgeMQqSMKioT';
$config['access_token_secret'] = 'c05oM7UTzbQ8LR08BWv3CIUXHvMIfCaeGzweb6Cy59A';

/* google */
$config['GOOGLE_CONSUMER_KEY'] = "468588326530.apps.googleusercontent.com";
$config['GOOGLE_CONSUMER_SECRET'] = "0uMVv7V0eeRbCumkcLyjXYNo";
$config['GOOGLE_OAUTH_HOST'] = "https://www.google.com";
$config['GOOGLE_REQUEST_TOKEN_URL'] = $config['GOOGLE_OAUTH_HOST'] . "/accounts/OAuthGetRequestToken";
$config['GOOGLE_AUTHORIZE_URL'] = $config['GOOGLE_OAUTH_HOST'] . "/accounts/OAuthAuthorizeToken";
$config['GOOGLE_ACCESS_TOKEN_URL'] = $config['GOOGLE_OAUTH_HOST'] . "/accounts/OAuthGetAccessToken";

// map keys
$config['GOOGLE_MAPS_KEY'] = 'ABQIAAAAaxKQDuhR-NZbDdVNthBcixTcguG23h8Ot03Mb3nVx7eBsPkVLBSCS-dWy54kkJ3xSEkmpj1qPVtzrg';
$config['YANDEX_MAPS_KEY'] = 'AAoXuk4BAAAAqZD_HwIAuLChp3Tt_Iecx_X4hhZGfHKbOQMAAAAAAAAAAACazl8djCaoV_lgiqBCAZFDNsJkgA==';

/* facebook */
$config['facebook']['app_id'] = '280216158685135';
$config['facebook']['secret'] = 'f8402fa713622c0b3dd075473dc8a7d3';

?>

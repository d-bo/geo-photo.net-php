<?php

/**
 * default.php
 *
 * default application controller
 * @author Dima
 */

class Default_Controller extends TinyMVC_Controller
{
	private $single_file;
	
	function index()
	{
		$error = array();
		ob_start();
		$tmvc = tmvc::instance();
		$this->load->model('Page_Model', 'page');
		$this->load->library('smarty_wrapper', 'smarty');
		$this->load->library('exif', 'exif');
		$this->load->library('crypt', 'crypto');
		$session_cookie = $tmvc->config['session']['cookie_name'];
		if(isset($tmvc->url_segments[1]) || !empty($tmvc->url_segments[1])) $this->smarty->assign('url_segments', $tmvc->url_segments[1]);
		
		/**
		 * account activation
		 */
		
		if($tmvc->url_segments[1] == 'activate' && !empty($_GET['token'])){
			$token = urldecode($_GET['token']);
			if($this->page->check_token($token)) header("Location: http://" . $tmvc->config['domain']); else $error = 'Wrong activation code';
		}
		
		/* start self engine session */
		if($this->page->start_session()) $this->smarty->assign('IS_AUTH', 1);
		
		/* redirect to IDxxx */
		if(empty($tmvc->url_segments[1]) && !empty($_COOKIE[$session_cookie])){
			$data = $this->page->get_user($_COOKIE[$session_cookie]);
			if($data) header("Location: http://" . $tmvc->config['domain'] . '/id' . $data['geo_id']);
		}
		
		/* is the user ID account SHARED while the session token is not registered */
		if(stristr($tmvc->url_segments[1], 'id') && !empty($_COOKIE[$session_cookie])){
			$id = str_ireplace('id', '', $tmvc->url_segments[1]);
			$data = $this->page->get_user($_COOKIE[$session_cookie]);
			if($data){
				if($data['geo_id'] != $id) header("Location: http://" . $tmvc->config['domain'] . '/id' . $data['geo_id']);
			} else {
				/* ajax PATH_INFO pushed in cookie */
				$check = $tmvc->get_token_by_id();
				if($check) setcookie($tmvc->config['account_cookie'], $check, time() + 36000, '/', '.' . $tmvc->config['domain']);
				/* account not shared */
				if(!$tmvc->is_public($id)) $error = 'Private account';
			}
		}
		
		@$this->smarty->assign('session_id', $_COOKIE[$session_cookie]);
		
		/**
		 * twitter oauth block
		 */
		
		/* twitter start oauth */
		if($tmvc->url_segments[1] == 'twitter'){
			require('plugins/twitter_api/twitteroauth/twitteroauth.php');
			$connect = new TwitterOAuth($tmvc->config['consumer_key'], $tmvc->config['consumer_secret']);
			$request_token = $connect->getRequestToken('http://' . $tmvc->config['domain'] . '/twitter_request_token/');
			setcookie("twitter_oauth_token", $request_token['oauth_token'], time() + 36000, '/', '.' . $tmvc->config['domain']);
			setcookie("twitter_oauth_token_secret", $request_token['oauth_token_secret'], time() + 36000, '/', '.' . $tmvc->config['domain']);
			$token = $request_token['oauth_token'];
			switch($connect->http_code){
				case 200:
				$url = $connect->getAuthorizeURL($token);
				header('Location: ' . $url);
				break;
				
				default:
				throw new Exception('Could not connect to Twitter. Refresh the page or try again later.');
			}
		}
		
		/* twitter clear sessions */
		if($tmvc->url_segments[1] == 'twitter_clear_sessions'){
			session_start();
			session_destroy();
			setcookie("twitter_oauth_token", $request_token['oauth_token'], time() - 36000, '/', '.' . $tmvc->config['domain']);
			setcookie("twitter_oauth_token_secret", $request_token['oauth_token_secret'], time() - 36000, '/', '.' . $tmvc->config['domain']);
			header('http://' . $tmvc->config['domain']);
		}
		
		/* twitter get credentials & redirect "signed in" user */
		if($tmvc->url_segments[1] == 'twitter_request_token'){
			require('plugins/twitter_api/twitteroauth/twitteroauth.php');
			if(isset($_REQUEST['oauth_token']) && $_COOKIE['twitter_oauth_token'] !== $_REQUEST['oauth_token']){
				//$_SESSION['oauth_status'] = 'oldtoken';
				header('http://' . $tmvc->config['domain'] . '/twitter_clear_sessions');
			}
			$connection = new TwitterOAuth($tmvc->config['consumer_key'], $tmvc->config['consumer_secret'], $_COOKIE['twitter_oauth_token'], $_COOKIE['twitter_oauth_token_secret']);
			$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
			$_SESSION['access_token'] = $access_token;
			if(200 == $connection->http_code){
				$_SESSION['status'] = 'verified';
				$content   = $connection->get('account/verify_credentials');
				$twit_id   = $content->id;
				$twit_name = $content->name;
				$this->page->create_user($twit_id, $twit_name, 'twitter');
				//echo $twit_id;
				echo '<script type="text/javascript">window.opener.window.location.reload(); self.close();</script>';
				exit();
				//header('http://' . $tmvc->config['domain']);
			} else {
				header('http://' . $tmvc->config['domain'] . '/twitter_clear_sessions');
			}
		}

		/**
		 * exit
		 */
		
		/* exit global */
		if($tmvc->url_segments[1] == 'exit'){
			if(isset($_COOKIE[$session_cookie]) && !empty($_COOKIE[$session_cookie])){
				$_data = $this->page->get_user($_COOKIE[$session_cookie]);
				switch($_data['provider']){
					
					// facebook
					case "facebook":
						@setcookie($tmvc->config['session']['cookie_name'], '', time() - 360000, '/', $tmvc->config['domain']);
						require 'plugins/facebook/facebook.php';						
						$facebook = new Facebook(array(
							'appId'  => $tmvc->config['facebook']['app_id'],
							'secret' => $tmvc->config['facebook']['secret'],
						));
						$user = $facebook->getUser();
						if($user){
							try {
								$user_profile = $facebook->api('/me');
							} catch (FacebookApiException $e){
								echo '<pre>'.htmlspecialchars(print_r($e, true)).'</pre>';
								$user = null;
							}
						}
						
						$params = array('next' => 'http://'. $tmvc->config['domain']);
						if(isset($user_profile)){
							header("Location: " . $facebook->getLogoutUrl($params));
						} else {throw new Exception('facebook exit error');}
						break;
						
					// twitter
					case "twitter":
						@setcookie($tmvc->config['session']['cookie_name'], '', time() - 360000, '/', $tmvc->config['domain']);
						echo '<script src="http://platform.twitter.com/anywhere.js?id=GGGQV0hEvvP4NzaDQZhQ&v=1" type="text/javascript"></script>';
						echo '<script type="text/javascript">twttr.anywhere.config({ callbackURL: "http://' . $tmvc->config['domain'] . '" }); twttr.anywhere.signOut();</script>';
						break;
						
					// google
					case "google":
						@setcookie($tmvc->config['session']['cookie_name'], '', time() - 360000, '/', $tmvc->config['domain']);
						break;
						
					// geophoto exit
					case "geophoto":
						@setcookie($tmvc->config['session']['cookie_name'], '', time() - 360000, '/', $tmvc->config['domain']);
						break;
				}
			}
			
			@setcookie("PHPSESSID", '', time() - 360000, '/', '.' . $tmvc->config['domain']);
			@setcookie("twitter_oauth_token",'', time() - 360000, '/', '.' . $tmvc->config['domain']);
			@setcookie("twitter_oauth_token_secret", '', time() - 360000, '/', '.' . $tmvc->config['domain']);
			header('Location: http://' . $tmvc->config['domain']);
		}
		
		/**
		 * download geophoto
		 */
		
		/* download user request */
		if(isset($_GET['c']) || !empty($_GET['c'])){
			if($this->page->get_user($_COOKIE[$session_cookie])){
				$path = $tmvc->config['root_path'] . $tmvc->config['user_folder'] . $_GET['c'];
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename=' . $_GET['c']);
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . filesize($path));
				ob_clean();
				flush();
				readfile($path);
				exit;
			} else {echo '<script type="text/javascript">Sitis.register_user();</script>';}
		}
		
		/* delete photo request */
		if(isset($_GET['cd']) || !empty($_GET['cd'])){
			if($this->page->get_user($_COOKIE[$session_cookie])){
				echo '<script type="text/javascript">alert("ok");</script>';
			} else {echo '<script type="text/javascript"></script>';}
		}
		
		/* .jpeg output */
		if($tmvc->url_segments[1] == 'files'){
			if(isset($_GET['src']) && !empty($_GET['src'])){
				require('plugins/phpthumb' . DS . 'phpThumb.php');
			}
			exit();
		}
		
		/* global auth check */
		if(isset($_COOKIE[$session_cookie]) && !empty($_COOKIE[$session_cookie]) && stristr($tmvc->url_segments[1], 'id')){
			$user_info = $this->page->get_user($_COOKIE[$session_cookie]);
			if($user_info){
				$_user = array(
					'LOGGED_IN' => 1,
					'USER_NAME' => $user_info['name'],
					'EXIT_URL' => 'exit'
				);
				$this->smarty->assign($_user);
			} else {
				$this->smarty->assign('LOGGED_IN' , 0);
			}
		} else {
			$this->smarty->assign('LOGGED_IN' , 0);
		}
		
		/**
		 * facebook oauth
		 */
		
		if($tmvc->url_segments[1] == 'facebook_login'){
			exit('facebook_login');
		}
		
		/* facebook sign in */
		if($tmvc->url_segments[1] == 'facebook'){
			require_once("plugins/facebook/facebook.php");
			
			$facebook = new Facebook(array(
				'appId'  => $tmvc->config['facebook']['app_id'],
				'secret' => $tmvc->config['facebook']['secret'],
				'cookie' => true
			));
			
			$user = $facebook->getUser();
			
			if($user){
			  try {
				$user_profile = $facebook->api('/me');
			  } catch (FacebookApiException $e){
					error_log($e->getType());
					error_log($e->getMessage());
			  }
			}

			if($user){
				$logoutUrl = $facebook->getLogoutUrl();			
			} else {
				$params = array('scope' => 'read_stream, friends_likes', 'redirect_uri' => 'http://' . $tmvc->config['domain'] . '/index.php/facebook');
				$loginUrl = $facebook->getLoginUrl($params);
			}
			
			if($user){
				$query = $this->page->create_user($user_profile['id'], $user_profile['name'], 'facebook');
				echo '<script type="text/javascript">window.opener.window.location.reload(); self.close();</script>';
				exit();
			} else {
				header("Location: " . $loginUrl);
				exit();
			}
		}
		
		/**
		 * google oauth
		 */
		
		/* google oauth */
		if($tmvc->url_segments[1] == 'google'){
			//exit('123');
			include_once "plugins/oauth/OAuthRequester.php";
			
			define("GOOGLE_CONSUMER_KEY", "468588326530.apps.googleusercontent.com");
			define("GOOGLE_CONSUMER_SECRET", "0uMVv7V0eeRbCumkcLyjXYNo");
			
			define("GOOGLE_OAUTH_HOST", "https://www.google.com");
			define("GOOGLE_REQUEST_TOKEN_URL", GOOGLE_OAUTH_HOST . "/accounts/OAuthGetRequestToken");
			define("GOOGLE_AUTHORIZE_URL", GOOGLE_OAUTH_HOST . "/accounts/OAuthAuthorizeToken");
			define("GOOGLE_ACCESS_TOKEN_URL", GOOGLE_OAUTH_HOST . "/accounts/OAuthGetAccessToken");
			
			define('OAUTH_TMP_DIR', function_exists('sys_get_temp_dir') ? sys_get_temp_dir() : realpath($_ENV["TMP"]));

			$options = array(
				'consumer_key' => GOOGLE_CONSUMER_KEY, 
				'consumer_secret' => GOOGLE_CONSUMER_SECRET,
				'server_uri' => GOOGLE_OAUTH_HOST,
				'request_token_uri' => GOOGLE_REQUEST_TOKEN_URL,
				'authorize_uri' => GOOGLE_AUTHORIZE_URL,
				'access_token_uri' => GOOGLE_ACCESS_TOKEN_URL
			);
			
			OAuthStore::instance("Session", $options);
			
			try
			{
				if(empty($_GET["oauth_token"]))
				{
					$getAuthTokenParams = array(
						'scope' => 'http://www-opensocial.googleusercontent.com/api/people/ https://www.googleapis.com/auth/userinfo#email',
						'xoauth_displayname' => 'geo-photo.net',
						'oauth_callback' => 'http://' . $tmvc->config['domain'] . '/index.php/google/'
					);
					
					$tokenResultParams = OAuthRequester::requestRequestToken(GOOGLE_CONSUMER_KEY, 0, $getAuthTokenParams);
					header("Location: " . GOOGLE_AUTHORIZE_URL . "?oauth_token=" . $tokenResultParams['token']);
					
				} else {
					
					$oauthToken = $_GET["oauth_token"];
					$tokenResultParams = $_GET;
					
					try {
						OAuthRequester::requestAccessToken(GOOGLE_CONSUMER_KEY, $oauthToken, 0, 'POST', $_GET);
					}
					catch (OAuthException2 $e)
					{
						echo '<pre>';
						var_dump($e);
						echo '</pre>';
						exit('wrong oauth_token');
					}
					
					$request = new OAuthRequester("https://www-opensocial.googleusercontent.com/api/people/@me/@self", 'GET', $tokenResultParams);
					$result = $request->doRequest(0);
					if($result['code'] == 200){
						$parse = json_decode($result['body'], true);
						$query = $this->page->create_user($parse['entry']['id'], $parse['entry']['name']['formatted'], 'google');
						echo '<script type="text/javascript">window.opener.window.location.reload(); self.close();</script>';
						exit();
					} else {
						exit('Error request user profile.');
					}
				}
			}
			
			/* bad OAuthRequester request */
			catch(OAuthException2 $e){
				echo "OAuthException:  " . $e->getMessage();
				echo '<pre>';
				var_dump($e);
				echo '</pre>';
				exit();
			}
		}
		
		/* crypto key generate */
		//$this->crypto->crypt(3, "כלבה שובבה");
		//$ph = urlencode($this->crypto->encrypt('funny shit'));
		//$zh = urldecode($ph);
		//echo $this->crypto->decrypt($zh);
		//$crypt->encrypt("raw data to encrypt");
		//$crypt->decrypt("encrypted data to decrypt");
		
		$content = $this->page->get_page($tmvc->url_segments);
		
		/* assign global vars for mobile devices */
		if(!empty($tmvc->url_segments))
		{
			if(in_array('iphone', $tmvc->url_segments))
			{
				$this->smarty->assign('type', 'iphone');
			}
			
			if(in_array('android', $tmvc->url_segments))
			{
				$this->smarty->assign('type', 'android');
			}
		}
		
		/* global domain assign */
		$this->smarty->assign('global_domain', $tmvc->config['domain']);
		
		/* display image in single mode (i.e. geo-photo/?uri=http://path_to_image) */
		if(!empty($_GET['uri']) && isset($_GET['uri'])){
			$this->smarty->assign('mochainit', 'mocha-init-uri.js');
			$this->single_file = $this->page->get_file($_GET['uri'], $tmvc->config['temp_folder']);
			$this->smarty->assign('single_image', $this->single_file);
		} else {
			$this->smarty->assign('mochainit', 'mocha-init-cmpld.js');
		}
		
		if(!empty($tmvc->config['base_href'])) $this->smarty->assign('base_href', $tmvc->config['base_href']); else $this->smarty->assign('base_href', $_SERVER['HTTP_HOST'].'/');
		if(!empty($content['title'])) $this->smarty->assign('title', $content['title']);
		if(!empty($content['desc'])) $this->smarty->assign('desc', $content['desc']);
		
		/* language vars */
		if(!empty($tmvc->config['lang_array'])) $this->smarty->assign('lang', $tmvc->config['lang_array']);
		if(!empty($tmvc->config['exif_array'])) $this->smarty->assign('exif', $tmvc->config['exif_array']);
		if(!empty($tmvc->config['lang_options'])) $this->smarty->assign('lang_options', $tmvc->config['lang_options']);
		if(!empty($tmvc->config['lang_selected'])) $this->smarty->assign('lang_selected', $tmvc->config['lang_selected']);
		
		// gmaps key assign
		$this->smarty->assign('GOOGLE_MAPS_KEY', $tmvc->config['GOOGLE_MAPS_KEY']);
		$this->smarty->assign('YANDEX_MAPS_KEY', $tmvc->config['YANDEX_MAPS_KEY']);
		
		/* check if loaded with fucking IE (mochaUI scripts crashes) */
		if(!stristr($_SERVER['HTTP_USER_AGENT'], 'msie1')){
			if(count($error) < 1){
				// no errors, displaying template
				if(!empty($content['template'])) $this->smarty->display(TMVC_BASEDIR.'myapp/views/'.$content['template']);
			} else {
				// found errors, display "error.tpl"
				$this->smarty->assign('errors', $error);
				$this->smarty->display(TMVC_BASEDIR.'myapp/views/error.tpl');
			}
		} else {$this->smarty->display(TMVC_BASEDIR.'myapp/views/ie.not.supported.tpl');}
		
		ob_end_flush();
	}
}

?>

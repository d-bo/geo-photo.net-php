{literal}
<style>
a {color: green;text-decoration: none;border-bottom: 1px dashed #aaa}
a:hover {color: gray}
</style>
{/literal}
<body style="background: #f0f0f0;">
<div style="font-size: 38px;text-align: center;border: 1px solid #ccc;background: #ffffff;margin-top: 100px;padding: 20px 0">
Sorry, but there are several bugs with Microsoft Internet Explorer, so it is not supported yet.<br/>
Recommend you to download <a href="http://www.google.com/chrome?hl=ru">Google Chrome</a>, <a href="http://www.mozilla.org/firefox/">Mozilla Firefox</a>, <a href="http://www.opera.com/download/">Opera</a>.
<br/>
</div>
</body>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<title>Geo-photo.net</title>
	<link rel="stylesheet" type="text/css" href="themes/charcoal/css/Content.css" />
</head>
<body>
<div id="desktop">
	{if !isset($single_image)}
	<div id="desktopHeader">
		<div id="desktopNavbar">
			<ul>
				<li><a class="returnFalse">{$lang.menu_file}</a>
					<ul>
						<li><a href="#" onclick="if($('uploader')) document.getElementById('upload_button').click(); else Sitis.login_user();">Upoad image</a></li>
						<li class="divider"><a href="#" onclick="MochaUI.parametricsWindow();return false;">Marker properties</a></li>
						<li><a href="#" onclick="Sitis.user_properties();">User properties</a></li>
					</ul>
				</li>
				
				<li><a class="returnFalse">{$lang.menu_help}</a>
					<ul>
						<li><a href="#" onclick="Sitis.about_window();">About</a></li>
					</ul>
				</li>
			</ul>
			<div class="toolbox divider">
				<div id="spinnerWrapper"><div id="spinner" style="padding: 0 20px"></div></div>
			</div>
			
			<div class="toolbox divider">
			{if $LOGGED_IN eq '1'}
			<span style="display: inline-block;padding-top: 3px;text-align: center">{include file='logged.in.tpl'}</span>
			{else}
			<div style="width: 100px;" class="qq-upload-button" onclick="Sitis.login_user();"><table width="100%"><tr><td><img src="images/login.png" alt="sign in" /></td><td>Log In</td></tr></table></div>
			</div>
			<div class="toolbox divider">
			<div style="width: 100px;" class="qq-upload-button" onclick="Sitis.register_user();"><table width="100%"><tr><td><img src="images/register.png" alt="sign in" /></td><td>Sign Up</td></tr></table></div>
			{/if}
			</div>
			
		</div><!-- desktopNavbar end -->
	</div><!-- desktopHeader end -->
	{/if}
	<div id="dockWrapper">
		<div id="dock">
			<div id="dockPlacement"></div>
			<div id="dockAutoHide"></div>
			<div id="dockSort"><div id="dockClear" class="clear"></div></div>
		</div>
	</div>
	
	<div id="pageWrapper"></div>
	
	<div id="desktopFooterWrapper">
		<div id="desktopFooter">
			&copy; {$smarty.now|date_format:"%Y"} <a href="http://sitis.mobi" target="blank">Sitis Mobile</a>. Based on <a href="http://mochaui.org/">MochaUI</a> library. <a href="http://sitis.mobi">GeoCam AR Pro</a> available at <a href="https://market.android.com/details?id=ru.sitis.android.geocam">Android Market</a> and at <a href="http://itunes.apple.com/us/app/geocam-ar-pro/id476292765?mt=8">AppStore</a>. Send your bugs and messages to <span style="unicode-bidi: bidi-override;direction: rtl"><b>ten.otohp-oeg@troppus</b></span>
			<span style="padding-left: 20px"><a href="http://facebook.com/pages/Sitis-Mobile/226823990665233"><img src="images/icons/facebook.png" /></a></span>
			<span style="padding-left: 5px"><a href="http://twitter.com/sitismobile"><img src="images/icons/twitter.png" /></a></span>
		</div>
	</div>
	
</div><!-- desktop end -->

<script type="text/javascript">
/* js<->php vars */
var lang = {};
lang.file = '{$lang.file}';
lang.geophoto = '{$lang.geophoto}';
lang.preview = '{$lang.preview}';
lang.googleMap = '{$lang.googleMap}';
lang.yandexMap = '{$lang.yandexMap}';
lang.properties = '{$lang.properties}';
lang.filter = '{$lang.filter}';
lang.data = '{$lang.data}';
lang.user_data = '{$lang.user_data}';
lang.result_data = '{$lang.result_data}';
lang.no_comments = '{$lang.no_comments}';
lang.map = '{$lang.map}';
window.session_id = '{$session_id}';
{if isset($url_segments)}
window.url_segments = '{$url_segments}';
{else}
window.url_segments = '0';
{/if}
{if isset($IS_AUTH)}
window.is_auth = '{$IS_AUTH}';
{/if}
{if isset($single_image)}
window.img_global = '{$single_image}';
{/if}
</script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script src="http://api-maps.yandex.ru/1.1/index.xml?key={$YANDEX_MAPS_KEY}" type="text/javascript"></script>
<script type="text/javascript" src="scripts/mootools-1.2.4-core-yc.js"></script>
<script type="text/javascript" src="scripts/mootools-1.2.4-more-yc.js"></script>
<script type="text/javascript" src="scripts/mocha-compiled.js"></script>
<!--[if IE]>
	<script type="text/javascript" src="scripts/excanvas_r43.js"></script>
<![endif]-->
<script type="text/javascript" src="scripts/mocha-init.js"></script>
{if isset($error)}
{$error}
{/if}
<ul id="contextmenu">
	<li><a href="#download">Download</a></li>
	<li><a href="#delete">Delete</a></li>
	<li><a href="#hide">Cancel</a></li>
</ul>

<iframe id="d_frame" style="visibility:hidden"></iframe>
<iframe id="cd_frame" style="visibility:hidden"></iframe>
</body>
</html>

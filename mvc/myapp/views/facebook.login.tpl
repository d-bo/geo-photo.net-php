<fb:login-button></fb:login-button>
<div id="fb-root"></div>
{literal}
<script>
  window.fbAsyncInit = function(){
	FB.init({
	  appId: '228439040503001', 
	  cookie: true, 
	  xfbml: true,
	  oauth: true
	});
	FB.Event.subscribe('auth.login', function(response){
		window.location = 'http://'+'{global_domain}'+'/facebook/';
	});
	FB.Event.subscribe('auth.logout', function(response){
	  window.location = 'http://'+'{global_domain}'+'/exit';
	});
  };
  (function(){
	var e = document.createElement('script'); e.async = true;
	e.src = document.location.protocol +
	  '//connect.facebook.net/ru_RU/all.js';
	document.getElementById('fb-root').appendChild(e);
  }());
</script>
{/literal}

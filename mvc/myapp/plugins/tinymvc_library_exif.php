<?php

class TinyMVC_Library_Exif
{
	function __construct() {}
	function __destruct() {}
	
	/**
	 * Get SGeo data from APP2 Exif section data
	 *
	 * @static
	 * @access public
	 * @param string $jpeg_path full path to jpeg file (linux style /)
	 * @param string $exec_path path to commmand line exec file, must be compiled in the same folder as ExifLibImpl.so
	 * @param string|array $exclude_element exclude SGeo tag element or elements from output
	 * @return array|int 0 - APP2 section contains no SGeo data, 1 - jpeg file not found, 2 - exec file not found , 3 - library permissions error access (status 127,126 etc.)
	 */
	
	public function getData($exec_path, $jpeg_path, $exclude_element = null)
	{
		if(!is_file($exec_path)) return 2;
		if(!is_file($jpeg_path)) return 1;
		$shell = exec($exec_path.' '.$jpeg_path, $result, $status);
		if($status > 0) return 3;
		if(!empty($result) || isset($result))
		{
			foreach($result as $key => $value)
			{
				$parse = explode(':', str_replace(" ", '', $value));
				if(!is_array($exclude_element) && !is_null($exclude_element))
				{
					if(!strstr($parse[0], $exclude_element))
					{
						$tag[$parse[0]] = $parse[1];
					}
					
				} elseif(is_array($exclude_element) && !is_null($exclude_element)) {
				
					foreach($exclude_element as $key_e => $value_e)
					{
						//echo $value_e;
						if(!strstr($value_e, $parse[0])) $tag[$parse[0]] = $parse[1];
						//if($value_e != $parse[0]) $tag[$parse[0]] = $parse[1];
					}
				
				} else { $tag[$parse[0]] = $parse[1]; }
			}
			return $tag;
			
		} else {return 0;}
		
	}
}

?>

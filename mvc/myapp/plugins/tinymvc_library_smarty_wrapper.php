<?php

// smarty шаблонизатор
define('SMARTY_SPL_AUTOLOAD', 1);
require(TMVC_BASEDIR.'smarty/Smarty.class.php');

class TinyMVC_Library_Smarty_Wrapper Extends Smarty
{
	function __construct()
	{
		parent::__construct();
		$this->template_dir = TMVC_BASEDIR.'myapp/views/';
		$this->compile_dir = TMVC_BASEDIR.'templates_c/';
		$this->config_dir = TMVC_BASEDIR.'configs/';
		$this->cache_dir = TMVC_BASEDIR.'cache/';
		//echo $this->template_dir;
	}
}

?>

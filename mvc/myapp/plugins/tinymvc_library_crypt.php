<?php

class TinyMVC_Library_crypt {
	
	var $algorithms = NULL;
	var $mode = NULL;
	var $key = NULL;
	var $algorithm = NULL;
	
	var $iv = NULL;
	var $iv_size = NULL;
	
	function crypt($alg = 1, $key = "123") {
		$this->algorithms = array (
				MCRYPT_3DES,
				MCRYPT_BLOWFISH,
				MCRYPT_CAST_256,
				MCRYPT_GOST,
				MCRYPT_LOKI97,
				MCRYPT_RIJNDAEL_128,
				MCRYPT_RIJNDAEL_192,
				MCRYPT_RIJNDAEL_256,
				MCRYPT_RC2,
				MCRYPT_SAFERPLUS,
				MCRYPT_SERPENT,
				MCRYPT_TRIPLEDES,
				MCRYPT_TWOFISH,
				"xor"
		);
		
		$this->algorithm = MCRYPT_CAST_256;
		$this->mode = MCRYPT_MODE_NOFB;
		$this->key = ($key != "") ? $key : "JF^#@&*973LSD38)(@#&_9&@#*";

		$this->algorithm = $this->algorithms[intval($alg)];
		if ($this->algorithm == "") {
			$this->algorithm = $this->algorithms[13];
		} else {}

		$this->iv_size = mcrypt_get_iv_size($this->algorithm, $this->mode);
		$this->iv = mcrypt_create_iv($this->iv_size, MCRYPT_RAND);
	}


	function set_key($key) {
		$this->key = $key;
	}

	function encrypt($data, $iv_len = 16) {
		switch($this->algorithm) {
			case "xor":
				$data .= "\x13";
				$n = strlen($data);
				if ($n % 16) {
					$data .= str_repeat("\0", 16 - ($n % 16));
					$i = 0;
					$data = $this->get_rnd_iv($iv_len);
					$iv = substr($this->key ^ $data, 0, 512);
					while ($i < $n) {
						$block = substr($data, $i, 16) ^ pack('H*', sha1($iv));
						$data .= $block;
						$iv = substr($block . $iv, 0, 512) ^ $this->key;
						$i += 16;
					}
					return base64_encode($data);
				} else {}
				break;
			
			default:
				return mcrypt_encrypt($this->algorithm, $this->key, $data, $this->mode, $this->iv);
				break;
		}
	}
	
	function decrypt($data, $iv_len = 16) {
		switch($this->algorithm) {
			case "xor":
				$data = base64_decode($data);
				$n = strlen($data);
				$i = $iv_len;
				$plain_text = '';
				$iv = substr($this->key ^ substr($data, 0, $iv_len), 0, 512);
				while ($i < $n) {
					$block = substr($data, $i, 16);
					$plain_text .= $block ^ pack("H*", sha1($iv));
					$iv = substr($block . $iv, 0, 512) ^ $this->key;
					$i += 16;
				}
				return stripslashes(preg_replace("/\\x13\\x00*$/", "", $plain_text));
				break;

			default:
				return mcrypt_decrypt($this->algorithm, $this->key, $data, $this->mode, $this->iv);
				break;
		}
	}
}

//$crypt = new class_crypt(13, "Your Secret Private Key");
//$crypt->encrypt("raw data to encrypt");
//$crypt->decrypt("encrypted data to decrypt");

?>

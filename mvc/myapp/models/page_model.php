<?php

/**
 * page_model.php
 *
 * default application model
 * @author Dima
 */

class Page_Model extends TinyMVC_Model
{
	private $token;
	protected $main_instance;
	
	function __construct()
	{
		$this->main_instance = tmvc::instance();
	}
	
	/**
	 * query build helper
	 */
	
	function construct_url_sql($url_segments, $extra_string, $db_prefix)
	{
		$count = count($url_segments);
		$sql = "SELECT * FROM ".$db_prefix."_template WHERE url LIKE '";
		for($i = 1; $i <= $count; $i++)
		{
			$sql .= "$url_segments[$i]"."/";
		}
		$_sql = substr($sql, 0, (strlen($sql) - 1));
		$_sql .= "'";
		if(!empty($extra_string)) $_sql .= $extra_string;
		return $_sql;
	}
	
	/**
	 * alias check for loading template (may be "static" or "redirect")
	 */
	
	function check_alias($res, $db, $tmvc, $device)
	{
		$db_prefix = $tmvc->config['db_prefix'];
		if(!empty($res[0]['alias']))
		{
			switch($res[0]['alias_type']) {

				case "static":
				
				$alias = $res[0]['alias'];
				$db->query('SET NAMES UTF8');
				$sql_alias = "SELECT * FROM ".$db_prefix."_template WHERE url = '$alias'";
				if(!empty($device)) $sql_alias .= " AND device = '$device'"; else $sql_alias .= " AND device IS NULL";
				$stmt_alias = $db->prepare($sql_alias);
				if(!$stmt_alias->execute()) $out = $db->errorInfo();
				$_res = $stmt_alias->fetchAll();
				$out = array(
				
					'title' => $res[0]['title'],
					'desc' => $res[0]['desc'],
					'template' => $_res[0]['template']
				
				);
				break;

				case "redirect":
				
				$url = $tmvc->config['base_href'].$res[0]['alias'];
				header("Location: $url");
				break;
				
			}
			
		} else {
			
			$out = array(
				
				'title' => $res[0]['title'],
				'desc' => $res[0]['desc'],
				'template' => $res[0]['template']
				
				);
		}
		
		return $out;
	}
	
	/**
	 * this one returns page template for the next step of compiling by smarty engine
	 */
	 
	function get_page($url_segments)
	{
		$out = array();
		$tmvc = tmvc::instance();
		$dbname = $tmvc->config['pgsql']['dbname'];
		$host = $tmvc->config['pgsql']['host'];
		$username = $tmvc->config['pgsql']['username'];
		$password = $tmvc->config['pgsql']['password'];
		$db = new PDO("pgsql:dbname=$dbname;host=$host", $username, $password);
		$db_prefix = $tmvc->config['db_prefix'];
		
		// если с мобильного устройства заход
		if(strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') || strstr($_SERVER['HTTP_USER_AGENT'], 'Android'))
		{
			// стартовая страница, если url_segment[1] пустой или в есть запись id 
			if(empty($url_segments[1]) || stristr($url_segments[1], 'id'))
			{
				unset($url_segments);
				$url_segments[1] = 'index';
			}
			// для apple страницу тащим, в базе device = iphone
			if(strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad'))
			{
				$_sql = $this->construct_url_sql($url_segments, " AND device = 'iphone'", $db_prefix);
				$stmt = $db->prepare($_sql);
				if(!$stmt->execute()) $out = $db->errorInfo();
				$res_count = $stmt->rowCount();
				$res = $stmt->fetchAll();
				
				if($res_count > 0)
				{
					$out = $this->check_alias($res, $db, $tmvc, 'iphone');
				
				} else {
				
				// если нет, то 404
					$out = array('title' => 'Страница не найдена', 'template' => '404.tpl');			
				
				}
			}
			
			// для android, в базе device = android (одна и таже страница mobile_iphone.tpl метка alias_type = static)
			if(strstr($_SERVER['HTTP_USER_AGENT'], 'Android'))
			{
				$_sql = $this->construct_url_sql($url_segments, " AND device = 'android'", $db_prefix);
				$stmt = $db->prepare($_sql);
				if(!$stmt->execute()) $out = $db->errorInfo();
				$res_count = $stmt->rowCount();
				$res = $stmt->fetchAll();
				
				if($res_count > 0)
				{
					$out = $this->check_alias($res, $db, $tmvc, 'android');
				
				} else {
				
					// если нет, то 404
					$out = array('title' => 'Страница не найдена', 'template' => '404.tpl');			
				
				}
			}
			
		} else {
			
			// стартовая страница, если url_segment[1] пустой или в есть запись id 
			if(empty($url_segments[1]) || stristr($url_segments[1], 'id'))
			{
				unset($url_segments);
				$url_segments[1] = 'index';
			}
			
			$db->query("SET NAMES 'UTF8'");
			$_sql = $this->construct_url_sql($url_segments, ' AND device IS NULL', $db_prefix);
			//echo $_sql;
			$stmt = $db->prepare($_sql);
			if(!$stmt->execute()) $out = $db->errorInfo();
			// rowCount в mysql не работает, только для pgsql
			$res_count = $stmt->rowCount();
			$res = $stmt->fetchAll();
			
			if($res_count > 0)
			{
				$out = $this->check_alias($res, $db, $tmvc, '');
				
			} else {				
				// если нет, то 404
				$out = array('title' => 'Страница не найдена', 'template' => '404.tpl');
			}
		}
		
		return $out;
	}
	
	function get_file($uri, $temp_folder)
	{
		$data = file_get_contents($uri);
		if(!$data) throw new Exception("Cannot find URI");
		$rand = rand(123, 832);
		$this->token = sha1(microtime());
		$filename = $this->token.'.jpg';
		$handle = fopen('/home/php5-3/geo-photo.net/www/' . $temp_folder . $filename, 'w+');
		$error = 'Error loading URI (possible missing cURL extension)';
		if(!$data) throw new Exception($error);
		fwrite($handle, $data);
		return $filename;
	}
	
	/* deprecated */
	private function delete_temp_file($file, $temp_folder)
	{
		if(unlink($temp_folder . $file)) throw new Exception("Cannot delete temp file");
	}
	
	/**
	 * user create
	 */
	
	public function create_user($user_id, $name, $provider)
	{
		// connect to pgsql
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		// user check
		$sql = "SELECT * FROM " . $this->main_instance->config['db_prefix'] . "_users WHERE mashup_id = '$user_id' AND provider = '$provider'";
		$stmt = $dbc->prepare($sql);
		//exit($sql);
		if(!$stmt->execute()) {print_r($dbc->errorInfo()); exit();}
		$res_count = $stmt->rowCount();
		$session_token = sha1(uniqid(microtime()) . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
		if($res_count > 0){
			$res = $stmt->fetchAll();
			setcookie($this->main_instance->config['session']['cookie_name'], $res[0]['session_token'], time() + 36000, '/', '.' . $this->main_instance->config['domain']);
			return true;
		} else {
			$sha1_name = sha1($name);
			/* so we set the cookie and create a folder for user uploads */
			setcookie($this->main_instance->config['session']['cookie_name'], $session_token, time() + 36000, '/', '.' . $this->main_instance->config['domain']);
			$exec1 = $dbc->exec("INSERT INTO " . $this->main_instance->config['db_prefix'] . "_users (mashup_id, provider, name, session_token, name_sha1) VALUES('$user_id', '$provider', '$name', '$session_token', '$sha1_name')");
			// create dir
			if(!is_dir($this->main_instance->config['root_path'] . $provider . '_' . $user_id)){
				if(!@mkdir($this->main_instance->config['root_path'] . $provider . '_' . $user_id, 0775)) throw new Exception('Error creating user folder');
			}
		}
	}
	
	/**
	 * get user by cookie token, returns false if no user present in DB
	 */
	
	public function get_user($token)
	{
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM " . $this->main_instance->config['db_prefix'] . "_users WHERE session_token = '$token'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit('Error page_model::get_user');
		$res_count = $stmt->rowCount();
		if($res_count > 0){
			$res = $stmt->fetchAll();
			return array(
				'id' => $res[0]['mashup_id'],
				'name' => $res[0]['name'],
				'provider' => $res[0]['provider'],
				// original site id
				'geo_id' => $res[0]['id']
			);
		} else return false;
	}
	
	/**
	 * some simple numbers for session auth
	 */
	
	protected function generate_numbers()
	{
		$num1 = rand(1, 15);
		$num2 = rand(1, 10);
		return array('nm1' => $num1, 'nm2' => $num2);
	}
	
	/**
	 * session token. may be more secure )
	 */
	
	protected function generate_session_token()
	{
		return sha1(uniqid(microtime()) . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
	}
	
	/**
	 * record session if there is no one else. returns true if the user is authenticated
	 * sets the cookie & redirect to the beginning
	 */
	
	public function start_session()
	{
		$cook = $this->main_instance->config['session']['cookie_name'];
		//if(isset($_COOKIE[$cook]) && !empty($_COOKIE[$cook]))
		if(!isset($_COOKIE[$cook]) || empty($_COOKIE[$cook])){
			$dbc = pgdb::get();
			$session_token = $this->generate_session_token();
			setcookie($this->main_instance->config['session']['cookie_name'], $session_token, time() + 36000, '/', '.' . $this->main_instance->config['domain']);
			$session_data = serialize($this->generate_numbers());
			$dbc->query("SET NAMES 'UTF8'");
			$time = time();
			$sql = "INSERT INTO " . $this->main_instance->config['db_prefix'] . "_sessions (session_token, session_data, session_start) VALUES ('$session_token', '$session_data', '$time')";
			$stmt = $dbc->prepare($sql);
			if(!$stmt->execute()) {print_r($dbc->errorInfo());exit();};
			// fucked up cookies did not work in the same stream so we need fucking redirect
			header("Location: http://" . $this->main_instance->config['domain']);
		} else {
			// session cookie present, check user auth
			if($this->get_user($_COOKIE[$cook])) return true; else return false;
		}
	}
	
	/**
	 * check session token
	 */
	
	public function check_token($token)
	{
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM " . $this->main_instance->config['db_prefix'] . "_users WHERE activation_code = '$token'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit('debug Error page_model::check_token::stage_0');
		$res_count = $stmt->rowCount();
		if($res_count > 0){
			$res = $stmt->fetchAll();
			$time = time();
			$sql = "UPDATE " . $this->main_instance->config['db_prefix'] . "_users SET activation_time = '$time' WHERE activation_code = '$token'";
			$stmt1 = $dbc->prepare($sql);
			if(!$stmt1->execute()){print_r($dbc->errorInfo()); echo '<br/>'; exit('Error page_model::check_token::stage_1');}
			setcookie($this->main_instance->config['session']['cookie_name'], $res[0]['session_token'], time() + 36000, '/', '.' . $this->main_instance->config['domain']);
			// create user folder
			if(!is_dir($this->main_instance->config['root_path'] . $res[0]['provider'] . '_' . $res[0]['id'])){
				if(!@mkdir($this->main_instance->config['root_path'] . $res[0]['provider'] . '_' . $res[0]['id'], 0775)) exit('Error creating user folder');
			}
			return true;
		} else {return false;}
	}
}

?>

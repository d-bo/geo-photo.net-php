<?php /* Smarty version Smarty-3.0.7, created on 2011-12-02 15:08:12
         compiled from "mvc/myapp/views/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14548485284ed8a38c172835-07174180%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9e998bca7aed1c66f8c7de620f6027e4e94af411' => 
    array (
      0 => 'mvc/myapp/views/index.tpl',
      1 => 1322820486,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14548485284ed8a38c172835-07174180',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include '/home/php5-3/geo-photo.net/www/mvc/smarty/plugins/modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<title>Geo-photo.net</title>
	<link rel="stylesheet" type="text/css" href="themes/charcoal/css/Content.css" />
</head>
<body>
<div id="desktop">
	<?php if (!isset($_smarty_tpl->getVariable('single_image',null,true,false)->value)){?>
	<div id="desktopHeader">
		<div id="desktopNavbar">
			<ul>
				<li><a class="returnFalse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_file'];?>
</a>
					<ul>
						<li><a href="#" onclick="if($('uploader')) document.getElementById('upload_button').click(); else Sitis.login_user();">Upoad image</a></li>
						<li class="divider"><a href="#" onclick="MochaUI.parametricsWindow();return false;">Marker properties</a></li>
						<li><a href="#" onclick="Sitis.user_properties();">User properties</a></li>
					</ul>
				</li>
				
				<li><a class="returnFalse"><?php echo $_smarty_tpl->getVariable('lang')->value['menu_help'];?>
</a>
					<ul>
						<li><a href="#" onclick="Sitis.about_window();">About</a></li>
					</ul>
				</li>
			</ul>
			<div class="toolbox divider">
				<div id="spinnerWrapper"><div id="spinner" style="padding: 0 20px"></div></div>
			</div>
			
			<div class="toolbox divider">
			<?php if ($_smarty_tpl->getVariable('LOGGED_IN')->value=='1'){?>
			<span style="display: inline-block;padding-top: 3px;text-align: center"><?php $_template = new Smarty_Internal_Template('logged.in.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?></span>
			<?php }else{ ?>
			<div style="width: 100px;" class="qq-upload-button" onclick="Sitis.login_user();"><table width="100%"><tr><td><img src="images/login.png" alt="sign in" /></td><td>Log In</td></tr></table></div>
			</div>
			<div class="toolbox divider">
			<div style="width: 100px;" class="qq-upload-button" onclick="Sitis.register_user();"><table width="100%"><tr><td><img src="images/register.png" alt="sign in" /></td><td>Sign Up</td></tr></table></div>
			<?php }?>
			</div>
			
		</div><!-- desktopNavbar end -->
	</div><!-- desktopHeader end -->
	<?php }?>
	<div id="dockWrapper">
		<div id="dock">
			<div id="dockPlacement"></div>
			<div id="dockAutoHide"></div>
			<div id="dockSort"><div id="dockClear" class="clear"></div></div>
		</div>
	</div>
	
	<div id="pageWrapper"></div>
	
	<div id="desktopFooterWrapper">
		<div id="desktopFooter">
			&copy; <?php echo smarty_modifier_date_format(time(),"%Y");?>
 <a href="http://sitis.mobi" target="blank">Sitis Mobile</a>. Based on <a href="http://mochaui.org/">MochaUI</a> library. <a href="http://sitis.mobi">GeoCam AR Pro</a> available at <a href="https://market.android.com/details?id=ru.sitis.android.geocam">Android Market</a> and at <a href="http://itunes.apple.com/us/app/geocam-ar-pro/id476292765?mt=8">AppStore</a>. Send your bugs and messages to <span style="unicode-bidi: bidi-override;direction: rtl"><b>ten.otohp-oeg@troppus</b></span>
			<span style="padding-left: 20px"><a href="http://facebook.com/pages/Sitis-Mobile/226823990665233"><img src="images/icons/facebook.png" /></a></span>
			<span style="padding-left: 5px"><a href="http://twitter.com/sitismobile"><img src="images/icons/twitter.png" /></a></span>
		</div>
	</div>
	
</div><!-- desktop end -->

<script type="text/javascript">
/* js<->php vars */
var lang = {};
lang.file = '<?php echo $_smarty_tpl->getVariable('lang')->value['file'];?>
';
lang.geophoto = '<?php echo $_smarty_tpl->getVariable('lang')->value['geophoto'];?>
';
lang.preview = '<?php echo $_smarty_tpl->getVariable('lang')->value['preview'];?>
';
lang.googleMap = '<?php echo $_smarty_tpl->getVariable('lang')->value['googleMap'];?>
';
lang.yandexMap = '<?php echo $_smarty_tpl->getVariable('lang')->value['yandexMap'];?>
';
lang.properties = '<?php echo $_smarty_tpl->getVariable('lang')->value['properties'];?>
';
lang.filter = '<?php echo $_smarty_tpl->getVariable('lang')->value['filter'];?>
';
lang.data = '<?php echo $_smarty_tpl->getVariable('lang')->value['data'];?>
';
lang.user_data = '<?php echo $_smarty_tpl->getVariable('lang')->value['user_data'];?>
';
lang.result_data = '<?php echo $_smarty_tpl->getVariable('lang')->value['result_data'];?>
';
lang.no_comments = '<?php echo $_smarty_tpl->getVariable('lang')->value['no_comments'];?>
';
lang.map = '<?php echo $_smarty_tpl->getVariable('lang')->value['map'];?>
';
window.session_id = '<?php echo $_smarty_tpl->getVariable('session_id')->value;?>
';
<?php if (isset($_smarty_tpl->getVariable('url_segments',null,true,false)->value)){?>
window.url_segments = '<?php echo $_smarty_tpl->getVariable('url_segments')->value;?>
';
<?php }else{ ?>
window.url_segments = '0';
<?php }?>
<?php if (isset($_smarty_tpl->getVariable('IS_AUTH',null,true,false)->value)){?>
window.is_auth = '<?php echo $_smarty_tpl->getVariable('IS_AUTH')->value;?>
';
<?php }?>
<?php if (isset($_smarty_tpl->getVariable('single_image',null,true,false)->value)){?>
window.img_global = '<?php echo $_smarty_tpl->getVariable('single_image')->value;?>
';
<?php }?>
</script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=<?php echo $_smarty_tpl->getVariable('YANDEX_MAPS_KEY')->value;?>
" type="text/javascript"></script>
<script type="text/javascript" src="scripts/mootools-1.2.4-core-yc.js"></script>
<script type="text/javascript" src="scripts/mootools-1.2.4-more-yc.js"></script>
<script type="text/javascript" src="scripts/mocha-compiled.js"></script>
<!--[if IE]>
	<script type="text/javascript" src="scripts/excanvas_r43.js"></script>
<![endif]-->
<script type="text/javascript" src="scripts/mocha-init.js"></script>
<?php if (isset($_smarty_tpl->getVariable('error',null,true,false)->value)){?>
<?php echo $_smarty_tpl->getVariable('error')->value;?>

<?php }?>
<ul id="contextmenu">
	<li><a href="#download">Download</a></li>
	<li><a href="#delete">Delete</a></li>
	<li><a href="#hide">Cancel</a></li>
</ul>

<iframe id="d_frame" style="visibility:hidden"></iframe>
<iframe id="cd_frame" style="visibility:hidden"></iframe>
</body>
</html>

<?php

/***
 * Name:       TinyMVC
 * About:      An MVC application framework for PHP
 * Copyright:  (C) 2007-2009 Monte Ohrt, All rights reserved.
 * Author:     Monte Ohrt, monte [at] ohrt [dot] com
 * License:    LGPL, see included license file  
 ***/

if(!defined('TMVC_VERSION'))
  define('TMVC_VERSION','1.1');

/* directory separator alias */
if(!defined('DS'))
  define('DS',DIRECTORY_SEPARATOR);  

/* define myapp directory */
if(!defined('TMVC_MYAPPDIR'))
  define('TMVC_MYAPPDIR', TMVC_BASEDIR . 'myapp' . DS);

/* set include_path for spl_autoload */
set_include_path(get_include_path()
  . PATH_SEPARATOR . TMVC_BASEDIR . 'sysfiles' . DS . 'plugins' . DS
  . PATH_SEPARATOR . TMVC_BASEDIR . 'myfiles' . DS . 'plugins' . DS
  . PATH_SEPARATOR . TMVC_MYAPPDIR . 'models' . DS
  . PATH_SEPARATOR . TMVC_MYAPPDIR . 'plugins' . DS
  );

/* set .php first for speed */ 
spl_autoload_extensions('.php,.inc');

$spl_funcs = spl_autoload_functions();
if($spl_funcs === false)
	spl_autoload_register();
elseif(!in_array('spl_autoload',$spl_funcs))
	spl_autoload_register('spl_autoload');

/**
 * postgresql - single connection
 *
 * @package		TinyMVC
 * @author		Dima
 */

class pgdb
{
	public static $dbname;
	public static $dbhost;
	public static $dbusername;
	public static $dbpassword;
	protected static $pgdb;
	
	final private function __construct(){}
	final private function __clone(){}
	
	public static function get()
	{
		if(is_null(self::$pgdb))
		{
			$dbname = self::$dbname;
			$dbhost = self::$dbhost;
			$dbusername = self::$dbusername;
			$dbpassword = self::$dbpassword;
			self::$pgdb = new PDO("pgsql:dbname=$dbname;host=$dbhost", $dbusername, $dbpassword);
		}
		
		return self::$pgdb;
	}
}

/**
 * tmvc
 *
 * main object class
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class tmvc
{
	var $config = null;
	var $controller = null;
	var $action = null;
	var $path_info = null;
	var $url_segments = null;
	var $browser_lang = null;
	var $available_lang = null;
	
	/**
	 * class constructor
	 *
	 * @access 	public
	*/
	
	public function __construct($id = 'default')
	{
		/* set instance */
		self::instance($this, $id);
		/* get browser compatible language */
		$this->browser_lang = $this->getDefaultLanguage();
		/* get available languages */
		$this->available_lang = $this->checkLangDir();
		include(TMVC_MYAPPDIR . 'configs' . DS . 'application.php');
		$this->config = $config;
		pgdb::$dbname = $this->config['pgsql']['dbname'];
		pgdb::$dbhost = $this->config['pgsql']['host'];
		pgdb::$dbusername = $this->config['pgsql']['username'];
		pgdb::$dbpassword = $this->config['pgsql']['password'];
	}
	
	/**
	* main method of execution
	*
	* @access	public
	*/
	
	public function main()
	{
    /* set initial timer */
	self::timer('tmvc_app_start');
	
    /* set path_info */
    $this->path_info = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
	
    /* internal error handling */
	$this->setupErrorHandling();
	
    /* include application config */
	include(TMVC_MYAPPDIR . 'configs' . DS . 'application.php');
	//$this->config = $config;
	
	//exit($this->config['user_folder']);
    /* url remapping/routing */
	$this->setupRouting();
    /* split path_info into array */
	$this->setupSegments();
	
	/* language config */
	$_cookie = $this->config['lang_cookie'];
	
    // test php 5.3 closures
    $closure = function($variable, $array, $cookie){
		
		if(!in_array(strtolower($variable), $array)) {
			
			$lang = 'en';
			setcookie($cookie, $lang, time()+60*60*24*6004);
			
		} else {
			
			$lang = $variable;
			setcookie($cookie, $lang, time()+60*60*24*6004);
		
		}
		
		return $lang;
    };
	
    // lang change
    /*
    if(isset($_POST['lang']) && !empty($_POST['lang'])) {
    
		$_load = $closure($_POST['lang'], $this->available_lang, $_cookie);
		$this->loadLang($_load);
    
    } else {
    
		if(isset($_COOKIE[$_cookie]) && !empty($_COOKIE[$_cookie])) {
			
			$_load = $closure($_COOKIE[$_cookie], $this->available_lang, $_cookie);
			$this->loadLang($_load);
			
		} else {
			
			$_load = $closure($this->browser_lang, $this->available_lang, $_cookie);
			$this->loadLang($_load);
		}
	}
	*/
	
	/* krk expects that only ENG needed ) */
	$this->loadLang('en');
	//exit($this->url_segments[1]);
    /* create controller object */
    $this->setupController();
    /* get controller method */
    $this->setupAction();
    /* run library/script autoloaders */
    $this->setupAutoloaders();
	/* capture output if timing */
    if($this->config['timer']) ob_start();
	
    /* execute controller action */
    $this->controller->{$this->action}();
	
	if($this->config['timer'])
	{
		/* insert timing info */
		$output = ob_get_contents();
		ob_end_clean();
		self::timer('tmvc_app_end');
		echo str_replace('{TMVC_TIMER}',sprintf('%0.5f',self::timer('tmvc_app_start','tmvc_app_end')),$output);
	}
}
	
	/**
	* get user folder
	*
	* @access	public
	*/
	
	public function get_user_folder(){
		$cook = $this->config['session']['cookie_name'];
		if(isset($_COOKIE[$cook]) && !empty($_COOKIE[$cook])){
			$dbc = pgdb::get();
			if(!$dbc) throw new Exception('Wrong db connection');
			$_cook = strip_tags($_COOKIE[$cook]);
			$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE session_token = '$_cook'";
			$stmt = $dbc->prepare($sql);
			if(!$stmt->execute()) throw new Exception($dbc->errorInfo());
			$count = $stmt->rowCount();
			if($count > 0){
				$res = $stmt->fetchAll();
				if($res[0]['provider'] != 'geophoto'){
					$this->config['user_folder'] = $res[0]['provider'] . '_' . $res[0]['mashup_id'] . '/';
					$folder = $res[0]['provider'] . '_' . $res[0]['mashup_id'] . '/';
				} else {
					$this->config['user_folder'] = $res[0]['provider'] . '_' . $res[0]['id'] . '/';
					$folder = $res[0]['provider'] . '_' . $res[0]['id'] . '/';
				}
				
			} else {
				
				// view mode
				if(!empty($_SERVER['HTTP_REFERER'])){
					$path = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
					$_id = explode('/', $path);
					if(!empty($_id[1]) && isset($_id[1])){
						$id = str_ireplace('id', '', $_id[1]);
						$sql1 = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE id = '$id'";
						$stmt1 = $dbc->prepare($sql1);
						if(!$stmt1->execute()){
							print_r($dbc->errorInfo());
							exit();
						}
						$count = $stmt1->rowCount();
						if($count > 0){
							// found id
							$res = $stmt1->fetchAll();
							if($res[0]['provider'] == 'geophoto'){
								// this site provider
								$folder = 'geophoto_' . $res[0]['id'] . '/';
							} else {
								// other account provider
								$folder = $res[0]['provider'] . '_' . $res[0]['mashup_id'] . '/';
							}
						} else {$folder = $this->config['user_folder'];}
					} else {$folder = $this->config['user_folder'];}
				} else {$folder = $this->config['user_folder'];}
										
				}
				
				return $folder;
				
		} else {
			// assume that cookies are always present
			return $this->config['user_folder'];
		}
	}
	
	/**
	* setup error handling for tmvc
	*
	* @access	public
	*/
	public function setupErrorHandling()
	{
    if(defined('TMVC_ERROR_HANDLING') && TMVC_ERROR_HANDLING==1){
			// catch all uncaught exceptions
			set_exception_handler(array('TinyMVC_ExceptionHandler','handleException'));
			require_once(TMVC_BASEDIR . 'sysfiles' . DS . 'plugins' . DS . 'tinymvc_errorhandler.php');   
			set_error_handler('TinyMVC_ErrorHandler');
		}
	}
	
  /**
   * setup url routing for tmvc
   *
   * @access	public
   */
  public function setupRouting()
  {
    if(!empty($this->config['routing']['search'])&&!empty($this->config['routing']['replace']))
      $this->path_info = preg_replace(
          $this->config['routing']['search'],
          $this->config['routing']['replace'],
          $this->path_info);
  }
  
  /**
   * setup url segments array
   *
   * @access	public
   */
  public function setupSegments()
  {
    $this->url_segments = !empty($this->path_info) ? array_filter(explode('/',$this->path_info)) : null;
  }
  
  /**
   * setup controller
   *
   * @access	public
   */    
  public function setupController()
  {
    /* get controller/method */
    if(!empty($this->config['root_controller'])){
      $controller_name = $this->config['root_controller'];
      $controller_file = TMVC_MYAPPDIR . DS . 'controllers' . DS . "{$controller_name}.php";
    } else {
      $controller_name = !empty($this->url_segments[1]) ? preg_replace('!\W!','',$this->url_segments[1]) : $this->config['default_controller'];
      $controller_file = TMVC_MYAPPDIR . DS . 'controllers' . DS . "{$controller_name}.php";
      /* if no controller, use default */
      if(!file_exists($controller_file))
      {
        $controller_name = $this->config['default_controller'];
        $controller_file = TMVC_MYAPPDIR . DS . 'controllers' . DS . "{$controller_name}.php";
      }
    }
    
    include($controller_file);
    
    /* see if controller class exists */
    $controller_class = $controller_name.'_Controller';
      
    /* instantiate the controller */
    $this->controller = new $controller_class(true);
    
  }
  
  /**
   * setup controller method (action) to execute
   *
   * @access	public
   */    
  public function setupAction()
  {
    if(!empty($this->config['root_action'])) {  
      /* user override if set */
      $this->action = $this->config['root_action'];
    } else {
      /* get from url if present, else use default */
      $this->action = !empty($this->url_segments[2]) ? $this->url_segments[2] :
      (!empty($this->config['default_action']) ? $this->config['default_action'] : 'index');
      /* cannot call method names starting with underscore */
      if(substr($this->action,0,1)=='_')
        throw new Exception("Action name not allowed '{$this->action}'");    
    }
  }  
  
  /**
   * autoload any libs/scripts
   *
   * @access	public
   */    
  public function setupAutoloaders()
  {
    include(TMVC_MYAPPDIR . 'configs' . DS . 'autoload.php');
    if(!empty($config['libraries']))
    {
      foreach($config['libraries'] as $library)
        if(is_array($library))
          $this->controller->load->library($library[0],$library[1]);
        else
          $this->controller->load->library($library);
    }
    if(!empty($config['scripts']))
    {
      foreach($config['scripts'] as $script)
        $this->controller->load->script($script);
    }
  }
	
  /**
   * instance
   *
   * get/set the tmvc object instance(s)
   *
   * @access	public
   * @param   object $new_instance reference to new object instance
   * @param   string $id object instance id
   * @return  object $instance reference to object instance
   */    
  public static function &instance($new_instance=null,$id='default')
  {
    static $instance = array();
    if(isset($new_instance) && is_object($new_instance))
      $instance[$id] = $new_instance;
    return $instance[$id];
  }

  /**
   * timer
   *
   * get/set timer values
   *
   * @access  public
   * @param   string $id the timer id to set (or compare with $id2)
   * @param   string $id2 the timer id to compare with $id
   * @return  float  difference of two times
   */    
  public static function timer($id=null,$id2=null)
  {
    static $times = array();
    if($id !== null && $id2 !== null)
      return (isset($times[$id]) && isset($times[$id2])) ? ($times[$id2] - $times[$id]) : false;
    elseif($id !== null)
      return $times[$id] = microtime();
    return false;
  }
  
	/**
	 * language
	 *
	 * get default browser language
	 *
	 * @access  public
	 * @return  string  language
	*/
	
	private function getDefaultLanguage()
	{
		if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
			return $this->parseDefaultLanguage($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		else
			return $this->parseDefaultLanguage(NULL);
	}
	
	/**
	 * language header parser
	 *
	 * parse browser headers, get prior language
	 *
	 * @access  public
	 * @return  string  language
	*/
	
	private function parseDefaultLanguage($http_accept, $deflang = "en")
	{
		if(isset($http_accept) && strlen($http_accept) > 1) {
		
		$x = explode(",",$http_accept);
		foreach ($x as $val) {
		
				if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
				$lang[$matches[1]] = (float)$matches[2];
					else
				$lang[$val] = 1.0;
		}
		
		$qval = 0.0;
		foreach ($lang as $key => $value) {
			 if ($value > $qval) {
				$qval = (float)$value;
				$deflang = $key;				
				}
			}
		}
		
		$_deflang = strtolower($deflang);
		return substr($deflang, 0, 2);
		
	}
	
	/**
	 *
	 * check available languages (i18n folder)
	 *
	*/
	
	private function checkLangDir()
	{
		$out = array();
		if (is_dir(TMVC_I18N_DIR)) {
			if ($dh = opendir(TMVC_I18N_DIR)) {
				while (($file = readdir($dh)) !== false) {
					$out[] = $file;
				}
				
				closedir($dh);
			}
		}
		$count = count($out);
		for($i = 0; $i < $count; $i++) {
			
			if(is_string($out[$i])) {
				if(strstr($out[$i], '.xml')) $_out[] = substr($out[$i], 0, strpos($out[$i], '.'));
			}			
		}
		return $_out;
	}
	
	/**
	 *
	 * check first url param reponsible for language ( example.com/en/ ) and redirect if it is not given to default
	 * @deprecated cant fix output headers 310 error too many redirects, fuck
	*/
	
	public function langRedirect()
	{
		$path = $_SERVER['REQUEST_URI'];
		$host = $_SERVER['SERVER_NAME'];
		$url = strtolower($host.$path);
		header("Location: http://$url" . $this->browser_lang);
	}
	
	/**
	 *
	 * load language & parse .xml file for using with smarty templater
	 *
	*/
	
	public function loadLang($lang)
	{
		$file = TMVC_I18N_DIR.$lang.'.xml';
		$xml = simplexml_load_file($file);
		foreach($xml->element as $item){
			$_item = $item['var'];
			$out["$_item"] = (string)$item;
		}
		
		$this->config['lang_array'] = $out;
		$this->config['lang_options'] = $this->available_lang;
		$this->config['lang_selected'] = $lang;
	}
	
	/**
	 *
	 * check for double login (sha1 encrypted fields)
	 *
	*/
	
	public function check_lgn_email($lgn, $email)
	{
		$dbc = pgdb::get();
		if(!$dbc) throw new Exception('Wrong database connection');
		$provider = $this->config['self_provider'];
		// check lgn
		$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE name = '$lgn' AND provider = '$provider'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) throw new Exception($dbc->errorInfo());
		$count = $stmt->rowCount();
		// check email
		$sql1 = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE email = '$email' AND provider = '$provider'";
		$stmt1 = $dbc->prepare($sql1);
		if(!$stmt1->execute()) throw new Exception($dbc->errorInfo());
		$count1 = $stmt1->rowCount();
		//if($count1 > 1) return 1; else return 0;
		return array('lgn' => $count, 'email' => $count1);
	}
	
	/**
	 *
	 * record new geophoto user
	 *
	*/
	
	public function record_user($login, $email, $pwd, $token, $code)
	{
		$dbc = pgdb::get();
		if(!$dbc) throw new Exception('Wrong database connection');
		$provider = $this->config['self_provider'];
		$sql = "INSERT INTO " . $this->config['db_prefix'] . "_users (provider, session_token, name, email, pwd, activation_code) VALUES ('$provider', '$token', '$login', '$email', '$pwd', '$code')";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) return $dbc->errorInfo(); else return true;
	}
	
	/**
	 *
	 * validate email address
	 *
	*/
	
	function is_valid_email($email){
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return false; else return true;
	}
	
	/**
	 *
	 * get user session token
	 *
	*/
	
	function get_session_token($lgn, $pwd, $provider){
		$dbc = pgdb::get();
		$_lgn = urldecode($lgn);
		$_pwd = urldecode($pwd);
		if(!$dbc) exit('Error connection');
		$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE name = '$lgn' AND pwd = '$pwd' AND provider = '$provider'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit($dbc->errorInfo());
		if(count($stmt->rowCount() > 0)){
			$res = $stmt->fetchAll();
			return $res[0]['session_token'];
		} else {return false;}
	}
	
	/**
	 * !!! clone method - page_model.php get_user. ajax helpers need to improve OOP style
	 */
	
	public function get_user($token)
	{
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE session_token = '$token'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit('Error page_model::get_user');
		$res_count = $stmt->rowCount();
		if($res_count > 0){
			$res = $stmt->fetchAll();
			return array(
				'id' => $res[0]['mashup_id'],
				'name' => $res[0]['name'],
				'provider' => $res[0]['provider'],
				// original site id
				'geo_id' => $res[0]['id'],
				'is_public' => $res[0]['is_public']
			);
		} else return false;
	}
	
	/**
	 * save user properties variable
	 */
	
	public function apply_var($array)
	{
		$cook = $this->config['session']['cookie_name'];
		$token = $_COOKIE[$cook];
		$user_data = $this->get_user($token);
		$id = $user_data['geo_id'];
		$share = $array['share'];
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		$sql = "UPDATE " . $this->config['db_prefix'] . "_users SET is_public = '$share' WHERE id = '$id'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit('Error page_model::appply_var exec sql');
		$res_count = $stmt->rowCount();
		if($res_count > 0) return true; else return false;
	}
	
	/**
	 * check for public account usage
	 */
	
	public function is_public($id)
	{
		$dbc = pgdb::get();
		$dbc->query("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE id = '$id'";
		$stmt = $dbc->prepare($sql);
		if(!$stmt->execute()) exit('Error page_model::appply_var exec sql');
		$res_count = $stmt->rowCount();
		if($res_count > 0){
			$res = $stmt->fetchAll();
			if($res[0]['is_public'] > 0) return true; else return false;
		} else {return false;}
	}
	
	/**
	 * get session token by url id (ajax helper)
	 */
	
	public function get_token_by_id()
	{
		if(isset($this->url_segments[1])){
			$dbc = pgdb::get();
			$id = str_ireplace('id', '', $this->url_segments[1]);
			//exit($this->url_segments[1]);
			$sql = "SELECT * FROM " . $this->config['db_prefix'] . "_users WHERE id = '$id'";
			$stmt = $dbc->prepare($sql);
			if(!$stmt->execute()){
				print_r($dbc->errorInfo());
				exit();
			}
			$count = $stmt->rowCount();
			if($count > 0){
				// found id
				$res = $stmt->fetchAll();
				return $res[0]['session_token'];
				
			} else {return false;}
			
		} else {return false;}
	}
	
}

?>

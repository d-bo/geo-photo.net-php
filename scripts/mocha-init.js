/**
    Head JS     The only script in your <HEAD>
    Copyright   Tero Piirainen (tipiirai)
    License     MIT / http://bit.ly/mit-license
    Version     0.96

    http://headjs.com
*/(function(a){function z(){d||(d=!0,s(e,function(a){p(a)}))}function y(c,d){var e=a.createElement("script");e.type="text/"+(c.type||"javascript"),e.src=c.src||c,e.async=!1,e.onreadystatechange=e.onload=function(){var a=e.readyState;!d.done&&(!a||/loaded|complete/.test(a))&&(d.done=!0,d())},(a.body||b).appendChild(e)}function x(a,b){if(a.state==o)return b&&b();if(a.state==n)return k.ready(a.name,b);if(a.state==m)return a.onpreload.push(function(){x(a,b)});a.state=n,y(a.url,function(){a.state=o,b&&b(),s(g[a.name],function(a){p(a)}),u()&&d&&s(g.ALL,function(a){p(a)})})}function w(a,b){a.state===undefined&&(a.state=m,a.onpreload=[],y({src:a.url,type:"cache"},function(){v(a)}))}function v(a){a.state=l,s(a.onpreload,function(a){a.call()})}function u(a){a=a||h;var b;for(var c in a){if(a.hasOwnProperty(c)&&a[c].state!=o)return!1;b=!0}return b}function t(a){return Object.prototype.toString.call(a)=="[object Function]"}function s(a,b){if(!!a){typeof a=="object"&&(a=[].slice.call(a));for(var c=0;c<a.length;c++)b.call(a,a[c],c)}}function r(a){var b;if(typeof a=="object")for(var c in a)a[c]&&(b={name:c,url:a[c]});else b={name:q(a),url:a};var d=h[b.name];if(d&&d.url===b.url)return d;h[b.name]=b;return b}function q(a){var b=a.split("/"),c=b[b.length-1],d=c.indexOf("?");return d!=-1?c.substring(0,d):c}function p(a){a._done||(a(),a._done=1)}var b=a.documentElement,c,d,e=[],f=[],g={},h={},i=a.createElement("script").async===!0||"MozAppearance"in a.documentElement.style||window.opera,j=window.head_conf&&head_conf.head||"head",k=window[j]=window[j]||function(){k.ready.apply(null,arguments)},l=1,m=2,n=3,o=4;i?k.js=function(){var a=arguments,b=a[a.length-1],c={};t(b)||(b=null),s(a,function(d,e){d!=b&&(d=r(d),c[d.name]=d,x(d,b&&e==a.length-2?function(){u(c)&&p(b)}:null))});return k}:k.js=function(){var a=arguments,b=[].slice.call(a,1),d=b[0];if(!c){f.push(function(){k.js.apply(null,a)});return k}d?(s(b,function(a){t(a)||w(r(a))}),x(r(a[0]),t(d)?d:function(){k.js.apply(null,b)})):x(r(a[0]));return k},k.ready=function(b,c){if(b==a){d?p(c):e.push(c);return k}t(b)&&(c=b,b="ALL");if(typeof b!="string"||!t(c))return k;var f=h[b];if(f&&f.state==o||b=="ALL"&&u()&&d){p(c);return k}var i=g[b];i?i.push(c):i=g[b]=[c];return k},k.ready(a,function(){u()&&s(g.ALL,function(a){p(a)}),k.feature&&k.feature("domloaded",!0)});if(window.addEventListener)a.addEventListener("DOMContentLoaded",z,!1),window.addEventListener("load",z,!1);else if(window.attachEvent){a.attachEvent("onreadystatechange",function(){a.readyState==="complete"&&z()});var A=1;try{A=window.frameElement}catch(B){}!A&&b.doScroll&&function(){try{b.doScroll("left"),z()}catch(a){setTimeout(arguments.callee,1);return}}(),window.attachEvent("onload",z)}!a.readyState&&a.addEventListener&&(a.readyState="loading",a.addEventListener("DOMContentLoaded",handler=function(){a.removeEventListener("DOMContentLoaded",handler,!1),a.readyState="complete"},!1)),setTimeout(function(){c=!0,s(f,function(a){a()})},300)})(document)

var hexcase = 0;
var b64pad  = "";

function hex_sha1(s)    { return rstr2hex(rstr_sha1(str2rstr_utf8(s))); }
function b64_sha1(s)    { return rstr2b64(rstr_sha1(str2rstr_utf8(s))); }
function any_sha1(s, e) { return rstr2any(rstr_sha1(str2rstr_utf8(s)), e); }
function hex_hmac_sha1(k, d)
	{ return rstr2hex(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d))); }
function b64_hmac_sha1(k, d)
	{ return rstr2b64(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d))); }
function any_hmac_sha1(k, d, e)
	{ return rstr2any(rstr_hmac_sha1(str2rstr_utf8(k), str2rstr_utf8(d)), e); }

/*
 * Perform a simple self-test to see if the VM is working
 */

function sha1_vm_test()
{
	return hex_sha1("abc").toLowerCase() == "a9993e364706816aba3e25717850c26c9cd0d89d";
}

/*
 * Calculate the SHA1 of a raw string
 */

function rstr_sha1(s)
{
	return binb2rstr(binb_sha1(rstr2binb(s), s.length * 8));
}

/*
 * Calculate the HMAC-SHA1 of a key and some data (raw strings)
 */

function rstr_hmac_sha1(key, data)
{
	var bkey = rstr2binb(key);
	if(bkey.length > 16) bkey = binb_sha1(bkey, key.length * 8);
	
	var ipad = Array(16), opad = Array(16);
	for(var i = 0; i < 16; i++)
	{
		ipad[i] = bkey[i] ^ 0x36363636;
		opad[i] = bkey[i] ^ 0x5C5C5C5C;
	}
	
	var hash = binb_sha1(ipad.concat(rstr2binb(data)), 512 + data.length * 8);
	return binb2rstr(binb_sha1(opad.concat(hash), 512 + 160));
}

/*
 * Convert a raw string to a hex string
 */

function rstr2hex(input)
{
  try { hexcase } catch(e) { hexcase=0; }
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
  var output = "";
  var x;
  for(var i = 0; i < input.length; i++)
  {
    x = input.charCodeAt(i);
    output += hex_tab.charAt((x >>> 4) & 0x0F)
           +  hex_tab.charAt( x        & 0x0F);
  }
  return output;
}

/*
 * Convert a raw string to a base-64 string
 */
function rstr2b64(input)
{
  try { b64pad } catch(e) { b64pad=''; }
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var output = "";
  var len = input.length;
  for(var i = 0; i < len; i += 3)
  {
    var triplet = (input.charCodeAt(i) << 16)
                | (i + 1 < len ? input.charCodeAt(i+1) << 8 : 0)
                | (i + 2 < len ? input.charCodeAt(i+2)      : 0);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > input.length * 8) output += b64pad;
      else output += tab.charAt((triplet >>> 6*(3-j)) & 0x3F);
    }
  }
  return output;
}

/*
 * Convert a raw string to an arbitrary string encoding
 */
function rstr2any(input, encoding)
{
  var divisor = encoding.length;
  var remainders = Array();
  var i, q, x, quotient;

  /* Convert to an array of 16-bit big-endian values, forming the dividend */
  var dividend = Array(Math.ceil(input.length / 2));
  for(i = 0; i < dividend.length; i++)
  {
    dividend[i] = (input.charCodeAt(i * 2) << 8) | input.charCodeAt(i * 2 + 1);
  }

  /*
   * Repeatedly perform a long division. The binary array forms the dividend,
   * the length of the encoding is the divisor. Once computed, the quotient
   * forms the dividend for the next step. We stop when the dividend is zero.
   * All remainders are stored for later use.
   */
  while(dividend.length > 0)
  {
    quotient = Array();
    x = 0;
    for(i = 0; i < dividend.length; i++)
    {
      x = (x << 16) + dividend[i];
      q = Math.floor(x / divisor);
      x -= q * divisor;
      if(quotient.length > 0 || q > 0)
        quotient[quotient.length] = q;
    }
    remainders[remainders.length] = x;
    dividend = quotient;
  }

  /* Convert the remainders to the output string */
  var output = "";
  for(i = remainders.length - 1; i >= 0; i--)
    output += encoding.charAt(remainders[i]);

  /* Append leading zero equivalents */
  var full_length = Math.ceil(input.length * 8 /
                                    (Math.log(encoding.length) / Math.log(2)))
  for(i = output.length; i < full_length; i++)
    output = encoding[0] + output;

  return output;
}

/*
 * Encode a string as utf-8.
 * For efficiency, this assumes the input is valid utf-16.
 */
function str2rstr_utf8(input)
{
  var output = "";
  var i = -1;
  var x, y;

  while(++i < input.length)
  {
    /* Decode utf-16 surrogate pairs */
    x = input.charCodeAt(i);
    y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
    if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
    {
      x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
      i++;
    }

    /* Encode output as utf-8 */
    if(x <= 0x7F)
      output += String.fromCharCode(x);
    else if(x <= 0x7FF)
      output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
                                    0x80 | ( x         & 0x3F));
    else if(x <= 0xFFFF)
      output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                                    0x80 | ((x >>> 6 ) & 0x3F),
                                    0x80 | ( x         & 0x3F));
    else if(x <= 0x1FFFFF)
      output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                                    0x80 | ((x >>> 12) & 0x3F),
                                    0x80 | ((x >>> 6 ) & 0x3F),
                                    0x80 | ( x         & 0x3F));
  }
  return output;
}

/*
 * Encode a string as utf-16
 */
function str2rstr_utf16le(input)
{
  var output = "";
  for(var i = 0; i < input.length; i++)
    output += String.fromCharCode( input.charCodeAt(i)        & 0xFF,
                                  (input.charCodeAt(i) >>> 8) & 0xFF);
  return output;
}

function str2rstr_utf16be(input)
{
  var output = "";
  for(var i = 0; i < input.length; i++)
    output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
                                   input.charCodeAt(i)        & 0xFF);
  return output;
}

/*
 * Convert a raw string to an array of big-endian words
 * Characters >255 have their high-byte silently ignored.
 */
function rstr2binb(input)
{
  var output = Array(input.length >> 2);
  for(var i = 0; i < output.length; i++)
    output[i] = 0;
  for(var i = 0; i < input.length * 8; i += 8)
    output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
  return output;
}

/*
 * Convert an array of big-endian words to a string
 */
function binb2rstr(input)
{
  var output = "";
  for(var i = 0; i < input.length * 32; i += 8)
    output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
  return output;
}

/*
 * Calculate the SHA-1 of an array of big-endian words, and a bit length
 */
function binb_sha1(x, len)
{
  /* append padding */
  x[len >> 5] |= 0x80 << (24 - len % 32);
  x[((len + 64 >> 9) << 4) + 15] = len;

  var w = Array(80);
  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;
  var e = -1009589776;

  for(var i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;
    var olde = e;

    for(var j = 0; j < 80; j++)
    {
      if(j < 16) w[j] = x[i + j];
      else w[j] = bit_rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
      var t = safe_add(safe_add(bit_rol(a, 5), sha1_ft(j, b, c, d)),
                       safe_add(safe_add(e, w[j]), sha1_kt(j)));
      e = d;
      d = c;
      c = bit_rol(b, 30);
      b = a;
      a = t;
    }

    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
    e = safe_add(e, olde);
  }
  return Array(a, b, c, d, e);

}

/*
 * Perform the appropriate triplet combination function for the current
 * iteration
 */
function sha1_ft(t, b, c, d)
{
  if(t < 20) return (b & c) | ((~b) & d);
  if(t < 40) return b ^ c ^ d;
  if(t < 60) return (b & c) | (b & d) | (c & d);
  return b ^ c ^ d;
}

/*
 * Determine the appropriate additive constant for the current iteration
 */
function sha1_kt(t)
{
  return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
         (t < 60) ? -1894007588 : -899497514;
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

var ScrollBar = new Class({
	
	Extends: Slider,
	
	options: {
		scroll: {
			wheelStops: false/*
			onStart: $empty,
			onComplete: $empty*/
		},
		slider: {
			mode: 'horizontal',
			wheel: true/*
			onChange: $empty(intStep),
			onComplete: $empty(strStep)*/
		},
		knob: {/*
			onStart: $empty*/
		}
	},
	
	initialize: function(scroller, slider, knob, options){
		this.knob = document.id(knob).set('tween', options.knob);
		this.slider = document.id(slider);
		this.scroller = document.id(scroller);
		this.scrollElement = this.scroller.getFirst();
		this.scroll = new Fx.Scroll(this.scroller, $extend(this.options.scroll, options.scroll));
		this.parent(this.slider, this.knob, $extend(this.options.slider, options.slider));
		this.steps = this.scrollElement.getSize()[this.axis] - this.scroller.getSize()[this.axis];
		this.scroller.addEvent('mousewheel', function(event){
			this.element.fireEvent('mousewheel', event);
		}.bind(this));
		this.ratio = this.steps / (this.slider.getSize()[this.axis] - this.knob.getSize()[this.axis]);
	},
	
	move2: function(amount){
		this.set(this.knob.getPosition(this.slider)[this.axis] + amount);
	},
	
	set: function(position){
		if($type(position) === 'element') position = position.getPosition(this.scrollElement)[this.axis] / this.ratio;
		position = position.limit(-this.options.offset, this.full -this.options.offset);
		this.move(position * this.ratio);
		this.knob.tween(this.property, position).get('tween').chain(function(){
			this.fireEvent('complete', Math.round(position * this.ratio) + '');
		}.bind(this));
	},
	
	move: function(position){
		var to = $chk(position) ? position : this.step;
		if(this.options.mode === 'vertical') this.scroll.cancel().start(0, to);
		else this.scroll.cancel().start(to, 0);
	},
	
	draggedKnob: function(){
		this.parent();
		if(this.options.mode === 'vertical') this.scroll.cancel().set(0, this.step);
		else this.scroll.cancel().set(this.step);
	},
	
	clickedElement: function(event){
		if(event.target === this.knob){
			this.knob.get('tween').cancel();
			return this;
		}
		var position = event.page[this.axis] - this.element.getPosition()[this.axis] - this.half;
		position = position.limit(-this.options.offset, this.full -this.options.offset);
		this.set(position);
	},
	
	scrolledElement: function(event){
		var mode = (this.options.mode == 'horizontal') ? (event.wheel < 0) : (event.wheel > 0);
		this.move2(mode ? -this.stepSize * 100 : this.stepSize * 100);
		event.stop();
	}
	
});

var ContextMenu = new Class({

  //implements
  Implements: [Options,Events],

  //options
  options: {
    actions: {},
    menu: 'contextmenu',
    stopEvent: true,
    targets: 'body',
    trigger: 'contextmenu',
    offsets: { x:0, y:0 },
    onShow: $empty,
    onHide: $empty,
    onClick: $empty,
    fadeSpeed: 200
  },

  //initialization
  initialize: function(options){
    //set options
    this.setOptions(options)
    
    //option diffs menu
    this.menu = $(this.options.menu);
    this.targets = $$(this.options.targets);
    
    //fx
    this.fx = new Fx.Tween(this.menu, {property: 'opacity', duration:this.options.fadeSpeed});
	
    //hide and begin the listener
    this.hide().startListener();
    
    //hide the menu
    this.menu.setStyles({'position':'absolute','top':'-900000px', 'display':'block'});
  },

  //get things started
  startListener: function(){
    /* all elements */
    this.targets.each(function(el){
      /* show the menu */
      el.addEvent(this.options.trigger,function(e){
        //enabled?
        if(!this.options.disabled){
          //prevent default, if told to
          if(this.options.stopEvent){e.stop();}
          //record this as the trigger
          this.options.element = $(el);
          //position the menu
          this.menu.setStyles({
            top: (e.page.y + this.options.offsets.y - 20),
            left: (e.page.x + this.options.offsets.x - 20),
            position: 'absolute',
            'z-index': '2000'
          });
          //show the menu
          this.show();
        }
      }.bind(this));
    },this);
	
    /* menu items */
    this.menu.getElements('a').each(function(item){
      item.addEvent('click',function(e) {
        if(!item.hasClass('disabled')) {
          this.execute(item.get('href').split('#')[1],$(this.options.element));
			this.fireEvent('click',[item,e]);
        }
      }.bind(this));
    },this);
	
    //hide on body click
    $(document.body).addEvent('click', function() {
      this.hide();
    }.bind(this));
  },
  
  //show menu
  show: function(trigger) {
    //this.menu.fade('in');
    this.fx.start(1);
    this.fireEvent('show');
    this.shown = true;
    return this;
  },
  
  //hide the menu
  hide: function(trigger) {
    if(this.shown)
    {
      this.fx.start(0);
      //this.menu.fade('out');
      this.fireEvent('hide');
      this.shown = false;
    }
    return this;
  },
  
  //disable an item
  disableItem: function(item) {
    this.menu.getElements('a[href$=' + item + ']').addClass('disabled');
    return this;
  },
  
  //enable an item
  enableItem: function(item) {
    this.menu.getElements('a[href$=' + item + ']').removeClass('disabled');
    return this;
  },
  
  //diable the entire menu
  disable: function() {
    this.options.disabled = true;
    return this;
  },
  
  //enable the entire menu
  enable: function() {
    this.options.disabled = false;
    return this;
  },

  //execute an action
  execute: function(action,element) {
    if(this.options.actions[action]) {
		this.options.actions[action](element,this);
	}
    return this;
	}
	
});

/* app core */

(function(){
	
	window.map_global = [];
	window.gm_instance = [];
	var map_instance = window.map_instance = {};
	var sha1 = {};
	
	var Sitis = window.Sitis = {
		
		opts: {
			spinner: {
				lines: 12,
				length: 20,
				width: 20,
				radius: 19,
				color: '#fff',
				speed: 1,
				trail: 40,
				shadow: false
			}
		},
		
		dump: function(arr,level){
			var dumped_text = "";
			if(!level) level = 0;
			
			var level_padding = "";
			for(var j=0;j<level+1;j++) level_padding += "    ";
			
			if(typeof(arr) == 'object'){
				for(var item in arr){
					
					var value = arr[item];
					
					if(typeof(value) == 'object'){
						
						dumped_text += level_padding + "'" + item + "' ...\n";
						dumped_text += dump(value,level+1);
						
					} else {
						
						dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
						
					}
				}
			
			} else {
				dumped_text = "===>"+arr+"<===("+typeof(arr)+")";			
			}
			
			return dumped_text;
			
		},
		
		apply_properties: function()
		{
			check = $('prop_share').checked;
			if(!check) flag = 0; else flag = 1;
			var myRequest = new Request({
				evalScripts: true,
				method: 'POST',
				url: 'pages/user.properties.php',
				data: {"token": window.session_id, "share": flag}
			});
			myRequest.setHeader('x-url', window.url_segments);
			myRequest.send();
			$('user_prop').retrieve('instance').close();
		},
		
		make_map_div: function(div_create, div_insert)
		{
			var moffset = Cookie.read('_moffset');
			var mnumarcs = Cookie.read('_mnumarcs');
			map_h = $(div_insert).offsetHeight;
			if($(div_create)){
				div_name = div_create+'1';
				div_inject = new Element('div', {id: div_name});
				map_tail = 'map1';
				div_inject.style.height = (map_h - 1)+'px';
				$(where).adopt(div_inject);
			} else {
				div_name = div_create;
				div_inject = new Element('div', {id: div_name});
				map_tail = 'map';
				div_inject.style.height = (map_h - 1)+'px';
				$(where).adopt(div_inject);
			}
		},
		
		google_map: function(map_div, instance, whereto){

			if(lat < 1 || lng < 1) {$(whereto).set('html', '<div style="margin: 10px"><div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">No geo data</td></tr></table></div></div>');} else {
			_cookie_google = Cookie.read('_'+whereto+'_type');
			if(_cookie_google == 0 || _cookie_google == null) _type = google.maps.MapTypeId.ROADMAP; else _type = google.maps.MapTypeId.HYBRID;
			latlng = new google.maps.LatLng(lat, lng);
			myOptions = {
				zoom: gzoom,
				center: latlng,
				disableDefaultUI: false,
				mapTypeId: _type
			};
			map_instance[instance] = new google.maps.Map(document.getElementById(map_div), myOptions);
			
			google.maps.event.addListener(window.map_instance[instance], 'maptypeid_changed', function() {
				g_type = window.map_instance[instance].getMapTypeId();
				MUI.notification('gmap type ' + g_type);
				if(g_type == 'roadmap') Cookie.write('_'+whereto+'_type', 0, {duration: 1}); else Cookie.write('_'+whereto+'_type', 1, {duration: 1});
				//gCookie = Cookie.write('_'+map_id, g_type, {duration: 1});
			});
			
			// orientation 0 - h, 1 - v
			google.maps.event.addListener(window.map_instance[instance], 'zoom_changed', function() {
				g_zoom = window.map_instance[instance].getZoom();
				MUI.notification('gmap zoom ' + g_zoom);
				gCookie = Cookie.write('_gzoom', g_zoom, {duration: 1});
			});
			
			path = new google.maps.Polyline({
				path: coord,
				map: window.map_instance[instance],
				strokeColor: "#0191C8",
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			
			path1 = new google.maps.Polyline({
				path: coord1,
				map: window.map_instance[instance],
				strokeColor: "#0191C8",
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			
			path2 = new google.maps.Polyline({
				path: coord2,
				map: window.map_instance[instance],
				strokeColor: "#ff0000",
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			
			if(typeof(arc[0]) != 'undefined'){
				for(x = 0; x <= mnumarcs; x++){
					
					polyline = new google.maps.Polyline({
						path: arc[x],
						map: window.map_instance[instance],
						strokeColor: "#0191C8",
						strokeOpacity: 1.0,
						strokeWeight: 1
					});
				}
			}
		}
		},
		
		bing_map: function(map_id, instance, whereto)
		{
			if(lat < 1 || lng < 1) {$(whereto).set('html', '<div style="margin: 10px"><div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">No geo data</td></tr></table></div></div>');} else {
			_cookie_bing = Cookie.read('_'+whereto+'_type');
			//alert(_cookie_bing);
			//if(_cookie_bing == '0' || _cookie_bing == null) _type = Microsoft.Maps.MapTypeId.road; else _type = Microsoft.Maps.MapTypeId.birdseye;
			if(_cookie_bing == 0 || _cookie_bing == null) _type = Microsoft.Maps.MapTypeId.road; else _type = Microsoft.Maps.MapTypeId.birdseye;
			bingOptions = {
				credentials: "ApkCEGtojBRAd3OTVZe8xAe-J8ZNXMXpZ6Sii_GfkoY4QHxuIpvR1OhsDzcgA5gi",
				center: new Microsoft.Maps.Location(lat, lng),
				mapTypeId: _type,
				enableClickableLogo: false,
				enableSearchLogo: false,
				//showDashboard: false,
				showCopyright: false,
				//showScalebar: false,
				zoom: bingzoom
			};
			map_instance[instance+'bing'] = new Microsoft.Maps.Map(document.getElementById(map_id), bingOptions);
			Microsoft.Maps.Events.addHandler(map_instance[instance+'bing'], 'maptypechanged', function(e){
				g_type = window.map_instance[instance+'bing'].getMapTypeId();
				MUI.notification('bingmap type ' + g_type);
				if(g_type == 'r') Cookie.write('_'+whereto+'_type', 0, {duration: 1}); else Cookie.write('_'+whereto+'_type', 1, {duration: 1});
			});
			
			location1 = new Microsoft.Maps.Location(coord[0].lat(), coord[0].lng());
			location1e = new Microsoft.Maps.Location(coord[1].lat(), coord[1].lng());
			location2 = new Microsoft.Maps.Location(coord1[0].lat(), coord1[0].lng());
			location2e = new Microsoft.Maps.Location(coord1[1].lat(), coord1[1].lng());
			location3 = new Microsoft.Maps.Location(coord2[0].lat(), coord2[0].lng());
			location3e = new Microsoft.Maps.Location(coord2[1].lat(), coord2[1].lng());
			
			lineVertices = new Array(location1, location1e);
			lineVertices1 = new Array(location2, location2e);
			lineVertices2 = new Array(location3, location3e);
			line = new Microsoft.Maps.Polyline(lineVertices, {strokeColor: new Microsoft.Maps.Color.fromHex('#0191C8')});
			line1 = new Microsoft.Maps.Polyline(lineVertices1, {strokeColor: new Microsoft.Maps.Color.fromHex('#0191C8')});
			line2 = new Microsoft.Maps.Polyline(lineVertices2);
			
			map_instance[instance+'bing'].entities.push(line);
			map_instance[instance+'bing'].entities.push(line1);
			map_instance[instance+'bing'].entities.push(line2);
			Handler = Microsoft.Maps.Events.addHandler(map_instance[instance+'bing'], 'viewchangeend', function(){Cookie.write('_bingzoom', map_instance[instance+'bing'].getZoom(), {duration: 1});});
			
			loc = [];
			line = [];
			soz = [];
			
			layer = new Microsoft.Maps.EntityCollection();
			layer.setOptions({visible: true, zIndex: 1001});
			map_instance[instance+'bing'].entities.push(layer);
			layer.setOptions({visible: true});
			
			for(x = 1;x <= mnumarcs; x++){
				
				for(z = 0;z < arc[x].length; z++){
					_lat = arc[x][z];
					_lng = arc[x][z];
					b_lat = _lat.lat();
					b_lng = _lng.lng();
					loc[z] = new Microsoft.Maps.Location(b_lat, b_lng);
				}
				
				soz[x] = loc;
				loc = [];
				line[x+'bing'] = new Microsoft.Maps.Polyline(soz[x], {strokeColor:new Microsoft.Maps.Color.fromHex('#0191C8'), strokeThickness: 1, strokeDashArray:"8 4"});
				map_instance[instance+'bing'].entities.push(line[x+'bing']);
			}
		}
		},
		
		osm_map: function(map_id, instance, whereto){
			if(lat < 1 || lng < 1) {$(whereto).set('html', '<div style="margin: 10px"><div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">No geo data</td></tr></table></div></div>');} else {
			//d926ad1ba9bd4b0180c176e29d02b5f6
			lineLayer = new OpenLayers.Layer.Vector("Line Layer");
			lineLayer1 = new OpenLayers.Layer.Vector("Line Layer1");
			lineLayer2 = new OpenLayers.Layer.Vector("Line Layer2");
			
			style_red = {strokeColor: '#ff0000',
				strokeOpacity: 1,
				strokeWidth: 3
			};
			
			map_instance[instance+'osm'] = new OpenLayers.Map(map_id, {controls: []});
			map_instance[instance+'osm_nik'] = new OpenLayers.Layer.OSM();
			map_instance[instance+'osm'].addLayer(map_instance[instance+'osm_nik']);
			
			style_blue = {strokeColor: "#0191C8", strokeOpacity: 1, strokeWidth: 3};
			style_blue1 = {strokeColor: "#0191C8", strokeOpacity: 1, strokeWidth: 1};
			
			points = [];
			points1 = [];
			points2 = [];
			
			points[0] = new OpenLayers.LonLat(coord[0].lng(), coord[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points[0] = new OpenLayers.Geometry.Point(points[0].lon,points[0].lat);
			
			points[1] = new OpenLayers.LonLat(coord[1].lng(), coord[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points[1] = new OpenLayers.Geometry.Point(points[1].lon, points[1].lat);
		
			points1[0] = new OpenLayers.LonLat(coord1[0].lng(), coord1[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points1[0] = new OpenLayers.Geometry.Point(points1[0].lon, points1[0].lat);
			
			points1[1] = new OpenLayers.LonLat(coord1[1].lng(), coord1[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points1[1] = new OpenLayers.Geometry.Point(points1[1].lon, points1[1].lat);
			
			points2[0] = new OpenLayers.LonLat(coord2[0].lng(), coord2[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points2[0] = new OpenLayers.Geometry.Point(points2[0].lon, points2[0].lat);
			
			points2[1] = new OpenLayers.LonLat(coord2[1].lng(), coord2[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
			points2[1] = new OpenLayers.Geometry.Point(points2[1].lon, points2[1].lat);
			
			linear_ring = new OpenLayers.Geometry.LinearRing(points);
			polygonFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]), null, style_blue);
			lineLayer.addFeatures([polygonFeature]);
			
			linear_ring1 = new OpenLayers.Geometry.LinearRing(points1);
			polygonFeature1 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring1]), null, style_blue);
			lineLayer1.addFeatures([polygonFeature1]);
			
			linear_ring2 = new OpenLayers.Geometry.LinearRing(points2);
			polygonFeature2 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring2]), null, style_red);
			lineLayer1.addFeatures([polygonFeature2]);
			
			map_instance[instance+'osm'].addLayer(lineLayer);
			map_instance[instance+'osm'].addLayer(lineLayer1);
			map_instance[instance+'osm'].addLayer(lineLayer2);
			
			//arc plot
			loc = [];
			line = [];
			soz = [];
			lay = [];
			
			for(x = 1;x <= mnumarcs; x++){
				
				lay[x] = new OpenLayers.Layer.Vector('vc'+x);
				
				for(z = 0;z < arc[x].length; z++){
					_lat = arc[x][z];
					_lng = arc[x][z];
					b_lat = _lat.lat();
					b_lng = _lng.lng();
					loc[z] = new OpenLayers.LonLat(b_lng, b_lat).transform(new OpenLayers.Projection("EPSG:4326"), map_instance[instance+'osm'].getProjectionObject());
					loc[z] = new OpenLayers.Geometry.Point(loc[z].lon, loc[z].lat);
				}
				
				soz[x] = loc;
				loc = [];
				line[x] = new OpenLayers.Geometry.LineString(soz[x]);
				var lineFeature = new OpenLayers.Feature.Vector(line[x], null, style_blue1); 
				lay[x].addFeatures([lineFeature]);
				map_instance[instance+'osm'].addLayer(lay[x]);
			}
			
			map_instance[instance+'osm'].setCenter(new OpenLayers.LonLat(lng, lat)
			.transform(
				new OpenLayers.Projection("EPSG:4326"),
				new OpenLayers.Projection("EPSG:900913")
			), osmzoom
			);
		}
		},
		
		pause_comp: function(ms){
			ms += new Date().getTime();
			while (new Date() < ms){}
		},
		
		yandex_map: function(map_id, instance, whereto)
		{
			if(lat < 1 || lng < 1) {$(whereto).set('html', '<div style="margin: 10px"><div class="geo-warn"><table width="100%"><tr><td><img src="images/warning.png" alt="warning" /></td><td style="text-align: left">No geo data</td></tr></table></div></div>');} else {
			map_instance[instance+'y'] = new YMaps.Map(YMaps.jQuery('#'+map_id));
			_cookie_yandex = Cookie.read('_'+whereto+'_type');
			if(_cookie_yandex == 0 || _cookie_yandex == null) map_instance[instance+'y'].setType(YMaps.MapType.MAP);
			if(_cookie_yandex == 1) map_instance[instance+'y'].setType(YMaps.MapType.SATELLITE);
			if(_cookie_yandex == 2) map_instance[instance+'y'].setType(YMaps.MapType.HYBRID);
			map_instance[instance+'y'].setCenter(new YMaps.GeoPoint(lng, lat), yzoom);
			YMaps.Events.observe(map_instance[instance+'y'], map_instance[instance+'y'].Events.TypeChange, function (){
				g_type = window.map_instance[instance+'y'].getType().getName();
				MUI.notification('yandex type ' + g_type);
				if(g_type == 'Схема') Cookie.write('_'+whereto+'_type', 0, {duration: 1});
				if(g_type == 'Спутник') Cookie.write('_'+whereto+'_type', 1, {duration: 1});
				if(g_type == 'Гибрид') Cookie.write('_'+whereto+'_type', 2, {duration: 1});
			});
			s_y = new YMaps.Style();
			s_y.lineStyle = new YMaps.LineStyle();
			s_y.lineStyle.strokeColor = '0191C8';
			s_y.lineStyle.strokeWidth = '2';
			YMaps.Styles.add("yamap#CustomLine", s_y);
			
			t_y = new YMaps.Style();
			t_y.lineStyle = new YMaps.LineStyle();
			t_y.lineStyle.strokeColor = 'ff0000';
			t_y.lineStyle.strokeWidth = '2';
			YMaps.Styles.add("yamap#CustomLineT", t_y);
			
			u_y = new YMaps.Style();
			u_y.lineStyle = new YMaps.LineStyle();
			u_y.lineStyle.strokeColor = '0191C8';
			u_y.lineStyle.strokeWidth = '1';
			YMaps.Styles.add("yamap#CustomLineU", u_y);
			
			line66 = new YMaps.Polyline([			
				new YMaps.GeoPoint(coord[0].lng(), coord[0].lat()),
				new YMaps.GeoPoint(coord[1].lng(), coord[1].lat())			
			]);
			
			line66.setStyle("yamap#CustomLine");
			map_instance[instance+'y'].addOverlay(line66);
			
			line77 = new YMaps.Polyline([
				new YMaps.GeoPoint(coord1[0].lng(), coord1[0].lat()),
				new YMaps.GeoPoint(coord1[1].lng(), coord1[1].lat())
			]);
			
			line77.setStyle("yamap#CustomLine");
			map_instance[instance+'y'].addOverlay(line77);
			
			line88 = new YMaps.Polyline([
				new YMaps.GeoPoint(coord2[0].lng(), coord2[0].lat()),
				new YMaps.GeoPoint(coord2[1].lng(), coord2[1].lat())			
			]);
			
			line88.setStyle("yamap#CustomLineT");
			map_instance[instance+'y'].addOverlay(line88);
			
			// arc plotter
			loc = [];
			line = [];
			soz = [];
			
			for(x = 1;x <= mnumarcs; x++){
				
				for(z = 0;z < arc[x].length; z++){
					_lat = arc[x][z];
					_lng = arc[x][z];
					b_lat = _lat.lat();
					b_lng = _lng.lng();
					loc[z] = new YMaps.GeoPoint(b_lng, b_lat);
				}
				
				soz[x] = loc;
				loc = [];
				line[x+'y'] = new YMaps.Polyline(soz[x]);
				line[x+'y'].setStyle("yamap#CustomLineU");
				map_instance[instance+'y'].addOverlay(line[x+'y']);
			}
			
			map_instance[instance+'y'].enableScrollZoom();
			map_instance[instance+'y'].addControl(new YMaps.TypeControl());
			map_instance[instance+'y'].addControl(new YMaps.ScaleLine());
			
			ymaps_listener = YMaps.Events.observe(map_instance[instance+'y'], map_instance[instance+'y'].Events.Update, function () {
				var y_zoom = map_instance[instance+'y'].getZoom();
				MUI.notification('yandex zoom ' + y_zoom);
				var gCookie = Cookie.write('_yzoom', y_zoom, {duration: 1});
			});
		}
		},
		
		loadImg: function(img, num_img)
		{
			// loading indicator
			ox = $('mainPanel').offsetWidth;
			oy = $('mainPanel').offsetHeight;
			Sitis.loading();
			//if($('mainPanel'))
			//alert($$('img[id^=pr]').length);
			$$('img[id*=_pr]').setStyle('background', '#303030');
			if($(img+'_pr')) $(img+'_pr').setStyle('background', '#fff');
			window.img_global = img;
			if($('mainPanel')) $('mainPanel').setStyle('text-align', 'center');
			if($('mainPanel')) $('mainPanel').setStyle('overflow', 'hidden');
			var myRequest = new Request({
			evalScripts: true,
			method: 'POST',
			url: 'pages/map-init.php',
			data: {img: window.img_global},
			onComplete: function(event, xhr){
				
				//if($('mainPanel')) $('mainPanel').set('html', '');
				if(!$('img_inject')){
					var img_selector = new Element('div', {id: 'img_inject'});
					if($('mainPanel')) $('mainPanel').adopt(img_selector);
				}
				
				Cookie.write('_imgx', ox, {duration: 1});
				Cookie.write('_imgy', oy, {duration: 1});
				
				if($$('.file_name')){
					$$('.file_name').setStyle('background', '#303030');
					$$('.file_name').setStyle('color', '#fff');
				}
				
				if($(img)){
					$(img).setStyle('background', '#707070');
					$(img).setStyle('color', '#fff');
				}
				
				var central_photo = new Request({
					'url': 'pages/geophoto.php',
					'method': 'POST',
					'data': {img: img},
					'evalScripts': true,
					onComplete: function(){
						//alert('ok');
						$('mainPanel_title').set('html', window.img_global);
						MUI.notification(img);
						
						MUI.updateContent({
							'element': $('properties'),
							//'content': response,
							'method': 'POST',
							'loadMethod': 'xhr',
							'data': {img: img},
							'url': 'pages/ajax-tabs.php',
							onContentLoaded: function(){
								
								if(window.gallery_loaded != 1){
									Sitis.update_preview();
									window.gallery_loaded = 1;
								}
								//alert(window.signal_pause);
							}
						});
					}
				});
				
				central_photo.send();
				
				// central
				/*
				MUI.updateContent({
					
					'element': $('mainPanel'),
					'method': 'POST',
					'loadMethod': 'xhr',
					'data': {img: img},
					'url': 'pages/geophoto.php',
					onContentLoaded: function(){
						
						$('mainPanel_title').set('html', window.img_global);
						MUI.notification(img);
						
						MUI.updateContent({
							'element': $('properties'),
							//'content': response,
							'method': 'POST',
							'loadMethod': 'xhr',
							'data': {img: img},
							'url': 'pages/ajax-tabs.php',
							onContentLoaded: function(){
								
								if(window.gallery_loaded != 1){
									Sitis.update_preview();
									window.gallery_loaded = 1;
								}
								//alert(window.signal_pause);
							}
						});
					}
				});
				*/
				
				Sitis.refreshMap('map1', true);
				var products = $$('#preview li');
				}
			});
			myRequest.setHeader('x-url', window.url_segments);
			myRequest.send();
			//Sitis.refresh_size();
		},
		
		changeData: function(img, task)
		{
			MUI.updateContent({
				'element': $('properties'),
				'method': 'POST',
				'loadMethod': 'xhr',
				'data': {img: img, code: task},
				'url': 'pages/ajax-tabs.php'
			}
			
			);
		},
		/*
		map_menu_open: function(button_id, menu_name)
		{
			var tofade = menu_name;
			if($(menu_name+'x')){
				
				$(menu_name+'x').dispose();
				
			} else {
				
				menu_html = $(menu_name).get('html');
				var find = $(button_id).getCoordinates();
				var menu_inject = new Element('div', {id: menu_name+'x'});
				menu_inject.set('html', menu_html);
				menu_inject.style.position = 'absolute';
				menu_inject.setStyle('z-index', 1000);
				menu_inject.style.top = (find.top + 25) + 'px';
				menu_inject.style.left = find.left+'px';
				$('desktop').adopt(menu_inject);
			}
		},
		*/
		
		loading: function(){
			//menu_html = $(menu_name).get('html');
			if(!$('sitis_loading')){
				var find = $('mainPanel').getCoordinates();
				var menu_inject = new Element('div', {'id': 'sitis_loading'});
				menu_inject.set('html', '<div style="padding: 10px;background: #444"><img src="images/icons/11.gif" /></div>');
				menu_inject.style.position = 'absolute';
				menu_inject.setStyle('z-index', 1000);
				menu_inject.style.top = find.top + 'px';
				menu_inject.style.left = find.left+'px';
				$('desktop').adopt(menu_inject);
			} else {
				if($('sitis_loading').get('opacity') == 0) $('sitis_loading').fade('in'); else $('sitis_loading').fade('out');
			}
		},
		
		url_map: function(zmap)
		{
			switch(zmap)
			{
				case 'google':
					map_zurl = 'pages/map.google.php';
					break;
				case 'yandex':
					map_zurl = 'pages/map.yandex.php';
					break;
				case 'bing':
					map_zurl = 'pages/map.bing.php';
					break;
				case 'osm':
					map_zurl = 'pages/map.osm.php';
					break;
			}
			return map_zurl;
		},
		
		changeMapButton: function(map_id, map)
		{
			$$('span[id^='+map_id+']').removeClass('green');
			$$('span[id^='+map_id+']').addClass('qq-upload-button');
			$(map_id+'_'+map).addClass('green');
		},
		
		changeMap: function(map_id, map, img, trigger)
		{
			if(trigger === false){var map_url = this.url_map(map);} else {map_url1 = this.url_map(window.map_global.map1); map_url2 = this.url_map(window.map_global.map2);}
			if(trigger === false){
				$(map_id).set('html', '');
				//$(map_id+'_id').set('html', map);
				Cookie.write('_' + map_id, map, {duration: 1});
				
				MUI.updateContent({
					'element': $(map_id),
					//'content': response,
					'method': 'POST',
					'loadMethod': 'xhr',
					'data': {where: map_id},
					'url': map_url
				});
				Sitis.changeMapButton(map_id, map);
				
			} else {
				
				Cookie.write('_map1', window.map_global.map1, {duration: 1});
				Cookie.write('_map2', window.map_global.map2, {duration: 1});
				
				$('map1').set('html', '');
				$('map2').set('html', '');
				
				// set map name in menu tabs
				
				//$('map1_id').set('html', window.map_global.map1);
				//$('map2_id').set('html', window.map_global.map2);
				
				MUI.updateContent({
				
					'element': $('map1'),
					//'content': response,
					'method': 'POST',
					'loadMethod': 'xhr',
					'data': {where: 'map1'},
					'url': map_url1
				
				});
				Sitis.changeMapButton('map1', window.map_global.map1);
				MUI.updateContent({
					'element': $('map2'),
					//'content': response,
					'method': 'POST',
					'loadMethod': 'xhr',
					'data': {where: 'map2'},
					'url': map_url2
				});
				Sitis.changeMapButton('map2', window.map_global.map2);
			}
			
			Sitis.print_comments();
		},
		
		refreshMap: function(map_id, trigger)
		{
			map1_cookie = Cookie.read('_map1');
			map2_cookie = Cookie.read('_map2');
			if(map1_cookie === null) window.map_global.map1 = 'osm'; else window.map_global.map1 = map1_cookie;
			if(map2_cookie === null) window.map_global.map2 = 'google'; else window.map_global.map2 = map2_cookie;
			Sitis.changeMap(map_id, window.map_global[map_id], window.img_global, trigger);
		},
		
		print_comments: function()
		{
			var myRequest = new Request({
				evalScripts: true,
				method: 'POST',
				url: 'pages/comments.php',
				data: {"token": encodeURIComponent(window.session_id), "src": encodeURIComponent(window.img_global)}
			});
			myRequest.setHeader('x-url', window.url_segments);
			myRequest.send();
		},
		
		register_user: function()
		{
			new MUI.Window({
				id: 'register_user',
				title: 'Sign up GeoPhoto',
				width: 360,
				height: 390,
				contentURL: 'pages/user.register.php',
				//padding: {top: 12, right: 12, bottom: 10, left: 12},
				resizable: false,
				maximizable: false,
				onContentLoaded: function(){
					window.rnd1 = Math.floor(Math.random()*11);
					window.rnd2 = Math.floor(Math.random()*12);
					$('cptch').innerHTML = '<div style="text-align: right">Check sum <b>'+window.rnd1+ '</b> + <b>'+window.rnd2+'</b> '+'<input type="text" class="reg_inp small" id="c_summ" value="" maxlength="3" autocomplete="off"></div>';
				}
			});
		},
		
		validate_email: function(email)
		{
			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			if(reg.test(email) == false) return false; else return true;
		},
		
		// check for sign_in
		sign_in: function(){
			lgn     = $('sign_lgn').value;
			pwd     = $('sign_pwd').value;
			sess_id = $('sess_id').value;
			_pwd = b64_sha1(pwd);
			var myRequest = new Request({
				evalScripts: true,
				method: 'POST',
				url: 'pages/signin.php',
				data: {"lgn": encodeURIComponent(lgn), "pwd": encodeURIComponent(_pwd), "sess_id": encodeURIComponent(sess_id)},
				onComplete: function(response){
					if(response > 0){
						new MUI.Window({
							id: 'log_check',
							title: 'OK',
							width: 440,
							height: 130,
							content: '<div class="geo-warn"><table width="100%"><tr><td><img id="reg_src_img" src="images/warning.png" alt="warning" style="padding: 0 10px" /></td><td id="reg_msg" style="text-align: left;font-size: 13px">Incorrect login or password. Please try again.</td></tr></table><br/><input type="button" class="qq-upload-button reg_ok" style="padding: 10px 20px" value="Ok" onclick="$(\'log_check\').retrieve(\'instance\').close();"></div>',
							//padding: {top: 12, right: 12, bottom: 10, left: 12},
							resizable: false,
							maximizable: false
						});
					} else {
						window.location.reload();
					}
				}
			});
			myRequest.setHeader('x-url', window.url_segments);
			myRequest.send();
		},
		
		check_sign_up: function()
		{
			err_arr = [
				'* Login must be 4 - 20 characters length',
				'* Invalid e-mail address',
				'* Password must be 4 - 20 characters length',
				'* Confirm password wrong',
				'* Wrong check sum'
			];
			err = [];
			lgn = $('reg_lgn').value;
			email = $('reg_email').value;
			pwd   = $('reg_pwd').value;
			pwd_c = $('reg_pwd_confirm').value;
			check = Sitis.validate_email(email);
			
			if(lgn.length < 4 || lgn.length > 20){
				err.push(err_arr[0]);
				$('reg_lgn').setStyle('background', 'brown');
			} else {$('reg_lgn').setStyle('background', '#282828');}
			
			if(!check){
				err.push(err_arr[1]);
				$('reg_email').setStyle('background', 'brown');
			} else {$('reg_email').setStyle('background', '#282828');}
			if(pwd.length < 4 || pwd.length > 20){
				err.push(err_arr[2]);
				$('reg_pwd').setStyle('background', 'brown');
			} else {$('reg_pwd').setStyle('background', '#282828');}
			if(pwd != pwd_c){
				err.push(err_arr[3]);
				$('reg_pwd_confirm').setStyle('background', 'brown');
			} else {$('reg_pwd_confirm').setStyle('background', '#282828');}
			if((window.rnd1+window.rnd2) != $('c_summ').value){
				err.push(err_arr[4]);
				$('c_summ').setStyle('background', 'brown');
			} else {$('c_summ').setStyle('background', '#282828');}
			if(err.length > 0){
				pr = err.join("<br/>");
				$('reg_msg').innerHTML = pr;
				$('reg_src_img').src = 'images/warning.png';
			}
			
			if(lgn.length > 0) _lgn = lgn; else _lgn = 0;
			if(pwd.length > 0) _pwd = b64_sha1(pwd); else _pwd = 0;
			if(email.length > 0) _email = email; else _email = 0;
			if(pwd_c.length > 0) _pwd_c = b64_sha1(pwd_c); else _pwd_c = 0;
			
			if(err.length < 1){
				var myRequest = new Request({
					evalScripts: true,
					method: 'POST',
					url: 'pages/check.signup.php',
					data: {"lgn": encodeURIComponent(_lgn), "email": encodeURIComponent(_email), "pwd": encodeURIComponent(_pwd), "check": encodeURIComponent(_pwd_c), "0": lgn.length, "1": email.length, "2": pwd.length, "3" : check.length},
					onComplete: function(response){
						if(response != 0){
							data = JSON.decode(response);
							//alert(data.double_login);
							if((data.double_login > 0 && data.double_email > 0) || (data.double_login > 0 && data.double_email < 1)){
								$('reg_msg').innerHTML = 'Login is already in use. Please try another.';
								$('reg_src_img').src = 'images/error.png';
							}
							if(data.double_login < 1 && data.double_email > 0){
								$('reg_msg').innerHTML = 'EMail is already in use. Please try another.';
								$('reg_src_img').src = 'images/error.png';
							}
							//alert('Please fill the form fields correctly.');
						} else {
							$('register_user').retrieve('instance').close();
							//data = JSON.decode(response);
							new MUI.Window({
								id: 'reg_check',
								title: 'OK',
								width: 440,
								height: 130,
								content: '<div class="geo-warn"><table width="100%"><tr><td><img id="reg_src_img" src="images/info.png" alt="warning" style="padding: 0 10px" /></td><td id="reg_msg" style="text-align: left;font-size: 13px">Please confirm activation code sended to your e-mail</td></tr></table><br/><input type="button" class="qq-upload-button reg_ok" style="padding: 10px 20px" value="Ok" onclick="$(\'reg_check\').retrieve(\'instance\').close();"></div>',
								//padding: {top: 12, right: 12, bottom: 10, left: 12},
								resizable: false,
								maximizable: false
							});
						}
					}
				});
				myRequest.setHeader('x-url', window.url_segments);
				myRequest.send();
			}
		},
		
		login_user: function()
		{
			new MUI.Window({
				id: 'login',
				title: 'Login',
				width: 230,
				height: 590,
				contentURL: 'pages/login.php',
				//padding: {top: 12, right: 12, bottom: 10, left: 12},
				resizable: false,
				maximizable: false
			});
		},
		
		user_properties: function()
		{
			var myRequest = new Request({
				evalScripts: true,
				method: 'POST',
				url: 'pages/user.properties.php',
				data: {"token": encodeURIComponent(window.session_id)}
			});
			myRequest.setHeader('x-url', window.url_segments);
			myRequest.send();
		},
		
		twitter_auth: function()
		{
			var twitter_popup = window.open("http://geo-photo.net/twitter","twit_window","menubar=1,resizable=1,width=850,height=650");
		},
		
		google_auth: function()
		{
			var google_popup = window.open("http://geo-photo.net/google","google_window","menubar=1,resizable=1,width=850,height=650");
		},
		
		facebook_auth: function()
		{
			var google_popup = window.open("http://geo-photo.net/facebook","google_window","menubar=1,resizable=1,width=850,height=650");
		},
		
		save_comments: function()
		{
			var __comments = document.getElementById('comments_text').value;
			_comments = encodeURIComponent(__comments);
			var saveRequest = new Request({
				url: 'pages/save.comments.php',
				method: 'POST',
				evalScripts: true,
				data: {f: window.img_global, c: _comments},
				onSuccess: function(txt){
					$('parametrics').retrieve('instance').close();
					$('inject').set('text', __comments);
					var comments = __comments;
				},
				onFailure: function(txt){
					MUI.notification('Bad request');
				}
			});
			saveRequest.setHeader('x-url', window.url_segments);
			saveRequest.send();
		},
		
		iframe_download: function(url)
		{
			frame = document.getElementById('d_frame');
			frame.src = url;
		},
		
		iframe_remove: function(url)
		{
			frame = document.getElementById('cd_frame');
			frame.src = url;
		},
		
		/* deprecated */
		makeScrollbar: function(needle, content, scrollbar, handle, horizontal, ignoreMouse)
		{
			var mainH = needle.offsetHeight;
			scrollbar.style.height = (mainH - 10)+'px';
			content.style.height = (mainH - 10)+'px';
			content.style.width = (needle.offsetWidth - 30)+'px';
			
			var steps = (horizontal?(content.getScrollSize().x - content.getSize().x):(content.getScrollSize().y - content.getSize().y));
			var slider = new Slider(scrollbar, handle, {
				steps: steps,
				mode: (horizontal?'horizontal':'vertical'),
				onChange: function(step){
					var x = (horizontal?step:0);
					var y = (horizontal?0:step);
					content.scrollTo(x,y);
				}
				
			}).set(0);
			if(!(ignoreMouse)){
				
				$$(content, scrollbar).addEvent('mousewheel', function(e){
					e = new Event(e).stop();
					var step = slider.step - e.wheel * 30;
					slider.set(step);
				});
			}
			
			$(document.body).addEvent('mouseleave',function(){slider.drag.stop();});
			
		},
		
		refresh_files: function()
		{
			if($('files-panel')) $('files-panel').setStyle('overflow', 'hidden');
			var context = new ContextMenu({
				targets: 'a',
				menu: 'contextmenu',
				actions: {
						'download': function(element,ref){
							Sitis.iframe_download('?c='+element.get('id'));
						},
						'delete': function(element,ref){
							Sitis.iframe_remove('?cd='+element.get('id'));
						}
					},
				offsets: {x:2, y:2}
			});
			
			some_y = $('files-panel').offsetHeight;
			some_x = $('files-panel').offsetWidth-20;
			if($('vScroll')){
				$('vScroll').setStyle('height', some_y+'px');
				$('vScroll').setStyle('width', some_x+'px');
			}
			if($('vBar')){
				$('vBar').setStyle('height', some_y+'px');
				var myVProducts = new ScrollBar('vScroll', 'vBar', 'vKnob', {
				slider: {
					mode: 'vertical',
					offset: -1
				}
			});
			}
		},
		
		initializeWindows: function(){
			
			MUI.parametricsWindow = function(){
				new MUI.Window({
					id: 'parametrics',
					title: 'Marker properties',
					contentURL: MUI.path.plugins + 'parametrics/index.html',
					width: 305,
					height: 160,
					x: 370,
					y: 160,
					padding: {top: 12, right: 12, bottom: 10, left: 12},
					resizable: false,
					maximizable: false,
					require: {
						css: [MUI.path.plugins + 'parametrics/css/style.css'],
						onload: function(){
							if(MUI.addMarkerOffsetSlider) MUI.addMarkerOffsetSlider();
							if(MUI.addMarkerNumArcsSlider) MUI.addMarkerNumArcsSlider();
						}
					}
				});
			};
			
			MUI.myChain.callChain();
			
		},
		
		refresh_size: function()
		{
			Sitis.refresh_files();
			_mw = $('mainPanel').offsetWidth-40;
			if($('inject')) $('inject').setStyle('width', _mw+'px');
			_pw = $('mainPanel').offsetWidth;
			if($('photo')) $('photo').setStyle('width', _pw+'px');
			w1 = $('map1_header').offsetWidth-1;
			h1 = $('map1').offsetHeight-3;
			w2 = $('map2_header').offsetWidth-1;
			h2 = $('map2').offsetHeight-3;
			if($('node1')){
				node1 = $('map1').childNodes[0];
				node2 = $('map2').childNodes[0];
			}
			
			node1.setStyle('width', w1+'px');
			node1.setStyle('height', h1+'px');
			$('map1').setStyle('overflow', 'hidden');
			node1.setStyle('overflow', 'hidden');
			
			node2.setStyle('width', w2+'px');
			node2.setStyle('height', h2+'px');
			$('map2').setStyle('overflow', 'hidden');
			node2.setStyle('overflow', 'hidden');
			
			// central photo size
			mpw = $('mainPanel').offsetWidth;
			mph = $('mainPanel').offsetHeight;
			if($('central_photo')){
				cw = $('central_photo').offsetWidth;
				ch = $('central_photo').offsetHeight;
				if(cw > ch) $('central_photo').setStyle('width', mpw+'px');
				if(ch > cw) $('central_photo').setStyle('height', mph+'px');
			}
		},
		
		about_window: function(){
			new MUI.Window({
				id: 'register_user',
				title: 'About',
				width: 360,
				height: 390,
				contentURL: 'pages/about.window.php',
				//padding: {top: 12, right: 12, bottom: 10, left: 12},
				resizable: false,
				maximizable: false
			});
		},
		
		prev_picture: function()
		{
			alert('prev');
		},
		
		next_picture: function()
		{
			alert('next');
		},
		
		remove_picture: function(){
			alert('delete picture');
		},
		
		init_keyboard: function()
		{
			var myKeyboardEventsW = new Keyboard({
				defaultEventType: 'keyup',
				events: {
					'ctrl+left': function(){Sitis.prev_picture();},
					'ctrl+right': function(){Sitis.next_picture();}
				}
			});
			
			var myKeyboardEvents1 = new Keyboard({
				defaultEventType: 'keydown'
			});
			myKeyboardEvents1.addEvents({
					'ctrl+left': function(){Sitis.prev_picture();},
					'ctrl+right': function(){Sitis.next_picture();},
					'ctrl+d': function(){Sitis.remove_picture();}
			});
		},
		
		update_preview: function(){
			
			MUI.updateContent({
				'element': $('mochaConsole'),
				//'content': response,
				'method': 'POST',
				'loadMethod': 'xhr',
				'url': 'pages/user.preview.php',
				onContentLoaded: function(){}
			});
		},
		
		initializeColumns: function(){
			
			new MUI.Column({
				id: 'sideColumn1',
				placement: 'left',
				width: 200,
				resizeLimit: [100, 400],
				onResize: function(){Sitis.refresh_size();}
			});
			
			new MUI.Column({
				id: 'mainColumn',
				placement: 'main',
				resizeLimit: [100, 400],
				collapsible: false
			});
			
			new MUI.Column({
				id: 'sideColumn2',
				placement: 'right',
				width: 400,
				resizeLimit: [350, 450],
				onResize: function(){Sitis.refresh_size();}
			});
			
			new MUI.Panel({
				id: 'files-panel',
				title: lang.file,
				contentURL: 'pages/file.tree.php',
				//headerToolbox: true,
				//headerToolboxURL: 'pages/files-tabs.html',
				column: 'sideColumn1',
				padding: {top: 0, right: 0, bottom: 0, left: 0},
				require: {
					//css: [MUI.path.plugins + 'tree/css/style.css'],			
					//js: [MUI.path.plugins + 'tree/scripts/tree.js'],
					onload: function(){
						if(buildTree) buildTree('tree1');
						//Sitis.update_preview();
					}
				},
				onContentLoaded: function(){
					Sitis.loadImg(window.img_global);
					//$('files-panel').setStyle('overflow', 'scroll');
					
				},
				onResize: function(){
					Sitis.refresh_size();
				},
				onCollapse: function(){Sitis.refresh_size();}
			});
			
			new MUI.Panel({
				id: 'panel2',
				title: lang.filter,
				contentURL: 'pages/actions.php',
				column: 'sideColumn1',
				height: 230,
				onCollapse: function(){Sitis.refresh_size();},
				onContentLoaded: function(){
					if($('uploader')){
						var uploader = new qq.FileUploader({
							element: document.getElementById('uploader'),
							action: 'pages/upload.php'
						});
					}
					
				}
			});
			
			new MUI.Panel({
				id: 'mochaConsole',
				addClass: 'mochaConsole',
				title: lang.preview,
				//headerToolbox: true,
				//headerToolboxURL: 'pages/preview-tabs.html',
				padding: {top: 10, right: 10, bottom: 0, left: 10},
				//contentURL: 'pages/user.preview.php',
				column: 'mainColumn',				
				onResize: function(){Sitis.refresh_size();},
				onExpand: function(){Sitis.refresh_size();}
			});
			
			new MUI.Panel({
				id: 'mainPanel',
				title: lang.geophoto,
				height: 550,
				//contentURL: 'pages/geophoto.php',
				column: 'mainColumn',
				padding: {top: 0, right: 0, bottom: 0, left: 0},
				onResize: function(){Sitis.refresh_size();},
				onExpand: function(){Sitis.refresh_size();},
				onCollapse: function(){Sitis.refresh_size();}
				//headerToolbox: true,
				//headerToolboxURL: 'pages/toolbox-demo2.html'
			});
			
			new MUI.Panel({
				id: 'map1',
				title: lang.map + ' #1',
				column: 'sideColumn2',
				padding: {top: 0, right: 0, bottom: 0, left: 0},
				headerToolbox: true,
				headerToolboxURL: 'pages/google-tabs.html',
				height: 350,
				onResize: function(){Sitis.refresh_size();},
				onExpand: function(){Sitis.refresh_size();},
				onCollapse: function(){Sitis.refresh_size();}
			});
			
			new MUI.Panel({
				id: 'map2',
				title: lang.map + ' #2',
				addClass: 'ya_test',
				column: 'sideColumn2',
				headerToolbox: true,
				headerToolboxURL: 'pages/yandex-tabs.html',
				padding: {top: 0, right: 0, bottom: 0, left: 0},
				height: 350,
				onResize: function(){Sitis.refresh_size();},
				onExpand: function(){Sitis.refresh_size();},
				onCollapse: function(){Sitis.refresh_size();}
			});
			
			new MUI.Panel({
				id: 'properties',
				column: 'sideColumn2',
				tabsURL: 'pages/test-tabs.php',
				padding: {top: 10, right: 10, bottom: 10, left: 10},
				require: {
					//css: [MUI.themePath() + 'css/Tabs.css']
					css: ['themes/charcoal/css/Tabs.css']
				},
				onResize: function(){Sitis.refresh_size();},
				onExpand: function(){Sitis.refresh_size();}
			});
			

			
			MUI.myChain.callChain();
		}
};

window.addEvent('load', function(){
	
	MUI.myChain = new Chain();
	MUI.myChain.chain(
		
		function(){MUI.Desktop.initialize();},
		function(){MUI.Dock.initialize();},
		function(){Sitis.initializeColumns();},
		function(){Sitis.initializeWindows();},
		function(){Sitis.init_keyboard();}
		
	).callChain();
	
});

})(window);

window.onunload=function(){Sitis = null; return true;}

/**
 * http://github.com/valums/file-uploader
 * 
 * Multiple file upload component with progress-bar, drag-and-drop. 
 * © 2010 Andrew Valums ( andrew(at)valums.com ) 
 * 
 * Licensed under GNU GPL 2 or later and GNU LGPL 2 or later, see license.txt.
 */    

//
// Helper functions
//

var qq = qq || {};

/**
 * Adds all missing properties from second obj to first obj
 */ 
qq.extend = function(first, second){
    for (var prop in second){
        first[prop] = second[prop];
    }
};  

/**
 * Searches for a given element in the array, returns -1 if it is not present.
 * @param {Number} [from] The index at which to begin the search
 */
qq.indexOf = function(arr, elt, from){
    if (arr.indexOf) return arr.indexOf(elt, from);
    
    from = from || 0;
    var len = arr.length;    
    
    if (from < 0) from += len;  

    for (; from < len; from++){  
        if (from in arr && arr[from] === elt){  
            return from;
        }
    }  
    return -1;  
}; 
    
qq.getUniqueId = (function(){
    var id = 0;
    return function(){ return id++; };
})();

//
// Events

qq.attach = function(element, type, fn){
    if (element.addEventListener){
        element.addEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.attachEvent('on' + type, fn);
    }
};
qq.detach = function(element, type, fn){
    if (element.removeEventListener){
        element.removeEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.detachEvent('on' + type, fn);
    }
};

qq.preventDefault = function(e){
    if (e.preventDefault){
        e.preventDefault();
    } else{
        e.returnValue = false;
    }
};

//
// Node manipulations

/**
 * Insert node a before node b.
 */
qq.insertBefore = function(a, b){
    b.parentNode.insertBefore(a, b);
};
qq.remove = function(element){
    element.parentNode.removeChild(element);
};

qq.contains = function(parent, descendant){       
    // compareposition returns false in this case
    if (parent == descendant) return true;
    
    if (parent.contains){
        return parent.contains(descendant);
    } else {
        return !!(descendant.compareDocumentPosition(parent) & 8);
    }
};

/**
 * Creates and returns element from html string
 * Uses innerHTML to create an element
 */
qq.toElement = (function(){
    var div = document.createElement('div');
    return function(html){
        div.innerHTML = html;
        var element = div.firstChild;
        div.removeChild(element);
        return element;
    };
})();

//
// Node properties and attributes

/**
 * Sets styles for an element.
 * Fixes opacity in IE6-8.
 */
qq.css = function(element, styles){
    if (styles.opacity != null){
        if (typeof element.style.opacity != 'string' && typeof(element.filters) != 'undefined'){
            styles.filter = 'alpha(opacity=' + Math.round(100 * styles.opacity) + ')';
        }
    }
	qq.extend(element.style, styles);
};
qq.hasClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    return re.test(element.className);
};
qq.addClass = function(element, name){
    if (!qq.hasClass(element, name)){
        element.className += ' ' + name;
    }
};
qq.removeClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    element.className = element.className.replace(re, ' ').replace(/^\s+|\s+$/g, "");
};
qq.setText = function(element, text){
    element.innerText = text;
    element.textContent = text;
};

//
// Selecting elements

qq.children = function(element){
    var children = [],
    child = element.firstChild;

    while (child){
        if (child.nodeType == 1){
            children.push(child);
        }
        child = child.nextSibling;
    }

    return children;
};

qq.getByClass = function(element, className){
    if (element.querySelectorAll){
        return element.querySelectorAll('.' + className);
    }

    var result = [];
    var candidates = element.getElementsByTagName("*");
    var len = candidates.length;

    for (var i = 0; i < len; i++){
        if (qq.hasClass(candidates[i], className)){
            result.push(candidates[i]);
        }
    }
    return result;
};

/**
 * obj2url() takes a json-object as argument and generates
 * a querystring. pretty much like jQuery.param()
 * 
 * how to use:
 *
 *    `qq.obj2url({a:'b',c:'d'},'http://any.url/upload?otherParam=value');`
 *
 * will result in:
 *
 *    `http://any.url/upload?otherParam=value&a=b&c=d`
 *
 * @param  Object JSON-Object
 * @param  String current querystring-part
 * @return String encoded querystring
 */
qq.obj2url = function(obj, temp, prefixDone){
    var uristrings = [],
        prefix = '&',
        add = function(nextObj, i){
            var nextTemp = temp 
                ? (/\[\]$/.test(temp)) // prevent double-encoding
                   ? temp
                   : temp+'['+i+']'
                : i;
            if ((nextTemp != 'undefined') && (i != 'undefined')) {  
                uristrings.push(
                    (typeof nextObj === 'object') 
                        ? qq.obj2url(nextObj, nextTemp, true)
                        : (Object.prototype.toString.call(nextObj) === '[object Function]')
                            ? encodeURIComponent(nextTemp) + '=' + encodeURIComponent(nextObj())
                            : encodeURIComponent(nextTemp) + '=' + encodeURIComponent(nextObj)                                                          
                );
            }
        }; 

    if (!prefixDone && temp) {
      prefix = (/\?/.test(temp)) ? (/\?$/.test(temp)) ? '' : '&' : '?';
      uristrings.push(temp);
      uristrings.push(qq.obj2url(obj));
    } else if ((Object.prototype.toString.call(obj) === '[object Array]') && (typeof obj != 'undefined') ) {
        // we wont use a for-in-loop on an array (performance)
        for (var i = 0, len = obj.length; i < len; ++i){
            add(obj[i], i);
        }
    } else if ((typeof obj != 'undefined') && (obj !== null) && (typeof obj === "object")){
        // for anything else but a scalar, we will use for-in-loop
        for (var i in obj){
            add(obj[i], i);
        }
    } else {
        uristrings.push(encodeURIComponent(temp) + '=' + encodeURIComponent(obj));
    }

    return uristrings.join(prefix)
                     .replace(/^&/, '')
                     .replace(/%20/g, '+'); 
};

//
//
// Uploader Classes
//
//

var qq = qq || {};

/**
 * Creates upload button, validates upload, but doesn't create file list or dd. 
 */
qq.FileUploaderBasic = function(o){
    this._options = {
        // set to true to see the server response
        debug: false,
        action: 'pages/upload.php',
        params: {},
        button: null,
        multiple: true,
        maxConnections: 3,
        // validation        
        allowedExtensions: ['jpg','jpeg','JPG','JPEG','gjpg','GJPG'],
        sizeLimit: 0,   
        minSizeLimit: 0,                             
        // events
        // return false to cancel submit
        onSubmit: function(id, fileName){},
        onProgress: function(id, fileName, loaded, total){},
        onComplete: function(id, fileName, responseJSON){},
        onCancel: function(id, fileName){},
        // messages                
        messages: {
            typeError: "{file} has invalid extension. Only {extensions} are allowed.",
            sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
            emptyError: "{file} is empty, please select files again without it.",
            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
        },
        showMessage: function(message){
            alert(message);
        }               
    };
    qq.extend(this._options, o);
        
    // number of files being uploaded
    this._filesInProgress = 0;
    this._handler = this._createUploadHandler(); 
    
    if (this._options.button){
        this._button = this._createUploadButton(this._options.button);
    }
                        
    this._preventLeaveInProgress();
};

qq.FileUploaderBasic.prototype = {
    setParams: function(params){
        this._options.params = params;
    },
    getInProgress: function(){
        return this._filesInProgress;         
    },
    _createUploadButton: function(element){
        var self = this;
        
        return new qq.UploadButton({
            element: element,
            multiple: this._options.multiple && qq.UploadHandlerXhr.isSupported(),
            onChange: function(input){
				self._onInputChange(input);
			}
		});
	},
    _createUploadHandler: function(){
        var self = this,
            handlerClass;        
        
        if(qq.UploadHandlerXhr.isSupported()){           
            handlerClass = 'UploadHandlerXhr';                        
        } else {
            handlerClass = 'UploadHandlerForm';
        }

        var handler = new qq[handlerClass]({
            debug: this._options.debug,
            action: this._options.action,         
            maxConnections: this._options.maxConnections,   
            onProgress: function(id, fileName, loaded, total){                
                self._onProgress(id, fileName, loaded, total);
                self._options.onProgress(id, fileName, loaded, total);                    
            },            
            onComplete: function(id, fileName, result){
                self._onComplete(id, fileName, result);
                self._options.onComplete(id, fileName, result);
            },
            onCancel: function(id, fileName){
                self._onCancel(id, fileName);
                self._options.onCancel(id, fileName);
            }
        });

        return handler;
    },    
    _preventLeaveInProgress: function(){
        var self = this;
		
        qq.attach(window, 'beforeunload', function(e){
            if (!self._filesInProgress){return;}
            
            var e = e || window.event;
            // for ie, ff
            e.returnValue = self._options.messages.onLeave;
            // for webkit
            return self._options.messages.onLeave;             
        });        
	},
    _onSubmit: function(id, fileName){
        this._filesInProgress++;  
    },
    _onProgress: function(id, fileName, loaded, total){        
    },
    _onComplete: function(id, fileName, result){
		this._filesInProgress--;
        if(result.error){
			this._options.showMessage(result.error);
		}
					
		//alert(fileName);
		
	},
    _onCancel: function(id, fileName){
        this._filesInProgress--;        
    },
    _onInputChange: function(input){
        if (this._handler instanceof qq.UploadHandlerXhr){                
            this._uploadFileList(input.files);                   
        } else {             
            if (this._validateFile(input)){                
                this._uploadFile(input);                                    
            }                      
        }               
        this._button.reset();   
    },  
    _uploadFileList: function(files){
        for (var i=0; i<files.length; i++){
            if ( !this._validateFile(files[i])){
                return;
            }            
        }
        
        for (var i=0; i<files.length; i++){
            this._uploadFile(files[i]);        
        }        
    },       
    _uploadFile: function(fileContainer){      
        var id = this._handler.add(fileContainer);
        var fileName = this._handler.getName(id);
        
        if (this._options.onSubmit(id, fileName) !== false){
            this._onSubmit(id, fileName);
            this._handler.upload(id, this._options.params);
        }
    },      
    _validateFile: function(file){
        var name, size;
        
        if (file.value){
            // it is a file input            
            // get input value and remove path to normalize
            name = file.value.replace(/.*(\/|\\)/, "");
        } else {
            // fix missing properties in Safari
            name = file.fileName != null ? file.fileName : file.name;
            size = file.fileSize != null ? file.fileSize : file.size;
        }
                    
        if (! this._isAllowedExtension(name)){            
            this._error('typeError', name);
            return false;
            
        } else if (size === 0){            
            this._error('emptyError', name);
            return false;
                                                     
        } else if (size && this._options.sizeLimit && size > this._options.sizeLimit){            
            this._error('sizeError', name);
            return false;
                        
        } else if (size && size < this._options.minSizeLimit){
            this._error('minSizeError', name);
            return false;            
        }
        
        return true;                
    },
    _error: function(code, fileName){
        var message = this._options.messages[code];        
        function r(name, replacement){ message = message.replace(name, replacement); }
        
        r('{file}', this._formatFileName(fileName));        
        r('{extensions}', this._options.allowedExtensions.join(', '));
        r('{sizeLimit}', this._formatSize(this._options.sizeLimit));
        r('{minSizeLimit}', this._formatSize(this._options.minSizeLimit));
        
        this._options.showMessage(message);                
    },
    _formatFileName: function(name){
        if (name.length > 33){
            name = name.slice(0, 19) + '...' + name.slice(-13);    
        }
        return name;
    },
    _isAllowedExtension: function(fileName){
        var ext = (-1 !== fileName.indexOf('.')) ? fileName.replace(/.*[.]/, '').toLowerCase() : '';
        var allowed = this._options.allowedExtensions;
        
        if (!allowed.length){return true;}        
        
        for (var i=0; i<allowed.length; i++){
            if (allowed[i].toLowerCase() == ext){ return true;}    
        }
        
        return false;
	},
    _formatSize: function(bytes){
        var i = -1;                                    
        do {
            bytes = bytes / 1024;
            i++;  
        } while (bytes > 99);
		
        return Math.max(bytes, 0.1).toFixed(1) + ['kB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];          
	}
};

/**
 * Class that creates upload widget with drag-and-drop and file list
 * @inherits qq.FileUploaderBasic
 */

qq.FileUploader = function(o){
    // call parent constructor
    qq.FileUploaderBasic.apply(this, arguments);
    // additional options
    qq.extend(this._options, {
        element: null,
        // if set, will be used instead of qq-upload-list in template
        listElement: null,
		
        template: '<div class="qq-uploader">' + 
                '<div class="qq-upload-drop-area"></div>' +
                '<div class="qq-upload-button"><table width="100%"><tr><td><img src="images/upload.png" /></td><td>Upload image</td></tr></table></div>' +
                '<ul class="qq-upload-list"></ul></div>',
		
        // template for one item in file list
        fileTemplate: '<li>' +
                '<span class="qq-upload-file"></span>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-size"></span>' +
                '<a class="qq-upload-cancel" href="#">Cancel</a>' +
                '<span class="qq-upload-failed-text">Failed</span>' +
            '</li>',        
		
        classes: {
            // used to get elements from templates
            button: 'qq-upload-button',
            drop: 'qq-upload-drop-area',
            dropActive: 'qq-upload-drop-area-active',
            list: 'qq-upload-list',
			
            file: 'qq-upload-file',
            spinner: 'qq-upload-spinner',
            size: 'qq-upload-size',
            cancel: 'qq-upload-cancel',
			
            // added to list item when upload completes
            // used in css to hide progress spinner
            success: 'qq-upload-success',
            fail: 'qq-upload-fail'
		}
	});
	
    // overwrite options with user supplied    
    qq.extend(this._options, o);
	
    this._element = this._options.element;
    this._element.innerHTML = this._options.template;
    this._listElement = this._options.listElement || this._find(this._element, 'list');
	
	this._classes = this._options.classes;
	this._button = this._createUploadButton(this._find(this._element, 'button'));
	
    this._bindCancelEvent();
	this._setupDragDrop();
};

// inherit from Basic Uploader
qq.extend(qq.FileUploader.prototype, qq.FileUploaderBasic.prototype);

qq.extend(qq.FileUploader.prototype, {
    /**
     * Gets one of the elements listed in this._options.classes
     **/
    _find: function(parent, type){                                
        var element = qq.getByClass(parent, this._options.classes[type])[0];        
        if (!element){
            throw new Error('element not found ' + type);
        }
        
        return element;
    },
    _setupDragDrop: function(){
        var self = this,
            dropArea = this._find(this._element, 'drop');                        

        var dz = new qq.UploadDropZone({
            element: dropArea,
            onEnter: function(e){
                qq.addClass(dropArea, self._classes.dropActive);
                e.stopPropagation();
            },
            onLeave: function(e){
                e.stopPropagation();
            },
            onLeaveNotDescendants: function(e){
                qq.removeClass(dropArea, self._classes.dropActive);  
            },
            onDrop: function(e){
                dropArea.style.display = 'none';
                qq.removeClass(dropArea, self._classes.dropActive);
                self._uploadFileList(e.dataTransfer.files);    
            }
        });
                
        dropArea.style.display = 'none';

        qq.attach(document, 'dragenter', function(e){     
            if (!dz._isValidFileDrag(e)) return; 
            
            dropArea.style.display = 'block';            
        });                 
        qq.attach(document, 'dragleave', function(e){
            if (!dz._isValidFileDrag(e)) return;            
            
            var relatedTarget = document.elementFromPoint(e.clientX, e.clientY);
            // only fire when leaving document out
            if ( ! relatedTarget || relatedTarget.nodeName == "HTML"){               
                dropArea.style.display = 'none';                                            
            }
        });                
    },
    _onSubmit: function(id, fileName){
        qq.FileUploaderBasic.prototype._onSubmit.apply(this, arguments);
        this._addToList(id, fileName);  
    },
    _onProgress: function(id, fileName, loaded, total){
        qq.FileUploaderBasic.prototype._onProgress.apply(this, arguments);

        var item = this._getItemByFileId(id);
        var size = this._find(item, 'size');
        size.style.display = 'inline';
        
        var text; 
        if (loaded != total){
            text = Math.round(loaded / total * 100) + '% from ' + this._formatSize(total);
        } else {                                   
            text = this._formatSize(total);
        }          
        
        qq.setText(size, text);
	},
    _onComplete: function(id, fileName, result){
        qq.FileUploaderBasic.prototype._onComplete.apply(this, arguments);

        // mark completed
        var item = this._getItemByFileId(id);                
        qq.remove(this._find(item, 'cancel'));
        qq.remove(this._find(item, 'spinner'));
        
        if (result.success){
            qq.addClass(item, this._classes.success);    
        } else {
            qq.addClass(item, this._classes.fail);
        }
                window.img_global = fileName;
		
		//var body = $(document.body).set('html', '');
		MUI.updateContent({
			
			'element': $('files-panel'),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {img: img},
			'url': 'pages/file.tree.php',
			onContentLoaded: function(){
				if(buildTree) buildTree('tree1');
				Sitis.loadImg(fileName);
			}
		});
		MUI.updateContent({
			
			'element': $('mochaConsole'),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {img: img},
			'url': 'pages/user.preview.php'
		});
	},
    _addToList: function(id, fileName){
        var item = qq.toElement(this._options.fileTemplate);                
        item.qqFileId = id;

        var fileElement = this._find(item, 'file');        
        qq.setText(fileElement, this._formatFileName(fileName));
        this._find(item, 'size').style.display = 'none';        

        this._listElement.appendChild(item);
    },
    _getItemByFileId: function(id){
        var item = this._listElement.firstChild;        
        
        // there can't be txt nodes in dynamically created list
        // and we can  use nextSibling
        while (item){            
            if (item.qqFileId == id) return item;            
            item = item.nextSibling;
        }          
    },
    /**
     * delegate click event for cancel link 
     **/
    _bindCancelEvent: function(){
        var self = this,
            list = this._listElement;            
        
        qq.attach(list, 'click', function(e){            
            e = e || window.event;
            var target = e.target || e.srcElement;
            
            if (qq.hasClass(target, self._classes.cancel)){                
                qq.preventDefault(e);
               
                var item = target.parentNode;
                self._handler.cancel(item.qqFileId);
                qq.remove(item);
            }
        });
    }    
});
    
qq.UploadDropZone = function(o){
    this._options = {
        element: null,  
        onEnter: function(e){},
        onLeave: function(e){},  
        // is not fired when leaving element by hovering descendants   
        onLeaveNotDescendants: function(e){},   
        onDrop: function(e){}                       
    };
    qq.extend(this._options, o); 
    
    this._element = this._options.element;
    
    this._disableDropOutside();
    this._attachEvents();   
};

qq.UploadDropZone.prototype = {
    _disableDropOutside: function(e){
        // run only once for all instances
        if (!qq.UploadDropZone.dropOutsideDisabled ){

            qq.attach(document, 'dragover', function(e){
                if (e.dataTransfer){
                    e.dataTransfer.dropEffect = 'none';
                    e.preventDefault(); 
                }           
            });
            
            qq.UploadDropZone.dropOutsideDisabled = true; 
        }        
    },
    _attachEvents: function(){
        var self = this;              
                  
        qq.attach(self._element, 'dragover', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            var effect = e.dataTransfer.effectAllowed;
            if (effect == 'move' || effect == 'linkMove'){
                e.dataTransfer.dropEffect = 'move'; // for FF (only move allowed)    
            } else {                    
                e.dataTransfer.dropEffect = 'copy'; // for Chrome
            }
                                                     
            e.stopPropagation();
            e.preventDefault();                                                                    
        });
        
        qq.attach(self._element, 'dragenter', function(e){
            if (!self._isValidFileDrag(e)) return;
                        
            self._options.onEnter(e);
        });
        
        qq.attach(self._element, 'dragleave', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            self._options.onLeave(e);
            
            var relatedTarget = document.elementFromPoint(e.clientX, e.clientY);                      
            // do not fire when moving a mouse over a descendant
            if (qq.contains(this, relatedTarget)) return;
                        
            self._options.onLeaveNotDescendants(e); 
        });
                
        qq.attach(self._element, 'drop', function(e){
            if (!self._isValidFileDrag(e)) return;
            
            e.preventDefault();
            self._options.onDrop(e);
        });          
    },
    _isValidFileDrag: function(e){
        var dt = e.dataTransfer,
            // do not check dt.types.contains in webkit, because it crashes safari 4            
            isWebkit = navigator.userAgent.indexOf("AppleWebKit") > -1;                        

        // dt.effectAllowed is none in Safari 5
        // dt.types.contains check is for firefox            
        return dt && dt.effectAllowed != 'none' && 
            (dt.files || (!isWebkit && dt.types.contains && dt.types.contains('Files')));
        
    }        
}; 

qq.UploadButton = function(o){
    this._options = {
        element: null,  
        // if set to true adds multiple attribute to file input      
        multiple: false,
        // name attribute of file input
        name: 'file',
        onChange: function(input){},
        hoverClass: 'qq-upload-button-hover',
        focusClass: 'qq-upload-button-focus'                       
    };
    
    qq.extend(this._options, o);
        
    this._element = this._options.element;
    
    // make button suitable container for input
    qq.css(this._element, {
        position: 'relative',
        overflow: 'hidden',
        // Make sure browse button is in the right side
        // in Internet Explorer
        direction: 'ltr'
    });
    
	this._input = this._createInput();
};

qq.UploadButton.prototype = {
    /* returns file input element */    
    getInput: function(){
        return this._input;
    },
    /* cleans/recreates the file input */
    reset: function(){
        if (this._input.parentNode){
            qq.remove(this._input);    
        }                
        
        qq.removeClass(this._element, this._options.focusClass);
        this._input = this._createInput();
    },    
    _createInput: function(){                
        var input = document.createElement("input");
        
        if (this._options.multiple){
            input.setAttribute("multiple", "multiple");
        }
		input.setAttribute("id", "upload_button");
        input.setAttribute("type", "file");
        input.setAttribute("name", this._options.name);
        
        qq.css(input, {
            position: 'absolute',
            // in Opera only 'browse' button
            // is clickable and it is located at
            // the right side of the input
            right: 0,
            top: 0,
            fontFamily: 'Arial',
            // 4 persons reported this, the max values that worked for them were 243, 236, 236, 118
            fontSize: '118px',
            margin: 0,
            padding: 0,
            cursor: 'pointer',
            opacity: 0
        });
        
        this._element.appendChild(input);

        var self = this;
        qq.attach(input, 'change', function(){
            self._options.onChange(input);
        });
                
        qq.attach(input, 'mouseover', function(){
            qq.addClass(self._element, self._options.hoverClass);
        });
        qq.attach(input, 'mouseout', function(){
            qq.removeClass(self._element, self._options.hoverClass);
        });
        qq.attach(input, 'focus', function(){
            qq.addClass(self._element, self._options.focusClass);
        });
        qq.attach(input, 'blur', function(){
            qq.removeClass(self._element, self._options.focusClass);
        });

        // IE and Opera, unfortunately have 2 tab stops on file input
        // which is unacceptable in our case, disable keyboard access
        if (window.attachEvent){
            // it is IE or Opera
            input.setAttribute('tabIndex', "-1");
        }

        return input;            
    }        
};

/**
 * Class for uploading files, uploading itself is handled by child classes
 */
qq.UploadHandlerAbstract = function(o){
    this._options = {
        debug: false,
        action: '/upload.php',
        // maximum number of concurrent uploads        
        maxConnections: 999,
        onProgress: function(id, fileName, loaded, total){},
        onComplete: function(id, fileName, response){},
        onCancel: function(id, fileName){}
    };
    qq.extend(this._options, o);    
    
    this._queue = [];
    // params for files in queue
    this._params = [];
};
qq.UploadHandlerAbstract.prototype = {
    log: function(str){
        if (this._options.debug && window.console) console.log('[uploader] ' + str);        
    },
    /**
     * Adds file or file input to the queue
     * @returns id
     **/    
    add: function(file){},
    /**
     * Sends the file identified by id and additional query params to the server
     */
    upload: function(id, params){
        var len = this._queue.push(id);

        var copy = {};        
        qq.extend(copy, params);
        this._params[id] = copy;        
                
        // if too many active uploads, wait...
        if (len <= this._options.maxConnections){               
            this._upload(id, this._params[id]);
        }
    },
    /**
     * Cancels file upload by id
     */
    cancel: function(id){
        this._cancel(id);
        this._dequeue(id);
    },
    /**
     * Cancells all uploads
     */
    cancelAll: function(){
        for (var i=0; i<this._queue.length; i++){
            this._cancel(this._queue[i]);
        }
        this._queue = [];
    },
    /**
     * Returns name of the file identified by id
     */
    getName: function(id){},
    /**
     * Returns size of the file identified by id
     */          
    getSize: function(id){},
    /**
     * Returns id of files being uploaded or
     * waiting for their turn
     */
    getQueue: function(){
        return this._queue;
    },
    /**
     * Actual upload method
     */
    _upload: function(id){},
    /**
     * Actual cancel method
     */
    _cancel: function(id){},     
    /**
     * Removes element from queue, starts upload of next
     */
    _dequeue: function(id){
        var i = qq.indexOf(this._queue, id);
        this._queue.splice(i, 1);
                
        var max = this._options.maxConnections;
        
        if (this._queue.length >= max && i < max){
            var nextId = this._queue[max-1];
            this._upload(nextId, this._params[nextId]);
        }
    }        
};

/**
 * Class for uploading files using form and iframe
 * @inherits qq.UploadHandlerAbstract
 */
qq.UploadHandlerForm = function(o){
    qq.UploadHandlerAbstract.apply(this, arguments);
       
    this._inputs = {};
};
// @inherits qq.UploadHandlerAbstract
qq.extend(qq.UploadHandlerForm.prototype, qq.UploadHandlerAbstract.prototype);

qq.extend(qq.UploadHandlerForm.prototype, {
    add: function(fileInput){
        fileInput.setAttribute('name', 'qqfile');
        var id = 'qq-upload-handler-iframe' + qq.getUniqueId();       
        
        this._inputs[id] = fileInput;
        
        // remove file input from DOM
        if (fileInput.parentNode){
            qq.remove(fileInput);
        }
                
        return id;
    },
    getName: function(id){
        // get input value and remove path to normalize
        return this._inputs[id].value.replace(/.*(\/|\\)/, "");
    },    
    _cancel: function(id){
        this._options.onCancel(id, this.getName(id));
        
        delete this._inputs[id];        

        var iframe = document.getElementById(id);
        if (iframe){
            // to cancel request set src to something else
            // we use src="javascript:false;" because it doesn't
            // trigger ie6 prompt on https
            iframe.setAttribute('src', 'javascript:false;');

            qq.remove(iframe);
        }
    },     
    _upload: function(id, params){                        
        var input = this._inputs[id];
        
        if (!input){
            throw new Error('file with passed id was not added, or already uploaded or cancelled');
        }                

        var fileName = this.getName(id);
                
        var iframe = this._createIframe(id);
        var form = this._createForm(iframe, params);
        form.appendChild(input);

        var self = this;
        this._attachLoadEvent(iframe, function(){                                 
            self.log('iframe loaded');
            
            var response = self._getIframeContentJSON(iframe);

            self._options.onComplete(id, fileName, response);
            self._dequeue(id);
            
            delete self._inputs[id];
            // timeout added to fix busy state in FF3.6
            setTimeout(function(){
                qq.remove(iframe);
            }, 1);
        });

        form.submit();        
        qq.remove(form);        
        
        return id;
    }, 
    _attachLoadEvent: function(iframe, callback){
        qq.attach(iframe, 'load', function(){
            // when we remove iframe from dom
            // the request stops, but in IE load
            // event fires
            if (!iframe.parentNode){
                return;
            }

            // fixing Opera 10.53
            if (iframe.contentDocument &&
                iframe.contentDocument.body &&
                iframe.contentDocument.body.innerHTML == "false"){
                // In Opera event is fired second time
                // when body.innerHTML changed from false
                // to server response approx. after 1 sec
                // when we upload file with iframe
                return;
            }

            callback();
        });
    },
    /**
     * Returns json object received by iframe from server.
     */
    _getIframeContentJSON: function(iframe){
        // iframe.contentWindow.document - for IE<7
        var doc = iframe.contentDocument ? iframe.contentDocument: iframe.contentWindow.document,
            response;
        
        this.log("converting iframe's innerHTML to JSON");
        this.log("innerHTML = " + doc.body.innerHTML);
                        
        try {
            response = eval("(" + doc.body.innerHTML + ")");
        } catch(err){
            response = {};
        }        

        return response;
    },
    /**
     * Creates iframe with unique name
     */
    _createIframe: function(id){
        // We can't use following code as the name attribute
        // won't be properly registered in IE6, and new window
        // on form submit will open
        // var iframe = document.createElement('iframe');
        // iframe.setAttribute('name', id);

        var iframe = qq.toElement('<iframe src="javascript:false;" name="' + id + '" />');
        // src="javascript:false;" removes ie6 prompt on https

        iframe.setAttribute('id', id);

        iframe.style.display = 'none';
        document.body.appendChild(iframe);

        return iframe;
    },
    /**
     * Creates form, that will be submitted to iframe
     */
    _createForm: function(iframe, params){
        // We can't use the following code in IE6
        // var form = document.createElement('form');
        // form.setAttribute('method', 'post');
        // form.setAttribute('enctype', 'multipart/form-data');
        // Because in this case file won't be attached to request
        var form = qq.toElement('<form method="post" enctype="multipart/form-data"></form>');

        var queryString = qq.obj2url(params, this._options.action);

        form.setAttribute('action', queryString);
        form.setAttribute('target', iframe.name);
        form.style.display = 'none';
        document.body.appendChild(form);

        return form;
    }
});

/**
 * Class for uploading files using xhr
 * @inherits qq.UploadHandlerAbstract
 */
qq.UploadHandlerXhr = function(o){
    qq.UploadHandlerAbstract.apply(this, arguments);

    this._files = [];
    this._xhrs = [];
    
    // current loaded size in bytes for each file 
    this._loaded = [];
};

// static method
qq.UploadHandlerXhr.isSupported = function(){
    var input = document.createElement('input');
    input.type = 'file';        
    
    return (
        'multiple' in input &&
        typeof File != "undefined" &&
        typeof (new XMLHttpRequest()).upload != "undefined" );       
};

// @inherits qq.UploadHandlerAbstract
qq.extend(qq.UploadHandlerXhr.prototype, qq.UploadHandlerAbstract.prototype)

qq.extend(qq.UploadHandlerXhr.prototype, {
    /**
     * Adds file to the queue
     * Returns id to use with upload, cancel
     **/    
    add: function(file){
        if (!(file instanceof File)){
            throw new Error('Passed obj in not a File (in qq.UploadHandlerXhr)');
        }
		
        return this._files.push(file) - 1;        
    },
    getName: function(id){        
        var file = this._files[id];
        // fix missing name in Safari 4
        return file.fileName != null ? file.fileName : file.name;       
    },
    getSize: function(id){
        var file = this._files[id];
        return file.fileSize != null ? file.fileSize : file.size;
    },    
    /**
     * Returns uploaded bytes for file identified by id 
     */    
    getLoaded: function(id){
        return this._loaded[id] || 0; 
    },
    /**
     * Sends the file identified by id and additional query params to the server
     * @param {Object} params name-value string pairs
     */    
    _upload: function(id, params){
        var file = this._files[id],
            name = this.getName(id),
            size = this.getSize(id);
                
        this._loaded[id] = 0;
                                
        var xhr = this._xhrs[id] = new XMLHttpRequest();
        var self = this;
		
        xhr.upload.onprogress = function(e){
            if(e.lengthComputable){
                self._loaded[id] = e.loaded;
                self._options.onProgress(id, name, e.loaded, e.total);
            }
        };
		
        xhr.onreadystatechange = function(){            
            if(xhr.readyState == 4){
                self._onComplete(id, xhr);                    
            }
        };
		
        // build query string
        params = params || {};
        params.session_id = window.session_id;
        params['qqfile'] = name;
        var queryString = qq.obj2url(params, this._options.action);

        xhr.open("POST", queryString, true);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("X-File-Name", encodeURIComponent(name));
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
        xhr.send(file);
	},
    _onComplete: function(id, xhr){
        // the request was aborted/cancelled
        if (!this._files[id]) return;
        
        var name = this.getName(id);
        var size = this.getSize(id);
        
        this._options.onProgress(id, name, size, size);
                
        if (xhr.status == 200){
            this.log("xhr - server response received");
            this.log("responseText = " + xhr.responseText);
                        
            var response;
			
            try {
                response = eval("(" + xhr.responseText + ")");
            } catch(err){
                response = {};
            }
            
            this._options.onComplete(id, name, response);
                        
        } else {
            this._options.onComplete(id, name, {});
        }
		
        this._files[id] = null;
        this._xhrs[id] = null;    
        this._dequeue(id);
	},
    _cancel: function(id){
        this._options.onCancel(id, this.getName(id));
        
        this._files[id] = null;
        
        if (this._xhrs[id]){
            this._xhrs[id].abort();
            this._xhrs[id] = null;                                   
        }
    }
});

/* TREE */

function buildTree(treeID){
	
	var iconPath = "plugins/tree/images/"

	$$('#'+treeID+' li.folder').each(function(folder){
		var folderContents = folder.getChildren('ul');
		var folderImage = new Element('img', {
			'src': iconPath + '_open.gif',
			'width': 18,
			'height': 18
		}).inject(folder, 'top');

		// Determine which open and close graphic each folder gets
		
		if (folder.hasClass('root')) {
			folder.minus = iconPath + 'Rminus.gif'
			folder.plus = iconPath + 'Rplus.gif'
		}
		else if (folder.hasClass('first')) {
			folder.minus = iconPath + 'Fminus.gif'
			folder.plus = iconPath + 'Fplus.gif'
		}		
		else 
			if(folder.getNext()){
				folder.minus = iconPath + 'Tminus.gif'
				folder.plus = iconPath + 'Tplus.gif'
			}
			else {
				folder.minus = iconPath + 'Lminus.gif'
				folder.plus = iconPath + 'Lplus.gif'
			}
		
		var image = new Element('img', {
			'src': folder.minus,
			'width': 18,
			'height': 18
		}).addEvent('click', function(){
			if (folder.hasClass('f-open')){
				image.setProperty('src', folder.plus);
				folderImage.setProperty('src', iconPath + '_closed.gif');
				folderContents.each(function(el){
					el.setStyle('display', 'none');
				});
				folder.removeClass('f-open');
			}
			else {
				image.setProperty('src', folder.minus);
				folderImage.setProperty('src', iconPath + '_open.gif');
				folderContents.each(function(el){
					el.setStyle('display', 'block');
				});
				folder.addClass('f-open');
			}
		}).inject(folder, 'top');
		
		if (!folder.hasClass('f-open')) {
			image.setProperty('src', folder.plus);
			folderContents.each(function(el){
				el.setStyle('display', 'none');
			});
			folder.removeClass('f-open');
		}

		// Add connecting branches to each file node

		folderContents.each(function(element){
			var docs = element.getChildren('li.doc');
			docs.each(function(el){
				if (el == docs.getLast() && !el.getNext()) {
					new Element('img', {
						'src': iconPath + 'L.gif',
						'width': 18,
						'height': 18
					}).inject(el.getElement('span'), 'before');
				}
				else {
					new Element('img', {
						'src': iconPath + 'T.gif',
						'width': 18,
						'height': 18
					}).inject(el.getElement('span'), 'before');
				}
			});
		});
	});
	
	// Add connecting branches to each node
	
	$$('#'+treeID+' li').each(function(node){
		node.getParents('li').each(function(parent){
			if (parent.getNext()){
				new Element('img', {
					'src': iconPath + 'I.gif',
					'width': 18,
					'height': 18
				}).inject(node, 'top');
			}
			else {
				new Element('img', {
					'src': iconPath + 'spacer.gif',
					'width': 18,
					'height': 18
				}).inject(node, 'top');
			}
		});
	});
	
	$$('#'+treeID+' li.doc').each(function(el){
		new Element('img', {
			'src': iconPath + '_doc.gif',
			'width': 18,
			'height': 18
		}).inject(el.getElement('span'), 'before');
	});
}

/*
---
description:     LazyLoad

authors:
  - David Walsh (http://davidwalsh.name)

license:
  - MIT-style license

requires:
  core/1.2.1:   "*"

provides:
  - LazyLoad
...
*/
var LazyLoad = new Class({

	Implements: [Options,Events],

	/* additional options */
	options: {
		range: 200,
		elements: "img",
		container: window,
		mode: "vertical",
		realSrcAttribute: "data-src",
		useFade: true
	},

	/* initialize */
	initialize: function(options) {
		
		// Set the class options
		this.setOptions(options);
		
		// Elementize items passed in
		this.container = document.id(this.options.container);
		this.elements = document.id(this.container == window ? document.body : this.container).getElements(this.options.elements);
		
		// Set a variable for the "highest" value this has been
		this.largestPosition = 0;
		
		// Figure out which axis to check out
		var axis = (this.options.mode == "vertical" ? "y": "x");
		
		// Calculate the offset
		var offset = (this.container != document.body ? this.container : "");
		//alert(this.container.innerHeight);
		// Find elements remember and hold on to
		this.elements = this.elements.filter(function(el) {
			// Make opacity 0 if fadeIn should be done
			//if(this.options.useFade) el.setStyle("opacity", 0);
			// Get the image position
			var elPos = el.getPosition(offset)[axis];
			// If the element position is within range, load it
			if(elPos < this.container.getSize()[axis] + this.options.range) {
				this.loadImage(el);
				return false;
			}
			return true;
		},this);
		
		// Create the action function that will run on each scroll until all images are loaded
		var action = function(e){
			
			// Get the current position
			var cpos = this.container.getScroll()[axis];
			
			// If the current position is higher than the last highest
			if(cpos > this.largestPosition){
				
				// Filter elements again
				this.elements = this.elements.filter(function(el){
					
					// If the element is within range...
					if((cpos + this.options.range + this.container.getSize()[axis]) >= el.getPosition(offset)[axis]){
						// Load the image!
						this.loadImage(el);
						return false;
					}
					return true;
					
				},this);
				
				// Update the "highest" position
				this.largestPosition = cpos;
			}
			
			// relay the class" scroll event
			this.fireEvent("scroll");
			
			// If there are no elements left, remove the action event and fire complete
			if(!this.elements.length) {
				this.container.removeEvent("scroll", action);
				this.fireEvent("complete");
			}
			
		}.bind(this);
		
		// Add scroll listener
		this.container.addEvent("scroll", action);
	},
	loadImage: function(image){
		// Set load event for fadeIn
		if(this.options.useFade){
			image.addEvent("load", function(){
				image.fade(1);
			});
		}
		// Set the SRC
		image.set("src", image.get(this.options.realSrcAttribute));
		// Fire the image load event
		this.fireEvent("load", [image]);
	}
});

/*

Script: Parametrics.js
	Initializes the GUI property sliders.

Copyright:
	Copyright (c) 2007-2008 Greg Houston, <http://greghoustondesign.com/>.	

License:
	MIT-style license.

Requires:
	Core.js, Window.js

*/

MUI.extend({
	addMarkerOffsetSlider: function(){
		if ($('radiusSliderarea')){
			var windowOptions = MUI.Windows.windowOptions;
			var sliderFirst = true;
			var mochaSlide = new Slider($('radiusSliderarea'), $('radiusSliderknob'), {
				range: [10, 100],
				steps: 9,
				offset: 0,
				onChange: function(pos) {
					$('radiusUpdatevalue').set('html', pos);
					// Change default corner radius of the original class
					windowOptions.markerOffset = pos;
					MUI.Window.implement({options: windowOptions});
					// Don't redraw windows the first time the slider is initialized
					if(sliderFirst == true) {
						sliderFirst = false;
						return;
					}
					
					// Change corner radius of all active classes and their windows
					
					/*
					MUI.Windows.instances.each(function(instance) {
						instance.options.cornerRadius = pos;
						instance.drawWindow();
					}.bind(this));
					*/
					
					var gCookie = Cookie.write('_moffset', pos, {duration: 1});
					var moffset = pos;
					MUI.notification('markerOffset '+pos);
				}.bind(this)
			}).set(moffset);
		}
	},
	addMarkerNumArcsSlider: function(){
		if ($('shadowSliderarea')){
			var windowOptions = MUI.Windows.windowOptions;
			var sliderFirst = true;
			var mochaSlide = new Slider($('shadowSliderarea'), $('shadowSliderknob'), {
				range: [0, 10],
				//steps: 1,
				offset: 0,
				onChange: function(pos){
					$('shadowUpdatevalue').set('html', pos);
					// Change default shadow width of the original class
					windowOptions.markerNumArcs = pos;
					MUI.Window.implement({ options: windowOptions });
					// Don't redraw windows the first time the slider is initialized
					// !!! Probably need to make this separate from the corner radius slider
					if (sliderFirst == true) {
						sliderFirst = false;
						return;
					}
					// Change shadow width of all active classes and their windows
					/*
					MUI.Windows.instances.each(function(instance) {
						var oldshadowBlur = instance.options.shadowBlur;
						instance.options.shadowBlur = pos;
						instance.windowEl.setStyles({
							'top': instance.windowEl.getStyle('top').toInt() - (instance.options.shadowBlur - oldshadowBlur),
							'left': instance.windowEl.getStyle('left').toInt() - (instance.options.shadowBlur - oldshadowBlur)
						});
						instance.drawWindow();
					}.bind(this));
					*/
					var gCookie = Cookie.write('_mnumarcs', pos, {duration: 1});
					var moffset = pos;
					MUI.notification('markerNumArcs '+pos);
				}.bind(this)/*,
				onComplete: function(){
					MUI.Windows.instances.each(function(instance) {
						if (instance.options.resizable){
							instance.adjustHandles();
						}
					});
				}
				*/
			}).set(mnumarcs);
		}
	}
});





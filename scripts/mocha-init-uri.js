
var Sitis = {};

window.map_global = new Array();
window.gm_instance = new Array();
var map_instance = new Object();

initializeWindows = function(){
	
	MUI.parametricsWindow = function(){
		new MUI.Window({
			id: 'parametrics',
			title: 'Параметры маркера',
			contentURL: MUI.path.plugins + 'parametrics/index.html',
			width: 305,
			height: 160,
			x: 370,
			y: 160,
			padding: {top: 12, right: 12, bottom: 10, left: 12},
			resizable: false,
			maximizable: false,
			require: {
				css: [MUI.path.plugins + 'parametrics/css/style.css'],
				js: [MUI.path.plugins + 'parametrics/scripts/parametrics.js'],
				onload: function(){
					if (MUI.addMarkerOffsetSlider) MUI.addMarkerOffsetSlider();
					if (MUI.addMarkerNumArcsSlider) MUI.addMarkerNumArcsSlider();
				}
			}
		});
	};
	
	MUI.myChain.callChain();
	
};

/* Columns & panels */

initializeColumns = function() {
	
	new MUI.Column({
		id: 'mainColumn',
		placement: 'main',
		resizeLimit: [100, 400]
	});
	
	new MUI.Column({
		id: 'sideColumn2',
		placement: 'right',
		width: 400,
		resizeLimit: [100, 400]
	});
	
	// Add panels to first side column
	/*
	new MUI.Panel({
		id: 'files-panel',
		title: lang.file,
		contentURL: 'pages/files_test.php',
		column: 'sideColumn1',
		padding: {top: 0, right: 0, bottom: 0, left: 0},
		require: {
			css: [MUI.path.plugins + 'tree/css/style.css'],			
			js: [MUI.path.plugins + 'tree/scripts/tree.js'],
			onload: function() {
				if(buildTree) buildTree('tree1');
			}
		},
		onContentLoaded: function() {
			
			Sitis.loadImg(window.img_global);
			
		}
	});
	*/
	
	/*
	new MUI.Panel({
		id: 'panel2',
		title: lang.filter,
		//contentURL: 'pages/ajax.form.html',
		column: 'sideColumn1',
		height: 230
	});
	*/
	
	// Add panels to main column
	new MUI.Panel({
		id: 'mainPanel',
		title: lang.geophoto,
		contentURL: 'pages/geophoto.php?uri='+window.img_global,
		column: 'mainColumn',
		padding: {top: 0, right: 0, bottom: 0, left: 0},
		collapsible: false,
		onContentLoaded: function() {
			
			Sitis.loadImg(window.img_global);
			
		}
		//headerToolbox: true,
		//headerToolboxURL: 'pages/toolbox-demo2.html'
	});
	
	// Add panels to second side column
	
	new MUI.Panel({
		id: 'map1',
		//contentURL: 'pages/overview.html',
		//contentURL: 'pages/google_map.php',
		column: 'sideColumn2',
		padding: {top: 0, right: 0, bottom: 0, left: 0},
		headerToolbox: true,
		headerToolboxURL: 'pages/google-tabs.html',
		title: lang.map+' #1',
		collapsible: false,
		height: 300
	});
	
	new MUI.Panel({
		id: 'map2',
		title: lang.map+' #2',
		addClass: 'ya_test',
		//contentURL: 'pages/yandex_map.php',
		column: 'sideColumn2',
		headerToolbox: true,
		headerToolboxURL: 'pages/yandex-tabs.html',
		padding: {top: 0, right: 0, bottom: 0, left: 0},
		collapsible: false,
		height: 300
	});
	
	new MUI.Panel({
		id: 'properties',
		//title: lang.properties,
		contentURL: 'pages/ajax-tabs.php?uri='+window.img_global,
		column: 'sideColumn2',
		tabsURL: 'pages/test-tabs.php',
		padding: {top: 10, right: 10, bottom: 10, left: 10},
		collapsible: false,
		require: {
			css: ['themes/charcoal/css/Tabs.css']
		}
	});
	
	/*
	new MUI.Panel({
		id: 'mochaConsole',
		addClass: 'mochaConsole',
		title: lang.preview,
		padding: {top: 5, right: 0, bottom: 0, left: 0},
		contentURL: 'pages/preview.php',
		column: 'mainColumn',
		height: 170,
		//headerToolbox: true,
		//headerToolboxURL: 'pages/console.toolbox.html',
	});
	
	MUI.myChain.callChain();
	*/
	
};

// Initialize MochaUI when the DOM is ready
window.addEvent('load', function(){
	
	MUI.myChain = new Chain();
	MUI.myChain.chain(
		
		function(){MUI.Desktop.initialize();},
		function(){MUI.Dock.initialize();},
		function(){initializeColumns();},
		function(){initializeWindows();}
		
	).callChain();
	
});

Sitis.loadImg = function(img, num_img)
{
	window.img_global = img;
	
	ok = $('mainColumn').style.width;
	Cookie.write('_imgx', ok, {duration: 1});
	
	oy = $('mainPanel').style.height;
	Cookie.write('_imgy', oy, {duration: 1});
	
	var myRequest = new Request({
	evalScripts: true,
	method: 'POST',
	url: 'pages/map-init.php',
	data: {img: window.img_global},
	onComplete: function(event, xhr){
		// central
		MUI.updateContent({
			
			'element': $('mainPanel'),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {img: img},
			'url': 'pages/geophoto.php',
			onContentLoaded: function(){
				
				MUI.notification(img);
				// properties
				MUI.updateContent({
					'element': $('properties'),
					//'content': response,
					'method': 'POST',
					'loadMethod': 'xhr',
					'data': {img: img},
					'url': 'pages/ajax-tabs.php'				
				}
				
				);
			}
			
		}
		);
		
		Sitis.refreshMap('map1', true);
		//alert(typeof(num_img));
		var products = $$('#preview li');
		//if(typeof(num_img) != 'undefined') myProducts.set(products[num_img]);
		if(typeof(num_img) != 'undefined') myProducts.set(51 * num_img);
		
		}
	});
	
	myRequest.send();
}

Sitis.changeData = function(img, task)
{
	
	MUI.updateContent({
	
		'element': $('properties'),
		//'content': response,
		'method': 'POST',
		'loadMethod': 'xhr',
		'data': {img: img, code: task},
		'url': 'pages/ajax-tabs.php',
	
	}
	
	);
	
}

Sitis.makeScrollbar = function(needle, content, scrollbar, handle, horizontal, ignoreMouse)
{
	var mainH = needle.offsetHeight;
	scrollbar.style.height = (mainH - 10)+'px';
	content.style.height = (mainH - 10)+'px';
	content.style.width = (needle.offsetWidth - 30)+'px';
	
	var steps = (horizontal?(content.getScrollSize().x - content.getSize().x):(content.getScrollSize().y - content.getSize().y))
	var slider = new Slider(scrollbar, handle, {
		steps: steps,
		mode: (horizontal?'horizontal':'vertical'),
		onChange: function(step) {
			
			var x = (horizontal?step:0);
			var y = (horizontal?0:step);
			content.scrollTo(x,y);
		
		}
		
	}).set(0);
	if(!(ignoreMouse)){
		
		$$(content, scrollbar).addEvent('mousewheel', function(e){
			e = new Event(e).stop();
			var step = slider.step - e.wheel * 30;
			slider.set(step);
		});
	}
	
	$(document.body).addEvent('mouseleave',function(){slider.drag.stop()});
	
}

Sitis.map_menu_open = function(button_id, menu_name)
{
	var tofade = menu_name;
	if($(menu_name+'x')) {
		
		$(menu_name+'x').dispose();
		
	} else {
		
		menu_html = $(menu_name).get('html');
		var find = $(button_id).getCoordinates();
		var menu_inject = new Element('div', {id: menu_name+'x'});
		menu_inject.set('html', menu_html);
		menu_inject.style.position = 'absolute';
		menu_inject.style.zIndex = '1000';
		menu_inject.style.top = (find.top + 25) + 'px';
		menu_inject.style.left = find.left+'px';
		$('desktop').adopt(menu_inject);
	}
	
}

Sitis.url_map = function(zmap)
{
	//alert(zmap);
	switch(zmap)
	{
		case 'google':
			map_zurl = 'pages/google_map.php';
			break;
		case 'yandex':
			map_zurl = 'pages/yandex_map.php';
			break;
		case 'bing':
			map_zurl = 'pages/bing_map.php';
			break;
		case 'osm':
			map_zurl = 'pages/osm_map.php';
			break;
	}
	
	return map_zurl;
}

Sitis.changeMap = function(map_id, map, img, trigger)
{
	if(trigger == false) {var map_url = this.url_map(map);} else {map_url1 = this.url_map(window.map_global['map1']); map_url2 = this.url_map(window.map_global['map2']);}

	// trigger чтобы вторая карта загружалась синxронно после первой
	if(trigger == false) {
		
		$(map_id).set('html', '');
		Cookie.write('_' + map_id, map, {duration: 1});
		MUI.updateContent({
		
			'element': $(map_id),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {where: map_id},
			'url': map_url
		}
		
		);
		
	} else {
		
		Cookie.write('_map1', window.map_global['map1'], {duration: 1});
		Cookie.write('_map2', window.map_global['map2'], {duration: 1});
		
		$('map1').set('html', '');
		$('map2').set('html', '');
		
		MUI.updateContent({
		
			'element': $('map1'),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {where: 'map1'},
			'url': map_url1,
		
		});
		
		MUI.updateContent({
			
			'element': $('map2'),
			//'content': response,
			'method': 'POST',
			'loadMethod': 'xhr',
			'data': {where: 'map2'},
			'url': map_url2
			
		}
		
		);
	}
	this.print_comments();
}

Sitis.refreshMap = function(map_id, trigger)
{
	var myRequest = new Request({
	evalScripts: true,
	method: 'POST',
	url: 'pages/map-init.php',
	data: {img: window.img_global},
	onComplete: function(event, xhr){
		
		var map1_cookie = Cookie.read('_map1');
		var map2_cookie = Cookie.read('_map2');
		if(map1_cookie == null) window.map_global['map1'] = 'osm'; else window.map_global['map1'] = map1_cookie;
		if(map2_cookie == null) window.map_global['map2'] = 'google'; else window.map_global['map2'] = map2_cookie;
		//if($('map_global')) alert($('map_global');
		Sitis.changeMap(map_id, window.map_global[map_id], window.img_global, trigger);
		//alert(map1_cookie+' / '+map2_cookie);
		}
	});
	
	myRequest.send();
	
}

Sitis.print_comments = function()
{
	// комментарии в окне с фотографией
	if($('inject')) $('inject').dispose();
	var comment_inject = new Element('div', {id: 'inject'});
	$('mainPanel').adopt(comment_inject);
	var off_width = $('mainPanel').offsetWidth;
	
	comment_inject.style.position = 'absolute';
	comment_inject.style.bottom = '0';
	comment_inject.style.left = '0';
	comment_inject.style.padding = '20px';
	
	comment_inject.style.background = 'url(images/tr.png)';
	comment_inject.style.width = (off_width - 40) + 'px';
	comment_inject.style.textAlign = 'center';
	comment_inject.style.cursor = 'pointer';
	if(comments == '0') var out_comments = lang.no_comments; else var out_comments = comments;
	comment_inject.set('text', out_comments);
	if(comments == '0') var edit_comments = ''; else var edit_comments = comments; 
	comment_inject.addEvent('click', function(event) {
		
		new MUI.Window({
			id: 'parametrics',
			title: 'Комментарии',
			//contentURL: MUI.path.plugins + 'parametrics/index.html',
			content: '<textarea style="overflow: hidden;width: 100%;height: 100px;background: #555;color: #fff;border: 1px solid #000">'+edit_comments+'</textarea>',
			//padding: {top: 12, right: 12, bottom: 10, left: 12},
			resizable: true,
			maximizable: false
		});
	});
	
}



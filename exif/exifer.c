
#include "ExifLibImpl.h"
#include <stdio.h>
#include <string.h>
#include <iconv.h>
#include <errno.h>
#include <err.h>
#include <argtable2.h>

size_t strlenu(const short *s){
        const short *p;
        
        if ( !s )
                return 0;
        for ( p = s; *p; ++p )
                ;
        return (p - s);
}

#define MAX_COMMENT_SIZE 2000

double RationalToDouble(ExifRational x)
{
	if(x.numerator != 0 && x.denominator != 0) return (double)x.numerator / x.denominator; else return 0;
}

double SRationalToDouble(ExifSRational x)
{
	if(x.numerator != 0 && x.denominator != 0) return (double)x.numerator / x.denominator; else return 0;
}

int mymain(const char **comments, int ncomments, const char **infiles, int ninfiles)
{
	/*
	int i, j;
	char ch;
	for (i = 0; i < ncomments; i++)
	{
		printf("comment %d (size=%d): %s\n", i, strlen(comments[i]), comments[i]);
		for (j = 0; j < strlen(comments[i]); j++)
		{
			ch = comments[i][j];
			printf("%d ", 0xff&ch);
		}
		printf("\n");
		wprintf(L"comment %d: %s\n", i, comments[i]);
	}
	for (i = 0; i < ninfiles; i++)
	{
		printf("comment %d: %s\n", i, infiles[i]);
		wprintf(L"comment %d: %s\n", i, infiles[i]);
	}
	iconv_t cd;
	size_t ka,fa,ta;
	char *in_str = *comments;
	fa = strlen(in_str);
	char buf[20];
	char *b = buf;
	ta = fa * 2;
	memset(buf, 0, sizeof(buf));
	cd = iconv_open("UTF-16LE", "UTF-8");
	ka = iconv(cd, &in_str, &fa, &b, &ta);
	printf("1\n");
	printf("1\n");
	printf("buf=%s\n", (wchar_t*)buf);
	printf("1\n");
	printf("1\n");
	iconv_close(cd);
	
		for (j = 0; j < sizeof(buf); j++)
		{
			ch = buf[j];
			printf("%d ", 0xff&ch);
		}
	*/

	iconv_t cd;
	size_t ka,fa,ta;
	
	int i, j;
	char ch;
	
	char *in_str = *comments;
	
	char buf[MAX_COMMENT_SIZE];
	char* out = buf;
	
	char buf1[MAX_COMMENT_SIZE];
	char* out1 = buf1;
	
	char *code;
	
	struct ImageInfo imageInfo = ReadImageInfo(infiles[0]);
	
	// cmd line (--comments="john")
	if(ncomments>0){
		
		cd = iconv_open("UTF-16LE", "UTF-8");
		fa = strlen(in_str);
		//printf("fa: %u\n", fa);
		//printf("comment in: %s\n", in_str);
		//printf("\n");
		//unsigned short *comm = (unsigned short*)*comments;
		//for (i = 0; i < strlen(*comments)/2; i++)
		//	printf("%X ", comm[i]);//(unsigned char)((*comments)[i]));
		printf("\n");
		
		ta = sizeof(buf);
		memset(buf, '\0', sizeof(buf));
		char *pout = out+2;
		ka = iconv(cd, &in_str, &fa, &pout, &ta);
		out[0] = 0xff;
		out[1] = 0xfe;
		//		comm = (unsigned char*)out;
		//	for (i = 0; i < 5; i++)
		//		printf("%X ", comm[i]);//(unsigned char)((*comments)[i]));
		//printf("comment out: %s\n", out);
		iconv_close(cd);
		memcpy(imageInfo.Comments, buf, sizeof(buf));
		imageInfo.CommentsLength = strlenu(buf);
		//printf("ExistVViewAngle =%d\n", imageInfo.SGeo.ExistVViewAngle);
		//printf("VViewAngle =%f\n", RationalToDouble(imageInfo.SGeo.VViewAngle));
		WriteImageInfo(infiles[0], infiles[0], &imageInfo);
	}
	
	code = imageInfo.Comments;
	cd = iconv_open("UTF-8", "UTF-16LE");
	//printf("imageInfo.CommentsLength=%d\n", imageInfo.CommentsLength);
	fa = imageInfo.CommentsLength*2;
	ta = sizeof(buf1);
	memset(buf1, '\0' , sizeof(buf1));
	ka = iconv(cd, &code, &fa, &out1, &ta);
	//memcpy(imageInfo.Comments, buf1, sizeof(buf1));
	iconv_close(cd);
	
	printf("CameraMake: %s\n", imageInfo.CameraMake);
	//printf("Comments: %s\n", imageInfo.Comments);
	/*
		for (j = 0; j < 30; j++)//imageInfo.Comments[j] != 0
		{
			ch = ((char*)imageInfo.Comments)[j*2];
			printf("%d ", 0xff&ch);
			ch = ((char*)imageInfo.Comments)[j*2+1];
			printf("%d ", 0xff&ch);
		}
	*/
	
	//printf("bufferComments: %s\n", buf);
	printf("Comments: %s\n", buf1);
	//printf("sizeofbuf1: %d\n", wcslen(buf));

	/*
	iconv_t cd;
	size_t ka,fa,ta;
	
	int i;
	
	char buf[MAX_COMMENT_SIZE];
	char* out = buf;
	size_t out_len;

	//char buf1[MAX_COMMENT_SIZE];
	
	struct ImageInfo imageInfo = ReadImageInfo(infiles[0]);
	
	// cmd line (--comments="john")
	if(ncomments>0) {
		
		cd = iconv_open("UTF-16LE", "UTF-8");
		if (cd == (iconv_t)(-1)) {
			if (errno != EINVAL)
				return -1;
		}
		fa = strlen(*comments) + 1;// ноль тоже учитывается в длине
		//ta = sizeof(buf);
		ka = iconv(cd, comments, &fa, &out, &out_len);
		if (ka == (size_t)(-1) && errno != E2BIG) 
		{
			int saved_errno = (errno == EINVAL ? EILSEQ : errno);
			iconv_close(cd);
			errno = saved_errno;
			return -1;
		}

		iconv_close(cd);
		memcpy(imageInfo.Comments, out, out_len);
		WriteImageInfo(infiles[0], infiles[0], &imageInfo);
		
	}
	
	char *code;
	char buf1[MAX_COMMENT_SIZE];
	char* out1 = buf1;
	size_t out_len1;
	code = imageInfo.Comments;
	
	cd = iconv_open("UTF-8", "UTF-16LE");
	fa = imageInfo.CommentsLength * 2 + 1;
	//ta = sizeof(buf1);
	ka = iconv(cd, &code, &fa, &out1, &out_len1);
	iconv_close(cd);
	
	printf("CameraMake: %s\n", imageInfo.CameraMake);
	printf("Comments: %s\n", imageInfo.Comments);
	printf("Comments1: %s\n", out1);
	printf("sizeofbuf1: %u\n", out_len1);
	*/
	/*
	for (i = 0; i < 20; i++)
	printf("%d ", buf[i]);
	printf("\nUnicode len: %u\n", sizeof(buf));
	memcpy(imageInfo.Comments, buf, 2000);
	printf("length: %u\n", strlen(*comments));
	printf("comm: ");
	for (i = 0; i < strlen(*comments); i++)
	printf("%c ", (*comments)[i]);
    printf("\n");
	*/
	
	printf("Longitude: %lf\n", (double)SRationalToDouble(imageInfo.SGeo.Longitude));
	printf("LongitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.LongitudeAccuracy));
	
	printf("Latitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.Latitude));
	printf("LatitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.LatitudeAccuracy));
	
	printf("Altitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.Altitude));
	printf("AltitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.AltitudeAccuracy));
	
	printf("Azimuth: %f\n", (double)SRationalToDouble(imageInfo.SGeo.Azimuth));
	printf("AzimuthAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.AzimuthAccuracy));
	
	printf("Pitch: %f\n", (double)SRationalToDouble(imageInfo.SGeo.Pitch));
	printf("PitchAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.PitchAccuracy));
	
	printf("Roll: %f\n", (double)SRationalToDouble(imageInfo.SGeo.Roll));
	printf("PitchAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.RollAccuracy));
	
	printf("HViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.HViewAngle));
	printf("HViewAngleAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.HViewAngleAccuracy));
	printf("VViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.VViewAngle));
	printf("ExistVViewAngle: %d\n", (int)imageInfo.SGeo.ExistVViewAngle);
	printf("VViewAngleAccuracy: %d\n", (int)(int)RationalToDouble(imageInfo.SGeo.VViewAngleAccuracy));
	//printf("VViewAngleAccuracy: %d\n", (int)imageInfo.SGeo.VViewAngleAccuracy.numerator);
	
	printf("UserLongitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserLongitude));
	printf("UserLongitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserLongitudeAccuracy));
	
	printf("UserLatitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserLatitude));
	printf("UserLatitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserLatitudeAccuracy));
	
	printf("UserAltitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserAltitude));
	printf("UserAltitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserAltitudeAccuracy));
	
	printf("UserEarthLevel: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserEarthLevel));
	
	printf("UserAzimuth: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserAzimuth));
	printf("UserAzimuthAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserAzimuthAccuracy));
	
	printf("UserPitch: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserPitch));
	printf("UserPitchAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserPitchAccuracy));
	
	printf("UserPitch: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserPitch));
	printf("UserPitchAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserPitchAccuracy));
	
	printf("UserRoll: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserRoll));
	printf("UserRollAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserRollAccuracy));
	
	printf("UserRoll: %f\n", (double)SRationalToDouble(imageInfo.SGeo.UserRoll));
	printf("UserRollAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserRollAccuracy));
	
	printf("UserHViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserHViewAngle));
	printf("UserHViewAngleAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.HViewAngleAccuracy));
	printf("UserVViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserVViewAngle));
	printf("UserVViewAngleAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.UserVViewAngleAccuracy));
	
	printf("ResultLongitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultLongitude));
	printf("ResultLongitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultLongitudeAccuracy));
	
	printf("ResultLatitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultLatitude));
	printf("ResultLatitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultLatitudeAccuracy));
	
	printf("ResultAltitude: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultAltitude));
	printf("ResultAltitudeAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultAltitudeAccuracy));
	
	printf("ResultAzimuth: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultAzimuth));
	printf("ResultAzimuthAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultAzimuthAccuracy));
	
	printf("ResultPitch: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultPitch));
	printf("ResultPitchAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultPitchAccuracy));
	
	printf("ResultRoll: %f\n", (double)SRationalToDouble(imageInfo.SGeo.ResultRoll));
	printf("ResultRollAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultRollAccuracy));
	
	printf("ResultHViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultHViewAngle));
	printf("ResultHViewAngleAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultHViewAngleAccuracy));
	printf("ResultVViewAngle: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultVViewAngle));
	printf("ResultVViewAngleAccuracy: %d\n", (int)RationalToDouble(imageInfo.SGeo.ResultVViewAngleAccuracy));
	
	printf("Version: %d\n", (int)(imageInfo.SGeo.VersionID[3]));
	
    return 0;
	
}

int main(int argc, char **argv)
{
	//struct arg_lit  *list    = arg_lit0("lL",NULL,                      "list files");
    //struct arg_lit  *recurse = arg_lit0("R",NULL,                       "recurse through subdirectories");
    //struct arg_int  *repeat  = arg_int0("k","scalar",NULL,              "define scalar value k (default is 3)");
    struct arg_str  *comments = arg_strn("c","comments","MACRO",0,argc+2,  "save comments in exif .jpg");
    //struct arg_file *outfile = arg_file0("o",NULL,"<output>",           "output file (default is \"-\")");
    //struct arg_lit  *verbose = arg_lit0("v","verbose,debug",            "verbose messages");
    //struct arg_lit  *help    = arg_lit0(NULL,"help",                    "print this help and exit");
    struct arg_lit  *version = arg_lit0(NULL,"version",                 "print version information and exit");
    struct arg_file *infiles = arg_filen(NULL,NULL,NULL,1,argc+2,       "input file(s)");
    struct arg_end  *end     = arg_end(20);
    //void* argtable[] = {list,recurse,repeat,defines,outfile,verbose,help,version,infiles,end};
	void* argtable[] = {comments,version,infiles,end};
	
    const char* progname = "Exif_parser";
    int nerrors;
    int exitcode=0;
	
	if(arg_nullcheck(argtable) != 0)
	{
		printf("%s: insufficient memory\n",progname);
		exitcode=1;
		goto exit;
	}
	
    //repeat->ival[0]=3;
    //outfile->filename[0]="-";
	
    nerrors = arg_parse(argc,argv,argtable);
	
    if(version->count > 0)
	{
        printf("\n'%s' september, 2011.\n", progname);
        printf("\nSitis, 2011\n");
        exitcode=0;
        goto exit;
	}
	
    if(nerrors > 0)
        {

        arg_print_errors(stdout,end,progname);
        exitcode=1;
        goto exit;
        }
	
    if(argc==1)
        {
        printf("Try '%s --help' for more information.\n",progname);
        exitcode=0;
        goto exit;
        }
	
    exitcode = mymain(comments->sval, comments->count, infiles->filename, infiles->count);
	
    exit:

    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

    return exitcode;
    }



#include "ExifLibImpl.h"
#include <stdio.h>
#include <string.h>
#include <iconv.h>
#include <errno.h>
#include <err.h>
#include <argtable2.h>

int mymain(int l, int R, int k, const char **defines, int ndefines, const char *outfile, int v,
			const char **infiles, int ninfiles)
{
	iconv_t cd;
	size_t ka, fa, ta;
	int i;
	int se;
	
	char buf[2000];
	char* out = buf;
	
	//printf("image: %s\n", infiles[0]);
    struct ImageInfo imageInfo = ReadImageInfo(infiles[0]);
	
	char *code = imageInfo.Comments;
	char* in = code;
	
	// iconv
	cd = iconv_open("UTF-8", "UTF-16LE");
	if(cd == (iconv_t)(-1)) err(1, "iconv_open");
	
	//in_string = (char*)imageInfo.Comments;
	
	fa = imageInfo.CommentsLength * 2;
	ta = sizeof buf;
	
	memset(&buf, 0 , sizeof buf);
	errno = 0;
	
	ka = iconv(cd, &code, &fa, &out, &ta);
	se = errno;
	
	printf("\nComments: ");
	printf("Comments: %s\n", buf);
	printf("converted: %u,error=%d\n", (unsigned)ka, se);
	
	/*
	for (i = 0; i < imageInfo.CommentsLength * 2; i++)
		printf("%c", in_string[i]);
	printf("\n");
	*/
	
	iconv_close(cd);
    return 0;
	
    }

int main(int argc, char **argv)
    {
   struct arg_lit  *list    = arg_lit0("lL",NULL,                      "list files");
    struct arg_lit  *recurse = arg_lit0("R",NULL,                       "recurse through subdirectories");
    struct arg_int  *repeat  = arg_int0("k","scalar",NULL,              "define scalar value k (default is 3)");
    struct arg_str  *defines = arg_strn("D","define","MACRO",0,argc+2,  "macro definitions");
    struct arg_file *outfile = arg_file0("o",NULL,"<output>",           "output file (default is \"-\")");
    struct arg_lit  *verbose = arg_lit0("v","verbose,debug",            "verbose messages");
    struct arg_lit  *help    = arg_lit0(NULL,"help",                    "print this help and exit");
    struct arg_lit  *version = arg_lit0(NULL,"version",                 "print version information and exit");
    struct arg_file *infiles = arg_filen(NULL,NULL,NULL,1,argc+2,       "input file(s)");
    struct arg_end  *end     = arg_end(20);
    void* argtable[] = {list,recurse,repeat,defines,outfile,verbose,help,version,infiles,end};
    const char* progname = "Exif_parser";
    int nerrors;
    int exitcode=0;
	
    if (arg_nullcheck(argtable) != 0)
        {

        printf("%s: insufficient memory\n",progname);
        exitcode=1;
        goto exit;
        }

    repeat->ival[0]=3;
    outfile->filename[0]="-";


    nerrors = arg_parse(argc,argv,argtable);

    if (help->count > 0)
        {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout,argtable,"\n");
        printf("This program demonstrates the use of the argtable2 library\n");
        printf("for parsing command line arguments. Argtable accepts integers\n");
        printf("in decimal (123), hexadecimal (0xff), octal (0o123) and binary\n");
        printf("(0b101101) formats. Suffixes KB, MB and GB are also accepted.\n");
        arg_print_glossary(stdout,argtable,"  %-25s %s\n");
        exitcode=0;
        goto exit;
        }

    if (version->count > 0)
        {
        printf("\n'%s' example program for the \"argtable\" command line argument parser.\n",progname);
        printf("\nСитис, 2011\n");
        exitcode=0;
        goto exit;
        }

    if (nerrors > 0)
        {

        arg_print_errors(stdout,end,progname);

        exitcode=1;
        goto exit;
        }

    if (argc==1)
        {
        printf("Try '%s --help' for more information.\n",progname);
        exitcode=0;
        goto exit;
        }

    exitcode = mymain(list->count, recurse->count, repeat->ival[0],
                      defines->sval, defines->count,
                      outfile->filename[0], verbose->count,
                      infiles->filename, infiles->count);

    exit:

    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

    return exitcode;
    }

